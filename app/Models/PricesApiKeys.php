<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricesApiKeys extends Model
{
    //
    protected $table = 'prices_api_keys';
}
