<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'order_auto_short_form';

    public $timestamps = false;

    protected $fillable = [
        'group',
        'num',
        'country_start',//
        'country_end',//
        'period_start',
        'period_end',
        'town_start',
        'town_end',
        'time_start',
        'time_end',
        'klass',
        'auto',
           'autos_name',
        'transmission',
        'fio',
        'tel',
        'mail',
        'source',
        'ip',
           'created',
        'user_agent',
           'login',
           'status',
           'vykonano',
           'feedback',
           'feedback_message',
        'comment',
        'sertificat',
        'message',
           "clients_fio",
           "agreements_key",
           "reservation_id",
           "del",
           "fio_del",
           "login_edit",
           "time_edit_start",
           "time_edit",
           "time_edit_activ",
           "redact_on",
           "comment_manager",
    ];

}
