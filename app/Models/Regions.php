<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use LaravelLocalization;
class Regions extends Model
{
    //
    protected $table = 'regions';

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'countries_id');
    }

    public static function getRequiredRegions($requiredRegionsArray)
    {
        $regionsFromTable =self::with('country')->get()->toArray();
        foreach ($regionsFromTable as $item){
            $regionsUpdated[$item['id']] = $item;
        }
        $regions = [];
        foreach ($requiredRegionsArray as $value){
            if (array_key_exists($value, $regionsUpdated)){
                $regions[$regionsUpdated[$value]['id']] = $regionsUpdated[$value];
            }

        }
        $locale = LaravelLocalization::getCurrentLocale();
        foreach ($regions as $key => $value){
            if ($locale == 'ru'){
                $regions[$key]['city_localized'] =  $regions[$key]['name'];
            }
            if ($locale == 'en'){
                $regions[$key]['city_localized'] =  $regions[$key]['name_en'];
            }
            if ($locale == 'ua'){
                $regions[$key]['city_localized'] =  $regions[$key]['name_uk'];
            }
//            if ($locale == 'ge'){
//                $countries[$key]['country_localized'] =  $countries[$key]['name_country_ge'];
//            }
        }
        return $regions;
    }
}
