<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Country extends Model
{
    protected $table = 'country';

    public function cities()
    {
        return $this->hasMany('App\Models\CityForPages');
    }

    public static function getRequiredCountries($requiredCountriesArray)
    {
        $countriesFromTable =self::all()->toArray();
        foreach ($countriesFromTable as $item){
            $countriesUpdated[$item['id']] = $item;
        }
        $countries = [];
        foreach ($requiredCountriesArray as $value){
            if (array_key_exists($value, $countriesUpdated)){
                $countries[$countriesUpdated[$value]['id']] = $countriesUpdated[$value];
            }
        }
        $locale = LaravelLocalization::getCurrentLocale();
        foreach ($countries as $key => $value){
            if ($locale == 'ru'){
                $countries[$key]['country_localized'] =  $countries[$key]['name_country_ru'];
            }
            if ($locale == 'en'){
                $countries[$key]['country_localized'] =  $countries[$key]['name_country_en'];
            }
            if ($locale == 'ua'){
                $countries[$key]['country_localized'] =  $countries[$key]['name_country'];
            }
//            if ($locale == 'ge'){
//                $countries[$key]['country_localized'] =  $countries[$key]['name_country_ge'];
//            }
        }
        return $countries;
    }

    public static function getCurrencyRateByName($currName)
    {
        return self::where('currency', $currName)->pluck('volume')->first();
    }



}
