<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryForPages extends Model
{
    //protected $connection = 'mysql2';

    protected $table = 'countries_for_pages';

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }


}
