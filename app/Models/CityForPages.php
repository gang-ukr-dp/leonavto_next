<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CityForPages extends Model
{
    //protected $connection = 'mysql2';

    protected $table = 'cities_for_pages';

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }


}
