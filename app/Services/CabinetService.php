<?php

namespace App\Services;


use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Session;

class CabinetService
{
    public function register($data)
    {
        $data = [
            'first_name' => $data['first_name'],
            'second_name' => $data['second_name'],
            'last_name' => $data['last_name'],
            'birthday' => $data['birthday'],
            'email' => 'LION__'.$data['email'],
            'phone' => $data['phone'],
            'site' => 'liontest.com',
            'password' => $data['password'],
            'sex' => 'x',
            'TokenMail' => '*',
            ];
        $client = new GuzzleClient();
        $response = $client->post('lionua.pp.ua/api/register?'.http_build_query($data));
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function login()
    {
        $client = new GuzzleClient();
        $response = $client -> post('https://lionua.pp.ua/public/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => 1,
                'client_secret' => 'OPidn4CYvVYKEIwAV0bTehqKixHaLxuJ31Y68UvU',
                'username' => 'LION__test@gmail.com',
                'password' => '111111',
                'scope' => '*',],]);

        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function token($username, $password)
    {
        try
        {
            $client = new  GuzzleClient();
            $response = $client -> post('https://lionua.pp.ua/public/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => 1,
                    'client_secret' => 'OPidn4CYvVYKEIwAV0bTehqKixHaLxuJ31Y68UvU',
                    'username' => $username,
                    'password' => $password,
                    'scope' => '*',],]);
        } catch (\GuzzleHttp\Exception\RequestException $e){
            return false;
        }
        $body = $response -> getBody();
        $object_body = json_decode((string)$body);
        if (array_key_exists('error', (array)$object_body)){
            return false;
        }
        return $object_body->access_token;
    }


    public function userInfo($token)
    {
        try{
            $client = new GuzzleClient();
            $response = $client -> get('https://lionua.pp.ua/api/user', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',],]);
        } catch (\GuzzleHttp\Exception\RequestException $e){
            return false;
        }
        $content = json_decode($response->getBody()->getContents());
        if (array_key_exists('user', (array)$content)){
            return $content;
        }else{
            return false;
        }
    }

    public function getOrdersList()
    {
        $token = Session::get('access_token');
        try{
            $client = new GuzzleClient();
            $response = $client -> get('https://lionua.pp.ua/api/orders', [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',],]);
        } catch (\GuzzleHttp\Exception\RequestException $e){
            return false;
        }
        return json_decode($response->getBody()->getContents());
    }



}