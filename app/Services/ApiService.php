<?php

namespace App\Services;

use App\Models\Intervals;
use GuzzleHttp\Client as GuzzleClient;
use App\Models\PricesApiKeys;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Models\Regions;

//source_domains
//0 Без групи
//1 Lion
//2 Race
//3 Rentacar
//4 24rent-voyage
//5 autoprokat.net.ua
//6 car-sharing.dp.ua
//7 goldencars.com.ua
//8 hire-auto.biz
//9 autodin.com.ua

//country
//1 Украина
//2 Россия
//3 Грузия
//4 Чехия
//5 Болгария
//
class ApiService
{
    const DOMAIN = 1;//source_domains
    const DOMAIN_FOR_CITIES = 1;
    const COUNTRY = 1;

    public function getAllCarsByClass()
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => true,
            'sorting' => 'asc',
            'type' => 'prices',
            'curr' => 3,
            'filters' => [
                'class_id' => [
                    1,2,4,5,7,12
                ],
            ],
            'group_by' => 'true',
            'date' => $date,
            'country' => self::COUNTRY
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    public function getAllCarsOfCountry($countryId=1, $locale = null, $curr = 6)
    {
        list($token, $date) = $this->getRequestParams();
        if (!$locale){
            $loc = LaravelLocalization::getCurrentLocale();
        }else{
            $loc = $locale;
        }
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'type' => 'prices',
            'date' => $date,
            'country' => $countryId,
            'sorting' => 'asc',
//            'regions' => 'true',
            'lang' => $loc,
            'curr' => $curr,
        ];
        $client = new GuzzleClient();
        try{
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e){
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    public function getAllCarsOfCity($countryId = 1, $cityId = 1,  $locale = null, $curr = 6)
    {
        $targetCity = Regions::where('id', $cityId)->get()->first();
        $targetCityName = explode(',',$targetCity->name)[0];
        $realCity = Regions::where('name', $targetCityName)->get()->first();
        list($token, $date) = $this->getRequestParams();
        if (!$locale){
            $loc = LaravelLocalization::getCurrentLocale();
        }else{
            $loc = $locale;
        }
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'type' => 'prices',
            'date' => $date,
            'sorting' => 'asc',
            //'country' => $countryId,
            'region' => $realCity->id,
            'lang' => $loc,
            'curr' => $curr,
        ];
        $client = new GuzzleClient();
        try{
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e){
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    public function getAllCars($countryId, $cityId = 1, $locale = null)
    {
        list($token, $date) = $this->getRequestParams();
        if (!$locale){
            $loc = LaravelLocalization::getCurrentLocale();
        }else{
            $loc = $locale;
        }
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            //'packages' => true,
            'sorting' => 'asc',
            'type' => 'prices',
            'date' => $date,
            'region' => $cityId,
            'country' => $countryId,
            'lang' => $loc,
        ];
        $client = new GuzzleClient();
        try{
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e){
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    function getAllCarsCalculated($request)
    {
        list($token, $date) = $this->getRequestParams();
        $locale = $request->get('locale');
        if (!$locale){
            $loc = LaravelLocalization::getCurrentLocale();
        }else{
            $loc = $locale;
        }
        if (intval($request->get('placeStart'))){
            $placeStart = $request->get('placeStart');
        }else{
            $placeStart = json_decode($request->get('placeStart'))->city_id;
        }
        if (intval($request->get('placeFinish'))){
            $placeFinish = $request->get('placeFinish');
        }else{
            $placeFinish = json_decode($request->get('placeFinish'))->city_id;
        }
        if ($request->get('currency')){
            $curr = \GuzzleHttp\json_decode($request->get('currency'))->id;

        }else{
            $curr = 6; //доллар
        }
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => 'true',
//            'country' => self::COUNTRY,
            'type' => 'calculated',
            'sorting' => 'asc',
            'lang' => $loc,
            'place_time_data' => [
                'date_get' =>$request->get('dateStartFormatted'),
                'date_return' =>$request->get('dateFinishFormatted'),
                'time_get' =>$request->get('timeStartFormatted'),
                'time_return' =>$request->get('timeFinishFormatted'),
                'region_get' => $placeStart,
                'region_return' => $placeFinish,
            ],
            'curr' => $curr,
        ];
        $client = new GuzzleClient();
        try{
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e){
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        $contents = json_encode($contents);
        return $contents;
    }

    public function getNCars($amountCars)
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => true,
            'sorting' => 'asc',
            'paginate' => $amountCars,
            'type' => 'prices',
            'date' => $date,
            'country' => self::COUNTRY
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    public function getCarById($id)
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => true,
            'car_id' => $id,
            'type' => 'prices',
            'date' => $date,
            'country' => self::COUNTRY
        ];

        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $this->setImages($contents);
        return $contents;
    }

    public function getAllCarsByTrans($transmission)
    {
        if ($transmission === 'Все равно') {
            return json_encode($this->getAllCars());
        }else{
            list($token, $date) = $this->getRequestParams();
            $get_data = [
                'token' => $token,
                'domain' => self::DOMAIN,
                'packages' => true,
                'sorting' => 'asc',
                'type' => 'prices',
                'filters' => [
                    'trans_id' => [
                        $transmission
                    ],
                ],
                'date' => $date,
                'country' => self::COUNTRY
            ];
            $client = new GuzzleClient();
            try
            {
                $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
            } catch (\Exception $e)
            {
                $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
            }
            $contents = \GuzzleHttp\json_decode($response->getBody()->getContents());
            $this->setImages($contents);
            return \GuzzleHttp\json_encode($contents);
        }
    }

    public function getIntervals()
    {
       return  Intervals::where('domain_groups_id', 1)->get()->first();
    }

    function getTransmissions()
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => true,
            'type' => 'prices',
            'date' => $date,
            'country' => self::COUNTRY
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        $contents = $contents->filters->transes;
        return $contents;
    }

    function getCitiesForRegion()
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN_FOR_CITIES,
            'country' => self::COUNTRY,
            'mode' => 'names_only',
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/regions?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/regions?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    function getCitiesForRegionById($id, $locale = null)
    {
        if (!$locale){
            $loc = LaravelLocalization::getCurrentLocale();
        }else{
            $loc = $locale;
        }
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN_FOR_CITIES,
            'country' => $id,
            'mode' => 'names_only',
            'lang' =>  $loc,
        ];
        $client = new GuzzleClient();
        try
        {
          $response = $client->get('prices-api.pp.ua/regions?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/regions?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    function calculateCar($request)
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => 'true',
            'country' => self::COUNTRY,
            'type' => 'calculated',
            'place_time_data' => [
                'date_get' =>$request->get('dateStart'),
                'date_return' =>$request->get('dateFinish'),
                'time_get' =>$request->get('timeStart'),
                'time_return' => $request->get('timeFinish'),
                'region_get' =>3,//$request->get('placeStart'),
                'region_return' =>3,//$request->get('placeFinish'),
            ],
            'car_id' => $request->get('car_id'),
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = $response->getBody()->getContents();
        return $contents;
    }
    function getAddServices($request)
    {
        $request->get('curr');
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => 'true',
            'country' => self::COUNTRY,
            'type' => 'services',
            'place_time_data' => [
                'date_get' => $request->get('dateStart'),
                'date_return' => $request->get('dateFinish'),
                'time_get' => $request->get('timeStart'),
                'time_return' => $request->get('timeFinish'),
                'region_get' => 3,//$request->get('placeStart'),
                'region_return' => 3,//$request->get('placeFinish'),
            ],
            'car_id' => $request->get('car_id'),
            'lang' =>  $request->get('locale'),
            'curr' =>  $request->get('curr'),
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = $response->getBody()->getContents();
        return $contents;
    }

    public function getCarsByClasses()
    {
        list($token, $date) = $this->getRequestParams();
        $get_data = [
            'token' => $token,
            'domain' => self::DOMAIN,
            'packages' => true,
            'sorting' => 'asc',
            'type' => 'prices',
            'group_by' => 'true',
            'date' => $date,
            'country' => self::COUNTRY
        ];
        $client = new GuzzleClient();
        try
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        } catch (\Exception $e)
        {
            $response = $client->get('prices-api.pp.ua/catalog?'.http_build_query($get_data));
        }
        $contents = json_decode($response->getBody()->getContents());
        return $contents;
    }

    public function getKey()
    {
        list($token, $date) = $this->getRequestParams();
        return $token;
    }


    private function getRequestParams()
    {
        $api_token = PricesApiKeys::where('domain',1)->get()->first();
        $token = $api_token->token;
        $date_now = new \DateTime('now');
        $date_now_formatted = $date_now -> format('Y-m-d');
        return [$token, $date_now_formatted];
    }

    private function setImages(&$contents)
    {
            foreach ($contents->catalog as $car)
            {
                $path = '/images/cars/'.str_replace(' ', '_',$car->name_en.'_'.$car->sipp_name.'_'.$car->vg.'_'.($car -> ec*1000).'_'.$car->color_en).'.jpg';
                $car->image = $path;
            }
    }


}