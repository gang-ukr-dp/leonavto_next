<?php

namespace App\Services\Form;
use App\Services\ApiService;

class OrderService
{

    private $apiService;

    function __construct(ApiService $service)
    {
        $this->apiService = $service;
    }

    public function createMessage($request)
    {
        $comment = '';
        $car = \GuzzleHttp\json_decode($request->get('carModel'));
        $priceTotal = $request->get('priceTotal');
        $addOptionsPrice = $request->get('addOptionsPrice');
        $checkedOptions = \GuzzleHttp\json_decode($request->get('checkedOptions'));
        $comment = "Желаемое авто: " . $car->name . "\n";
        $comment .= "Предусмотренный пробег на период проката: " . "\n";
        $comment .= "Стоимость проката: " . $priceTotal . "\n";
        $comment .= "Экстра страховка: " . "\n";
        foreach ($checkedOptions as $option){
            $comment .= $option->name . ' ' . $option->price . 'GEL' . "\n";
        }
        $comment .= "Сумма заказа: (со скидкой): "  . $priceTotal . "/" . $addOptionsPrice . 'GEL' . "\n";
        return $comment;
    }

    public function createCertificate($request, $options, $orderNumber)
    {
        $car = \GuzzleHttp\json_decode($request->get('carModel'));
        $view1 = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body style="font-family: Verdana, Geneva, sans-serif;">
<table width="1118px">
   <tr>
      <td width="50%">
         <table width="100%" style="border-collapse: collapse;">
            <tr>
               <td colspan="3" valign="middle" style="height: 100px;background-color: #7398f7;color: #fff;font-size: 34px;text-align: center;">';

        $view1 .=  $car->name;

        $view2 = ' </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;font-size: 18px;" height="324px" valign="top">
                  <div style="">';

        $carImage = '<img src="'.$car->image.'" alt="" style="float: right;">';
        $carClass = ' <div class="" style="margin-top: 25px;">Класс: '.$car->class_name.'SUV</div>';
        $carSipp = ' <div class="" style="margin-top: 25px;">SIPP: '.$car->sipp_name.'</div>';
        $carPower = '<div class="" style="margin-top: 25px;">Мощность: '.$car->pow.'</div>';
        $carYear = '<div class="" style="margin-top: 25px;">Год выпуска: '.$car->vg.'</div>';
        $carTruck = '<div class="" style="margin-top: 25px;">Объем багажника: XXX л</div>';

        $view1 .= $view2.$carImage.$carClass.$carSipp.$carPower.$carYear.$carTruck;

        $view3 = ' <div class="" style="font-size:13px;margin-top: 45px;color: #929292;">(Заявка принимается на класс автомобиля. Марка, модель, характеристики конкретного автомобиля могут отличаться.)</div>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;" height="372px" valign="top">
                  <div style="border-top: 1px solid #7398f7;">
                     <div class="" style="font-weight: bold;font-size: 24px;padding-top: 22px;">Заказ включает в себя</div>
                     <table width="100%" style="padding-top: 10px;font-size: 18px">';

        $addOptions = '';
        return $options;
        foreach ($options->services as $option){
            $addOptions .= '<tr>
                           <td width="80%" style="padding-top: 5px; border-bottom: 1px dashed #000;">'.$option->alias.'</td>
                           <td width="20%" style="text-align:right;padding-top: 5px; border-bottom: 1px dashed #000;">'.$option->price.'$</td>
                        </tr>';
        }

        $priceTotal = '<tr>
                           <td width="80%" style="padding-top: 22px;font-size: 24px;">
                              <span style="font-weight: bold;">Сумма заказа:</span>
                              <span style="padding-top: 22px;font-size: 13px;color: #929292;">(За весь период аренды)</span>
                           </td>
                           <td width="20%" style="text-align:right;padding-top: 22px;font-size: 29px;font-weight: bold;">'.$request->get('priceTotal').'$</td>
                        </tr>';
        $view1 .= $view3.$addOptions.$priceTotal;

        $view4 = ' </table>
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;" height="158px" valign="top">
                  <div style="border-top: 1px solid #7398f7;">
                     <div class="" style="font-size: 24px;font-weight: bold;margin-top: 10px;">Контакты:</div>
                     <div class="" style="margin-top: 15px;font-size: 18px;">улица Сортировочная, 20, Киев, Украина</div>
                  </div>
               </td>
            </tr>
         </table>
      </td>
      <td width="50%">
         <table width="100%" style="border-collapse: collapse;">
            <tr>
               <td colspan="3" valign="middle" style="height: 100px;background-color: #7398f7;color: #fff;font-size: 34px;text-align: center;">';

        $orderNumber = $orderNumber;

        $view1 .=$view4.$orderNumber;


        $view5 = '  </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;font-size: 18px;" height="324px" valign="top">
            <div style="">
             <div class="" style="font-weight: bold;margin-top: 10px;">Подача</div>';

        $placeStart = '<div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/marker.png" alt="">
                        '.$request->get('placeStart').'
                     </div>';
        $dateStart = ' <div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/calendar.png" alt="">
                       '.$request->get('dateStart').'
                     </div>';
        $timeStart = '<div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/time.png" alt="">
                        '.$request->get('timeStart').'
                     </div>';
        $view6 = '  </div>
                  <div style="">
                     <div class="" style="font-weight: bold;margin-top: 26px;">Возврат</div>';
        $placeFinish = ' <div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/marker.png" alt="">
                       '.$request->get('placeFinish').'
                     </div>';
        $dateFinish = ' <div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/calendar.png" alt="">
                       '.$request->get('dateFinish').'
                     </div>';
        $timeFinish = '<div class="" style="margin-top: 12px;">
                        <img style="margin-bottom: -5px;" src="//autodin.com.ua/image/voucher/time.png" alt="">
                        '.$request->get('timeFinish').'
                     </div>';

        $view1 .= $view5.$placeStart.$dateStart.$timeStart.$view6.$placeFinish.$dateFinish.$timeFinish;

        $view7 = '</div>
               </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;" height="372px" valign="top">
                  <div style="border-top: 1px solid #7398f7;">
                     <div class="" style="font-weight: bold;font-size: 24px;margin-top: 20px;">Персональные данные</div>
             ';

        $clientName = ' <div class="" style="font-size: 24px;margin-top: 20px;"> '.$request->get('userName').'</div>';
        $clientPhone = '<div class="" style="margin-top: 20px;">
                        <span style="text-decoration: none;color: #000;font-size: 18px;">'.$request->get('phone').'</span>
                     </div>';
        $clientEmail = '<div class="" style="margin-top: 20px;">
                        <span style="text-decoration: none;color: #000;font-size: 18px;">'.$request->get('email').'</span>
                     </div>';

        $view1 .= $view7.$clientName.$clientPhone.$clientEmail;

        $view8 = '  <div class="" style="margin-top: 20px;text-align: center;font-style:italic;">Мы ожидаем Вашу оплату 22.02.1012 <br> <br> <br>
                        <a href="" style="padding: 15px 20px;background-color: #6fc5bb;font-style: normal;text-decoration: none;color: #fff;font-weight:bold; ">Оплатить сейчас</a>
                     </div>
                     <div class="" style="margin-top: 30px;font-size: 18px;color: #6fc5bb;">Для получения автомобля не забудьте взять паспорт, водительское удостоверение и деньги</div>
                  
                  </div>
               </td>
            </tr>
            <tr>
               <td colspan="3" style="background-color: #fefaea;padding-right: 16px;padding-left: 16px;" height="158px" valign="top">
                  <div style="border-top: 1px solid #7398f7;">
                     <table style="font-size: 24px;line-height: 50px;" width="100%">
                        <tr>
                           <td width="30%" style="font-weight: bold;">Email:</td>
                           <td width="70%" style="text-align: right;">
                              <a href="mailto:order@autodin.com.ua" style="color: #000;text-decoration: none;">order@rentacar.dp.ua</a>
                           </td>
                        </tr>
                        <tr>
                           <td width="30%" style="font-weight: bold;">Телефон:</td>
                           <td width="70%" style="text-align: right;" valign="top">
                              <a href="tel:+380676442644" style="color: #000;text-decoration: none;">+38-067-644-2-644</a>
                           </td>
                        </tr>
                     </table>
                  </div>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
</body>
</html>';
        $view1 .= $view8;
        return $view1;
    }


}