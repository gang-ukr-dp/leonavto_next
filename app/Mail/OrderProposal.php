<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderProposal extends Mailable
{
    use Queueable, SerializesModels;

    public $car;
    public $request;

    public function __construct($car, $request)
    {
        $this->car = $car;
        $this->request = $request;
    }


    public function build()
    {
        return $this->from( 'dima.maimesko@gmail.com', 'LION')
            ->subject('Car Rent Proposal(' . $this->car->name . ')')
            ->view('email.propose_mail')
            ->with([
                'car' => $this->car,
                'request' => $this->request,
            ]);
    }
}
