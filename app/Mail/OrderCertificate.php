<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCertificate extends Mailable
{
    use Queueable, SerializesModels;

    public $car;
    public $request;
    public $options;
    public $currencyRate;
    public $currencyImage;
    public $clock = '🕒';
    public $calendar = '📅';


    public function __construct($car, $request, $options, $currencyRate, $currencyImage)
    {
        $this->car = $car;
        $this->request = $request;
        $this->options = $options;
        $this->currencyRate = $currencyRate;
        $this->currencyImage = $currencyImage;
    }


    public function build()
    {
        $name = $this->request->input('timeFinish');


        return $this->from( 'hireauto.biz@gmail.com', '🚘 LION AVTOPROKAT')
            ->subject('Order from lion-avtoprokat.com.ua')
            ->view('email.certificate')
            ->with([
                'car' => $this->car,
                'request' => $this->request,
                'options' => $this->options,
                'iconClock' => $this->clock,
                'iconCalendar' => $this->calendar,
                'currencyRate' => $this->currencyRate,
                'currencyImage' => $this->currencyImage,
            ]);
    }
}
