<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddOrderCertificate extends Mailable
{
    use Queueable, SerializesModels;

    public $certificate;


    public function __construct($certificate)
    {
        $this->certificate = $certificate;

    }


    public function build()
    {
        return $this->from( 'dima.maimesko@gmail.com', 'LION')
            ->subject('Car Rent Certificate')
            ->view('email.addcertificate')
            ->with([
                'certificate' => $this->certificate,

            ]);
    }
}
