<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;

## or
use SEO;


class ArticleController extends Controller
{
    public function index()
    {

        $title = "This is a blog title";
        $description ="This is a discription of my blog post";
        $body        = "This is main body of my blog post";
        $keywords    = "This is keywords, keywords, keywords, keywords";

        SEO::setTitle($title);
        SEO::setDescription($description);
        SEO::setKeywords($keywords);

        return view('app', compact('posts'));
        //return view('article',compact('title','description','body'));
    }
 /*   public function article()
    {
        SEOMeta::setTitle('Home');
        SEOMeta::setDescription('This is my page description');
        SEOMeta::setCanonical('https://codecasts.com.br/lesson');

        OpenGraph::setDescription('This is my page description');
        OpenGraph::setTitle('Home');
        OpenGraph::setUrl('http://current.url.com');
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle('Homepage');
        Twitter::setSite('@LuizVinicius73');

        ## Or

        SEO::setTitle('Home');
        SEO::setDescription('This is my page description');
        SEO::opengraph()->setUrl('http://current.url.com');
        SEO::setCanonical('https://codecasts.com.br/lesson');
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite('@LuizVinicius73');

        $posts = Post::all();

        return view('app', compact('posts'));
    }*/
}