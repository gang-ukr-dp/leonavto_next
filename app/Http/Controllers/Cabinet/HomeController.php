<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CabinetService;
use Session;
class HomeController extends Controller
{
    private $cabinetService;


    public function __construct(CabinetService $cabinetService)
    {
        $this->cabinetService = $cabinetService;
    }


    public function index()
    {
        $ordersList = $this->cabinetService->getOrdersList();
//        dd($ordersList);
        return view('cabinet.home', [
            'orders' => $ordersList
        ]);
    }


}
