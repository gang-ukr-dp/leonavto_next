<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AutoparkController extends Controller
{

    public function country($country)
    {
        return view('country',
            [
                'country' => $country,
            ]);
    }
}
