<?php

   namespace App\Http\Controllers\Auth\Api;
   
   use Illuminate\Http\Request;
   use Illuminate\Support\Facades\Validator;
   use App\Services\CabinetService;
   use Session;
   class AuthController
   {
       private $cabinetService;


       public function __construct(CabinetService $cabinetService)
       {
           $this->cabinetService = $cabinetService;
       }

       public function showRegistrationForm()
       {
           return view('auth.register');
       }

       public function showLoginForm()
       {
           return view('auth.login');
       }


       public function register(Request $request)
       {
           $username = 'LION__'.$request -> email;
           $password = $request -> password;
           $this->validateRegister($request->all())->validate();
           try {
               $contents = $this->cabinetService->register($request->all());
           }
           catch (\GuzzleHttp\Exception\RequestException $e) {
               return redirect() -> route('main') -> with('status', 'Что-то пошло не так! Обратитесь в техническую поддержку');
           }
           if (($contents == NULL) || (!array_key_exists('id', $contents))){
               return redirect() -> route('main') -> with('danger', 'Что-то пошло не так! Обратитесь в техническую поддержку'.'Error:'.$contents);
           }else{
               $this->login();
               return redirect() -> route('api.login-form') -> with('success', 'Регистрация успешна.');
           }
       }


       public function login(Request $request)
       {
           $username = 'LION__'.$request -> email;
           $password = $request -> password;
           $access_token = $this->cabinetService->token($username, $password);
           if ($access_token){
               Session::put('access_token', $access_token);
               Session::put('username', $username);
               Session::put('password', $password);
               $userInfo = $this->cabinetService->userInfo($access_token);
               if ($userInfo){
                   Session::put('usernameToShow', $userInfo->user->first_name);
               }
               return redirect() -> route('cabinet.home')->with('success', 'Добро пожаловать на сайт, '.$userInfo->user->first_name.'!');
           }else{
               return redirect() -> route('api.login-form') -> with('status', 'Неверное имя пользователя или пароль');
           }
       }


       public function logout()
       {
           Session::forget('access_token');
           Session::forget('username');
           Session::forget('password');
           Session::forget('usernameToShow');
           return redirect() -> route('api.login-form') -> with('warning', 'Вы разлогинены!');
       }

       protected function validateRegister(array $data)
       {
           return Validator::make($data, [
               'first_name' => 'required|string|max:255',
               'second_name' => 'required|string|max:255',
               'last_name' => 'required|string|max:255',
               'birthday' => 'required',
               'phone' => 'required',
               'password' => 'required|string|min:6|confirmed',
               'email' => 'required|string|email|max:255',
           ]);
       }





   }