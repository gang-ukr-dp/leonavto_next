<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Services\ApiService;
use Artesaos\SEOTools\Facades\SEOMeta;
use App\Models\CountryForPages;
use App\Models\Conditions;
use App\Models\CityForPages;
use App\Models\Places;
use App;
use App\Models\Order;
use App\Mail\AddOrderCertificate;
use Mail;
use App\Models\Currency;
use App\Models\Regions;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class MainController extends Controller
{
    const NUM_CARS = 6;
    private $apiService;
    private $countriesForNav;
    private $regionsInMain;

    public function __construct(ApiService $service)
    {
        $this->apiService = $service;
        $this->countriesForNav = config('siteparams.countries_for_nav');
        $this->regionsInMain = config('siteparams.cities_in_main');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $title = 'title';
        $description ="description";
        $keywords    = "keywords";
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }

        SEOMeta::setTitle(__('seo-title.main'));
        SEOMeta::setDescription(__('seo-title.main-description'));
        SEOMeta::setKeywords(__('seo-title.main-keywords'));

//        SEOMeta::setDescription(__('car.order_btn'));

        return view('main',
            [
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'regionsInMain' => Regions::getRequiredRegions($this->regionsInMain),
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function preview()
    {
        return view('preview', [
            'allCurrencies' => Currency::all()->toArray(),
            'requiredCurrencies' => config('siteparams.currencies'),
        ]);
    }

    /**
     * @param $country
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @text похоже не работает
     */
    public function country($country, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }
        $cities = $this->apiService->getCitiesForRegionById($request->get('id'));
        $cities = json_decode(json_encode($cities), true);
        if ($cities) ksort($cities, SORT_STRING);

        $citiesForSlack = $this->apiService->getCitiesForRegionById($request->get('id'), 'en');
        $citiesForSlack = json_decode(json_encode($citiesForSlack), true);
        if ($citiesForSlack) {
            ksort($citiesForSlack, SORT_STRING);
            $citiesOfficesIdForSlack = self::getCitiesOfficesId($citiesForSlack);
        } else {
            $citiesOfficesIdForSlack = null;
        }


        return view('country',
            [
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'cities' => $cities,
                'citiesOfficesId' => self::getCitiesOfficesId($cities),
                'citiesOfficesIdForSlack' => $citiesOfficesIdForSlack,
                'country' => Country::where('id', '=', $request->get('id'))->get()->first(),
                'countryForPages' => CountryForPages::where('country_id', '=', $request->get('id'))->with('country')->get()->first(),
                'presetFormParams' => '',
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);
    }

    /**
     * @param null $country
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function autoparkCountry($country = null, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
            $curr = $request->session()->get('currencyId');
        } else {
            $curr = 1;
        }
        $id = $request->get('id');

        SEOMeta::setTitle(__('seo-title.autopark-country-'.$country));
        //SEOMeta::setDescription('MyDescription');
        $country = $request->get('id');


        if ($country == 1) {
            $result = $this->apiService->getAllCarsOfCountry($id, LaravelLocalization::getCurrentLocale(), $curr);
        }
        if ($country == 3) {
            $result = $this->apiService->getAllCarsOfCity($id, 3, LaravelLocalization::getCurrentLocale(), $curr);//11 это из regions
        }

        if ($country == 4) {
            $result = $this->apiService->getAllCarsOfCity($id, 4, LaravelLocalization::getCurrentLocale(), $curr);
            //$result = $this->apiService->getAllCarsOfCountry($id, 'ua', $curr);
        }

        if ($country == 11) {
            $result = $this->apiService->getAllCarsOfCountry($id, LaravelLocalization::getCurrentLocale(), $curr);
        }


        $allCars = $result->catalog;
        //dd($allCars);
        $allCars = self::deleteUnpricedCars($allCars);
        $allFilters = $result->filters;
        $cities = $this->apiService->getCitiesForRegionById($id);
        $cities = json_decode(json_encode($cities), true);
        if ($cities) {
            ksort($cities, SORT_STRING);
            $citiesOfficesId = self::getCitiesOfficesId($cities);
        } else {
            $citiesOfficesId = null;
        }

        $citiesForSlack = $this->apiService->getCitiesForRegionById($id, 'en');
        $citiesForSlack = json_decode(json_encode($citiesForSlack), true);
        if ($citiesForSlack) {
            ksort($citiesForSlack, SORT_STRING);
            $citiesOfficesIdForSlack = self::getCitiesOfficesId($citiesForSlack);
        } else {
            $citiesOfficesIdForSlack = null;
        }

        $intervals = $this->apiService->getIntervals();
        $token = $this->apiService->getKey();
        return view('autopark',
            [
                'presetCity' => '',
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'cities' => $cities,
                'citiesOfficesId' => $citiesOfficesId,
                'citiesOfficesIdForSlack' => $citiesOfficesIdForSlack,

                'country' => Country::where('id', '=', $id)->get()->first(),
                'presetFormParams' => '',
                'token' => $token,
                'carsNumber' => self::NUM_CARS,
                'allCars' => $allCars,
                'allFilters' => $allFilters,
                'intervals' => $intervals,
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);
    }

    /**
     * @param null $country
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function autoparkCalculated($country = null, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
            $curr = $request->session()->get('currencyId');
        } else {
            $curr = 6;
        }
        SEOMeta::setTitle(__('seo-title.autopark-country-'.$country));
//        SEOMeta::setDescription('MyDescription');
        $id = $request->get('id');
        $result = json_decode($this->apiService->getAllCarsCalculated($request));
        $allCars = $result->catalog;
        $allCars = self::sortByPriceAsc($allCars);
        $allFilters = $result->filters;
        $request = $request->all();
        $cities = $this->apiService->getCitiesForRegionById($request['countryId']);
        $cities = json_decode(json_encode($cities), true);
        if ($cities) {
            ksort($cities, SORT_STRING);
            $citiesOfficesId = self::getCitiesOfficesId($cities);
        } else {
            $citiesOfficesId = null;
        }

        $citiesForSlack = $this->apiService->getCitiesForRegionById($request['countryId'], 'en');
        $citiesForSlack = json_decode(json_encode($citiesForSlack), true);
        if ($citiesForSlack) {
            ksort($citiesForSlack, SORT_STRING);
            $citiesOfficesIdForSlack = self::getCitiesOfficesId($citiesForSlack);
        } else {
            $citiesOfficesIdForSlack = null;
        }

        $intervals = $this->apiService->getIntervals();
        $token = $this->apiService->getKey();
        return view('autopark',
            [
                'presetCity' => '',
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'cities' => $cities,
                'citiesOfficesId' => $citiesOfficesId,
                'citiesOfficesIdForSlack' => $citiesOfficesIdForSlack,
                'country' => Country::where('name_country_en', '=', $request['countryName'])->get()->first(),
                'presetFormParams' => $request,
                'token' => $token,
                'carsNumber' => self::NUM_CARS,
                'allCars' => $allCars,
                'allFilters' => $allFilters,
                'intervals' => $intervals,
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);

    }

    /**
     * @param $country
     * @param $city
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function autoparkCity($country, $city, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
            $curr = $request->session()->get('currencyId');
        } else {
            $curr = 6;
        }
        SEOMeta::setTitle(__('seo-title.autopark-city-'.$city ));
//        SEOMeta::setDescription('MyDescription');
        $country_id = $request->get('country_id');
        $city_id = $request->get('city_id');
        $result = $this->apiService->getAllCarsOfCity($country_id, $city_id, LaravelLocalization::getCurrentLocale(), $curr);

        $allCars = $result->catalog;
        $allCars = self::deleteUnpricedCars($allCars);
        $allFilters = $result->filters;

        $cities = $this->apiService->getCitiesForRegionById($country_id);
        $cities = json_decode(json_encode($cities), true);
        if ($cities) {
            ksort($cities, SORT_STRING);
            $citiesOfficesId = self::getCitiesOfficesId($cities);
        } else {
            $citiesOfficesId = null;
        }

        $citiesForSlack = $this->apiService->getCitiesForRegionById($country_id, 'en');
        $citiesForSlack = json_decode(json_encode($citiesForSlack), true);
        if ($citiesForSlack) {
            ksort($citiesForSlack, SORT_STRING);
            $citiesOfficesIdForSlack = self::getCitiesOfficesId($citiesForSlack);
        } else {
            $citiesOfficesIdForSlack = null;
        }

        $intervals = $this->apiService->getIntervals();
        $token = $this->apiService->getKey();
        return view('autopark',
            [
                'presetCity' => Regions::where('id', $city_id)->get()->first(),
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'cities' => $cities,
                'citiesOfficesId' => $citiesOfficesId,
                'citiesOfficesIdForSlack' => $citiesOfficesIdForSlack,
                'country' => Country::where('id', '=', $country_id)->get()->first(),
                'presetFormParams' => '',
                'token' => $token,
                'carsNumber' => self::NUM_CARS,
                'allCars' => $allCars,
                'allFilters' => $allFilters,
                'intervals' => $intervals,
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);


    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts(Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }
		$locale = LaravelLocalization::getCurrentLocale();

		switch($locale){
			case 'ua':
                $lang = '';
				break;
			case 'ru':
				$lang = '_ru';
				break;
			case 'en':
				$lang = '_en';
				break;
		}
        $offices = Places::where('regions_id', '!=', null)
			->select('place'.$lang.' as place','place_descr'.$lang.' as place_descr','place_adress'.$lang.' as place_adress', 'country_id','region','latitude','longitude','GMT','phone1','phone1_viber','phone1_whatsapp','phone2','phone2_viber','phone2_whatsapp','phone3','phone3_viber','phone3_whatsapp','skype','mail','regions_id','type')
            ->where('type', 0)
            ->where('country_id', $request->get('id'))->orderBy('place', 'ASC')
            ->get();
        return view('contacts',
            [
                'id' => $request->get('id'),
                'offices' => $offices,
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'country' => Country::where('id', $request->get('id'))->get()->first(),
            ]);
    }

    /**
     * @param $country
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function conditions($country, Request $request)
    {

        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }
        $cities = $this->apiService->getCitiesForRegionById($request->get('id'));
        $cities = json_decode(json_encode($cities), true);
        if ($cities) ksort($cities, SORT_STRING);


        $locale = LaravelLocalization::getCurrentLocale();

        $condition = Conditions::where('country_id', '=', $request->get('id'))->with('country')->get()->first();

        switch($locale){
            case 'ua':
                $condition->content = $condition->content_ua;
                break;
            case 'ru':
                break;
            case 'en':
                $condition->content = $condition->content_en;
                break;
        }


       // var_dump( $condition );

        return view('conditions',
            [
                'countries' => Country::getRequiredCountries($this->countriesForNav),
                'cities' => $cities,
                'country' => Country::where('id', '=', $request->get('id'))->get()->first(),
                'conditions' => $condition,
                'presetFormParams' => '',
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
                'countries' => Country::getRequiredCountries($this->countriesForNav),
            ]);
    }

    /**
     * @param $cities
     * @return array
     */
    private function getCitiesOfficesId($cities)
    {
        $citiesOfficesId = [];
        foreach ($cities as $city => $cityPlaces) {
            ksort($cityPlaces);
            $firstValue = array_values($cityPlaces)[0];
            $firstKey = array_keys($cityPlaces)[0];
            $citiesOfficesId[$firstKey] = $firstValue;
        }
        return $citiesOfficesId;
    }

    /**
     * @param $orderNumber
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCertificate($orderNumber, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }
        $order = Order::where('num', '=', $orderNumber)->get()->first();
        $view = $order->sertificat;

        return view('certificate', [
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendAnotherEmail(Request $request, Request $request)
    {
        if ($request->session()->has('currencyId')) {
            session(['currencyId' => $request->session()->get('currencyId')]);
        }
        $orderNumber = $request->get('order_number');
        $order = Order::where('num', '=', $orderNumber)->get()->first();
        $view = $order->sertificat;

        Mail::to($request->get('email'))->send(new AddOrderCertificate($view));
        return response()->json([
            'orderNumber' => $orderNumber,
        ]);
    }

    /**
     * @param $cars
     * @return array
     */
    private static function deleteUnpricedCars($cars)
    {
        $filteredCars = [];
        foreach ($cars as $car) {
            if ($car->main_10 !== '0') {
                $filteredCars[] = $car;
            }
        }
        return $filteredCars;
    }

    /**
     * @param $catalog
     * @return mixed
     */
    private static function sortByPriceAsc($catalog)
    {
//        dd($catalog);
        return $catalog;
    }


}
