<?php

namespace App\Http\Controllers;

use App\Mail\OrderCertificate;
use App\Mail\OrderProposal;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Services\ApiService;
use App\Services\Form\OrderService;
use App\Models\Country;
use Mail;
use View;
use Session;


class AjaxController extends Controller
{
    private $apiService;
    private $orderService;

    /**
     * AjaxController constructor.
     * @param ApiService $service
     * @param OrderService $orderService
     */
    public function __construct(ApiService $service, OrderService $orderService)
    {
        $this->apiService = $service;
        $this->orderService = $orderService;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getCities(Request $request)
    {
        $cities = $this->apiService->getCitiesForRegionById($request->get('countryId'), $request->get('locale'));
        $cities = json_decode(json_encode($cities), true);
        return [
                'cities' => $cities,
            ];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getClasses(Request $request)
    {
        $cities = $this->apiService->getCitiesForRegionById($request->get('countryId'));
        $cities = json_decode(json_encode($cities), true);
        return [
                'cities' => $cities,
            ];
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getAddServices(Request $request)
    {
        return $services = $this->apiService->getAddServices($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function calculateForm(Request $request)
    {
        return $this->apiService->calculateCar($request);
    }

    /**
     * @param Request $request
     * @return false|mixed|string
     */
    public function getCalculatedCars(Request $request)
    {
        return $this->apiService->getAllCarsCalculated($request);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @name
     * @uses
     * @var {car: {…}, request: {…}, options: Array(4), orderNumber: "190528093746"}
     */
    public function sendForm(Request $request)
    {
        $country = json_decode($request->get('country'));
        $car = json_decode($request->get('carModel'));
        if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
            $origin = $_SERVER['HTTP_ORIGIN'];
        } else if (array_key_exists('HTTP_REFERER', $_SERVER)) {
            $origin = $_SERVER['HTTP_REFERER'];
        } else {
            $origin = $_SERVER['REMOTE_ADDR'];
        }
        $comment = $this->orderService->createMessage($request);
        $orderNumber = date('ymdHis');
        $request->request->add(['car_id' => $car->id]);
        $request->request->add(['order_number' => $orderNumber]);
        $options = json_decode($request->get('checkedOptions'));
//      dd($request->get('order_number'));
        $view = View::make('email.certificate', [
            'car' => $car,
            'request' => $request,
            'options' => $options,
            'currencyRate' => $request->get('currencyRate'),
            'currencyImage' => $request->get('currencyImage'),
        ]);
        $certificate = (string)$view;
        Order::create([
            'group' => 0,
            'num' => $orderNumber,
            'country_start' => $country->name_country,
            'country_end' => $country->name_country,
            'period_start' => $request->get('dateStart'),
            'period_end' => $request->get('dateFinish'),
            'town_start' => json_decode($request->get('placeStart'))->city_location,
            'town_end' => json_decode($request->get('placeFinish'))->city_location,
            'time_start' => $request->get('timeStart'),
            'time_end' => $request->get('timeFinish'),
            'klass' => $car->class_name,
            'auto' => $car->name,
            'autos_name' => '',
            'transmission' => $car->trans_name,
            'fio' => $request->get('userName'),
            'tel' => $request->get('phone'),
            'mail' => $request->get('email'),
            'ip' => $_SERVER['REMOTE_ADDR'],
            'created' => date("Y-m-d H:i:s"),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
            'login' => "",
            'status' => 0,
            'vykonano' => 0,
            'feedback' => 0,
            'feedback_message' => '',
            'source' => $origin,
            'comment' => $comment,
            'sertificat' => $certificate,
            'message' => '',
            "clients_fio" => 0,
            "agreements_key" => 0,
            "reservation_id" => 0,
            "del" => 0,
            "fio_del" => "",
            "login_edit" => '',
            "time_edit_start" => date("Y-m-d H:i:s"),
            "time_edit" => date("Y-m-d H:i:s"),
            "time_edit_activ" => date("Y-m-d H:i:s"),
            "redact_on" => 0,
        ]);
        Mail::to($request->get('email'))->send(new OrderCertificate($car, $request, $options, $request->get('currencyRate'), $request->get('currencyImage')));
        // отправка E-mail
        // Mail::to($request->get('email'))->send(new OrderCertificate($car,$request,$options));
        return response()->json([
            'car' => $car,
            'request' => $request->all(),
            'options' => $options,
            'orderNumber' => Order::where('num', '=', $orderNumber)->get()->first()->num,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendOffer(Request $request)
    {
        $car = json_decode($request->get('carModel'));
        Mail::to($request->get('email'))->send(new OrderProposal($car, $request));
        return response()->json([
            'success' => true,
        ]);
    }

    public function getAllCars(Request $request)
    {
        $countryId = $request->get('country_id');
        $cityId = $request->get('city_id');
        $locale = $request->get('locale');
        $curr = $request->get('curr');
        return \GuzzleHttp\json_encode($this->apiService->getCars($countryId, $cityId, $locale, $curr));
    }
    public function getAllCarsOfCountry(Request $request)
    {
        $country = $request->get('country_id');
        $locale = $request->get('locale');
        $curr = $request->get('curr');
        if ($country == 1){
            $result = $this->apiService->getAllCarsOfCountry($country, 'ua', $curr);
        }
        if ($country == 3){
            $result = $this->apiService->getAllCarsOfCity($country, 11, 'ua', $curr);//11 это из regions
        }
        $allCars = $result->catalog;
        $allCars = self::deleteUnpricedCars($allCars);
        $result->catalog = $allCars;
        return \GuzzleHttp\json_encode($result);
    }

    public function getAllCarsOfCity(Request $request)
    {
        $countryId = $request->get('country_id');
        $cityId = $request->get('city_id');
        $locale = $request->get('locale');
        $curr = $request->get('curr');
        $result = $this->apiService->getAllCarsOfCity($countryId, $cityId, $locale, $curr);
        $allCars = $result->catalog;
        $allCars = self::deleteUnpricedCars($allCars);
        $result->catalog = $allCars;
        return \GuzzleHttp\json_encode($result);
    }


    public function writeCurrencyToSession(Request $request)
    {
        session(['currencyId' => $request->get('currencyId')]);
        return 'true';
    }

    private static function deleteUnpricedCars($cars)
    {
        $filteredCars = [];
        foreach ($cars as $car){
            if ($car->main_10 !== '0'){
                $filteredCars[] = $car;
            }
        }
        return $filteredCars;
    }

}
