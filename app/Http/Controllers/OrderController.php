<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Services\ApiService;
use Artesaos\SEOTools\Facades\SEOMeta;
use App\Models\Currency;
use Session;

class OrderController extends Controller
{
    const NUM_CARS = 6;
    private $apiService;
    private $countriesForNav;

    public function __construct(ApiService $service)
    {
        $this->apiService = $service;
        $this->countriesForNav = config('siteparams.countries_for_nav');
    }

    public function index(Request $request)
    {
        if ($request->session()->has('currencyId')){
            session(['currencyId' => $request->session()->get('currencyId')]);
            $curr = $request->session()->get('currencyId');
        }else{
            $curr = 6;
        }

        SEOMeta::setTitle(__('seo-title.main'));
        SEOMeta::setDescription(__('seo-title.main-description'));
        SEOMeta::setKeywords(__('seo-title.main-keywords'));

        $currName = Currency::where('id', $curr)->pluck('name_currency')->first();
        $currencyRate = Country::getCurrencyRateByName($currName);
        return view('order',
            [
                'currencyRate' => $currencyRate,
                'car' => $request->get('car'),
                'allCurrencies' => Currency::all()->toArray(),
                'requiredCurrencies' => config('siteparams.currencies'),
            ]);
    }






}
