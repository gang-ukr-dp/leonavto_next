<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesForPagesTable extends Migration
{
    public function up()
    {
        Schema::create('countries_for_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country_id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('head')->nullable();
            $table->text('content')->nullable();
            $table->string('url')->nullable();
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries_for_pages');
    }
}
