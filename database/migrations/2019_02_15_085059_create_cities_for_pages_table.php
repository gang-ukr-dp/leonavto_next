<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesForPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities_for_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('city_name');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('head')->nullable();
            $table->text('content')->nullable();
            $table->string('url')->nullable();
            $table->string('slug')->nullable();
            $table->integer('country_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities_for_pages');
    }
}
