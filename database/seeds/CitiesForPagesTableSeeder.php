<?php
use Illuminate\Database\Seeder;
use App\Models\CityForPages;
use App\Models\Country;
use App\Services\ApiService;

class CitiesForPagesTableSeeder extends Seeder
{

    private $apiService;

    public function __construct(ApiService $service)
    {
        $this->apiService = $service;
    }

    public function run()
    {
        $countriesIds = config('siteparams.countries_for_nav');
        foreach ($countriesIds as $key => $id)
        {
            $citiesForRegion = $this->apiService->getCitiesForRegionById($id);
            $country = Country::find($id);
            if ($citiesForRegion){
                foreach($citiesForRegion as $cityForRegion => $placesInCity){
                    $city = new CityForPages();
                    $city->city_name = $cityForRegion;
                    $city->title = 'Title for '.$cityForRegion;
                    $city->description = 'Description for '.$cityForRegion;
                    $city->head = 'Head for '.$cityForRegion;
                    $city->content = '<h3>Среди услуг, которые не входят в основную стоимость и предоставляются за дополнительную плату можно указать следующие:</h3>
			<ol>
				<li>Доставка и подбор автомобилей вне офиса – на ж/д вокзалах, аэропортах, адресах указанных клиентом.</li>
				<li>Доставка и приём автомобиля в сверхурочное время – когда наши офисы закрыты, вы все равно можете воспользоваться автомобилем – можно получить и сдать автомобиль в удобное вам время.</li>
				<li>Услуги водителя – если вам необходимы услуги водителя, водитель на час – мы всегда готовы помочь в этом – наши профессиональные водители, которые имеют не один год безаварийного вождения, прошли курсы контраварийного вождения – всегда готовы помочь в этом.</li>
				<li>Трансферы – услуги по встрече гостей в аэропортах и на ж/д вокзалах с доставкой в указанное место.</li>
				<li>Дополнительные страховки – дополнительное страховое покрытие – если вы хотите иметь наименьшую ответственность при управлении автомобилем можно всегда доплатить за повышенное страховое покрытие.</li>
				<li>Аренда навигаторов – дополнительное оборудование для отличного ориентирования на украинских автодорогах.</li>
				<li>Детские кресла – если ваша поездка осуществляется с ребенком – есть возможность арендовать детские кресла.</li>
				<li>Безлимитный пробег – при необходимости можно оплатить услуги по прокату авто без ограничения пробега.</li>
			</ol>';
                    $city->url = '/city/'.str_slug($cityForRegion, '-');;
                    $city->slug = str_slug($cityForRegion, '-');
                    $city->country()->associate($country);
                    $city->save();
                }
            }

        }


    }
}

