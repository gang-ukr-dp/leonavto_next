<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CitiesForPagesTableSeeder::class);
         $this->call(CountriesForPagesTableSeeder::class);
         $this->call(CountryConditionsTableSeeder::class);
    }
}
