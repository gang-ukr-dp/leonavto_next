<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/register-form', 'Auth\Api\AuthController@showRegistrationForm')->name('api.register-form');
Route::post('/register', 'Auth\Api\AuthController@register')->name('api.register');
Route::get('/login-form', 'Auth\Api\AuthController@showLoginForm')->name('api.login-form');
Route::post('/login', 'Auth\Api\AuthController@login')->name('api.login');
Route::post('/logout', 'Auth\Api\AuthController@logout') -> name('api.logout');

Route::group(
    [
        'prefix' => 'cabinet',
        'as' => 'cabinet.',
        'namespace' => 'Cabinet',
//        'middleware' => ['auth'],
    ],
    function () {
      Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
    /** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
    Route::get('/', 'MainController@index')->name('main');

    Route::get('/country/{country}', 'MainController@country')->name('country');

    Route::get('/autopark-calc', 'MainController@autoparkCalculated')->name('autopark.calc');
    Route::get('/autopark-country/{country}', 'MainController@autoparkCountry')->name('autopark.country');
    Route::get('/autopark-city/{country}/{city}', 'MainController@autoparkCity')->name('autopark.city');

    Route::get('/contacts/{country}', 'MainController@contacts')->name('contacts');
    Route::get('/conditions/{country}', 'MainController@conditions')->name('conditions');

    Route::get('/order-form', 'OrderController@index')->name('order.form');
});


Route::get('/cities', 'AjaxController@getCities')->name('cities');
Route::get('/get-add-services', 'AjaxController@getAddServices')->name('get.add.services');
Route::post('/calculate-form', 'AjaxController@calculateForm')->name('calculate.form');
Route::get('/get-calculated-cars', 'AjaxController@getCalculatedCars')->name('get.calculated.cars');
Route::get('/get-all-cars', 'AjaxController@getAllCars')->name('get.all.cars');
Route::get('/get-all-cars-of-country', 'AjaxController@getAllCarsOfCountry')->name('get.all.cars.of.country');
Route::get('/get-all-cars-of-city', 'AjaxController@getAllCarsOfCity')->name('get.all.cars.of.city');
Route::get('/write-currency-to-session', 'AjaxController@writeCurrencyToSession')->name('write.currency.to.session');

Route::post('/send-form', 'AjaxController@sendForm')->name('send.form');
Route::post('/send-offer', 'AjaxController@sendOffer')->name('send.offer');

Route::get('/preview', 'MainController@preview')->name('preview');

Route::get('/show-certificate/{order}', 'MainController@showCertificate')->name('show.certificate');
Route::post('/send-another-email', 'MainController@sendAnotherEmail')->name('send.another.email');