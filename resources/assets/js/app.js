
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');



window.Vue = require('vue');
export const eventBus = new Vue();
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('order-form', require('./components/OrderForm.vue').default);  // +
Vue.component('search-form', require('./components/SearchForm.vue').default);
Vue.component('search-form-modal', require('./components/SearchFormModal.vue').default);
Vue.component('part-car', require('./components/unit/partCar').default);
Vue.component('part-sidebar', require('./components/unit/partSidebar').default); // +
Vue.component('part-ravon-price', require('./components/unit/partRavonPrice').default);
Vue.component('filters', require('./components/Filters').default);
Vue.component('cars-list', require('./components/CarsList').default);
Vue.component('final-form', require('./components/FinalForm.vue').default);
Vue.component('contacts-page', require('./components/ContactsPage').default);
Vue.component('order', require('./components/Order.vue').default);
Vue.component('languages-switch', require('./components/unit/languagesSwitch.vue').default); // +
Vue.component('currency-switch', require('./components/unit/currencySwitch.vue').default); // +
Vue.component('input-contacts-errors', require('./components/unit/inputsContactsErrors.vue').default); // +
Vue.component('sort', require('./components/unit/sort.vue').default); // +


//In future this files can by remove
Vue.component('Paginator', require('./components/unit/partPagination.vue').default);



import Vue from 'vue';
import ElementUI from 'element-ui'
import VueI18n from 'vue-i18n';

import Locale from './vue-i18n-locales.generated';
import localeLang from 'element-ui/lib/locale';

import enLocale from 'element-ui/lib/locale/lang/en';
import ruLocale from 'element-ui/lib/locale/lang/ru-RU';
import uaLocale from 'element-ui/lib/locale/lang/ua';

Vue.use(VueI18n);

// ElementUI translate
let param = {
    'ru': ruLocale,
    'en': enLocale,
    'ua': uaLocale
};
localeLang.use(param[document.documentElement.lang]);

const lang = document.documentElement.lang.substr(0, 2);
// or however you determine your current app locale

const i18n = new VueI18n({
    locale: lang,
    messages: Locale
});

const app = new Vue({
    el: '#app',
    i18n,
});
