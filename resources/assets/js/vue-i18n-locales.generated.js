(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            (global.vuei18nLocales = factory());
}(this, (function () { 'use strict';
    return {
    "en": {
        "auth": {
            "get": "These credentials do not match our records.",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "car": {
            "days": "Days",
            "days_price": "Day's price",
            "final_price": "Final price for",
            "equivalent": "or equivalent",
            "send_proposal": "Send proposal",
            "enter_email": "enter your email",
            "order_btn": "Order",
            "deposit": "Deposit"
        },
        "certificate": {
            "order_name": "Proposal to rent a car:",
            "order": "order",
            "order_includes": "Your order includes",
            "total": "Total",
            "price": "Order",
            "deposit": "Deposit"
        },
        "filters": {
            "class": "Class",
            "brand": "Brand",
            "transmission": "Transmission",
            "fuel": "Fuel",
            "all_classes": "All Classes",
            "all_brands": "All Brands",
            "all_transes": "All Transmissions",
            "all_fuels": "All Fuels",
            "sortA": "Sort by price ASC",
            "sortD": "Sort by price DESC"
        },
        "form": {
            "choose_continue":"Continue",
            "adres":"Address",
            "choose_country": "Select Country",
            "header": "Select Car",
            "get": "Get",
            "place_get": "Place to get",
            "return": "Return",
            "place_return": "Place to return",
            "button": "Select a car",
            "button_order": "Make Order",
            "personal_data": "Personal params",
            "fio": "Full Name",
            "phone": "Phone",
            "phoneCode": "+380",
            "email": "email",
            "email_placeholder": "E-mail",
            "comment": "Comment",
            "options": "Additional options",
            "notice": "Attention, please! We guarantee that we will give you car in the class with the stated gearbox, we will take into account your wishes for a particular car brand. When you choose the form of payment and click \"order the car\" you agree with the terms of payment and booking set forth on the website in the section rental conditions, as well as the public offer contract.",
            "notice2": "To get your car don't forget your passport, driver's licence, money and charge your phone.",
            "back": "Back",
            "final_price": "Final price",
            "your_order": "Your order",
            "accepted": "accepted",
            "in_process": "Our managers are already processing your order!",
            "for_consult": "In order to consult you can make a call on the phone number:",
            "in_ukraine": "in Ukraine",
            "rent_a_car": "Rent a car in",
            "our_offices_in": "Our offices in"
        },
        "errors": {
            "set_name": "enter your name",
            "set_correct_name": "Please enter a valid name.",
            "set_phone": "enter your phone number.",
            "set_correct_phone": "Please enter a valid phone number.",
            "set_email": "enter your email.",
            "set_correct_email": "Please enter a valid email.",
        },
        "menu": {
            "main": "Main",
            "autopark": "Autopark",
            "conditions": "Conditions",
            "contacts": "Contacts",
            "correct": "Success"
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "validate": {
            "message": "Please, correct next errors:"
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    },
    "ru": {
        "auth": {
            "failed": "Пиздец",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "car": {
            "days": "Дни",
            "days_price": "Цена за сутки",
            "final_price": "Итоговая стоимость за",
            "equivalent": "или аналоги",
            "send_proposal": "Отправить предложение",
            "enter_email": "введите свой email",
            "order_btn": "Заказать",
            "deposit": "Залог"
        },
        "certificate": {
            "order_name": "ЗАЯВКА НА ПРОКАТ АВТОМОБИЛЯ:",
            "order": "заказ",
            "order_includes": "Ваш заказ включает в себя",
            "total": "Итого",
            "price": "Сумма заказа",
            "deposit": "Залог"
        },
        "filters": {
            "class": "Класс",
            "brand": "Марка",
            "transmission": "Коробка передач",
            "fuel": "Топливо",
            "all_classes": "Все классы",
            "all_brands": "Все марки",
            "all_transes": "Все трансмиссии",
            "all_fuels": "Все топлива",
            "sortA": "от дешевых к дорогим",
            "sortD": "от дорогих к дешевым"
        },
        "form": {
            "choose_continue":"Продолжить",
            "adres":"Адрес",
            "choose_country": "Выберете страну",
            "header": "Подбор авто",
            "get": "Подача",
            "place_get": "Место подачи",
            "return": "Возврат",
            "place_return": "Место возврата",
            "button": "Подобрать автомобиль",
            "button_order": "Заказать",
            "personal_data": "Личные данные",
            "fio": "Полное имя",
            "phone": "Телефон",
            "phoneCode": "+380",
            "email": "email",
            "email_placeholder": "E-mail",
            "comment": "Комментарий",
            "options": "Дополнительные опции",
            "notice": "Внимание! Мы гарантируем выдачу автомобиля в классе с заявленной коробкой передач, мы обязательно учтем ваши пожелания по конкретной марке автомобиля. Выбрав форму оплаты и нажимая заказать вы соглашаетесь с условиями платежа и бронирования изложенными на сайте в разделе условия проката, а так же договора оферты",
            "notice2": "Для получения автомобиля не забудьте взять паспорт, водительское удостоверение и деньги. Зарядить телефон, установить приложение.",
            "back": "Назад",
            "final_price": "Итоговая стоимость",
            "your_order": "Ваш заказ",
            "accepted": "принят",
            "in_process": "Наши операторы уже обрабатывают Ваш заказ!",
            "for_consult": "Для консультации Вы можете позвонить по номеру телефона:",
            "in_ukraine": "в Украине",
            "rent_a_car": "Прокат авто в",
            "our_offices_in": "Наши офисы в"
        },
        "errors": {
            "set_name": "Укажите имя.",
            "set_correct_name": "Укажите корректное имя.",
            "set_phone": "Укажите телефон.",
            "set_correct_phone": "Укажите корректный телефон.",
            "set_email": "Укажите электронную почту.",
            "set_correct_email": "Укажите корректный адрес электронной почты.",
        },
        "menu": {
            "main": "Главная",
            "autopark": "Автопарк",
            "conditions": "Условия",
            "contacts": "Контакты",
            "correct": "Успех"
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "validate": {
            "message": "Пожалуйста исправьте указанные ошибки:"
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    },
    "ua": {
        "auth": {
            "failed": "Пи..",
            "throttle": "Too many login attempts. Please try again in {seconds} seconds."
        },
        "car": {
            "days": "Дні",
            "days_price": "Ціна за добу",
            "final_price": "Підсумкова вартість за",
            "equivalent": "Або аналоги",
            "send_proposal": "Відправити пропозицію",
            "enter_email": "введить свій email",
            "order_btn": "Замовити",
            "deposit": "Застава"
        },
        "certificate": {
            "order_name": "ЗАЯВКА НА ПРОКАТ АВТОМОБІЛЯ:",
            "order": "замовлення",
            "order_includes": "Ваше замовлення включає в себе",
            "total": "Разом",
            "price": "Сумма замовлення",
            "deposit": "Застава"
        },
        "filters": {
            "class": "Класс",
            "brand": "Марка",
            "transmission": "Коробка передач",
            "fuel": "Паливо",
            "all_classes": "Усі класи",
            "all_brands": "Усі марки",
            "all_transes": "Все трансміссії",
            "all_fuels": "Всі палива",
            "sortA": "від дешевих до дорогих",
            "sortD": "від дорогих до дешевих"
        },
        "form": {
            "choose_continue":"Продовжити",
            "adres":"Адреса",
            "choose_country": "Оберить країну",
            "header": "Підбор авто",
            "get": "Отримати",
            "place_get": "Місце отримання",
            "return": "Повернути",
            "place_return": "Місце повернення",
            "button": "Підібрати автомобіль",
            "button_order": "Замовити",
            "personal_data": "Особисті данні",
            "fio": "Повне им'я",
            "phone": "Телефон",
            "phoneCode": "+380",
            "email": "email",
            "email_placeholder": "E-mail",
            "comment": "Комментарій",
            "options": "Додаткові опції",
            "notice": "Увага! Ми гарантуємо отримання автомобіля у класі із заявленною коробкою передач,\n                 ми обов'язково врахуйємо Ваші побажання щодо конкретної марки автомобіля.\n                 Обрав форму оплати і натиская заказати Ви погоджуєтесь з умовами платежу та бронювання викладенними на сайті у розділі Умови проката,\n                 а також умовами оферти",
            "notice2": "Для отримання авто не забудьте взяти паспорт, водійське посвідчення та гроші. Зарядіть телефон.",
            "back": "Назад",
            "final_price": "Підсумкова вартість",
            "your_order": "Више замовлення",
            "accepted": "прийнято",
            "in_process": "Наші оперетори вже оброблюють Ваше замовлення!",
            "for_consult": "Для консультування зв'яжіться з нами за номером телефона:",
            "in_ukraine": "в Україні",
            "rent_a_car": "Арендувати авто в",
            "our_offices_in": "Наші офіси у"
        },
        "errors": {
            "set_name": "Вкажить им'я.",
            "set_correct_name": "Вкажить коректне им'я.",
            "set_phone": "Вкажить телефон.",
            "set_correct_phone": "Вкажить коректний телефон.",
            "set_email": "Вкажить електронну пошту.",
            "set_correct_email": "Вкажить коректну адресу електронної пошти.",
        },
        "menu": {
            "main": "Головна",
            "autopark": "Автопарк",
            "conditions": "Умови",
            "contacts": "Контакти",
            "correct": "Успіх"
        },
        "pagination": {
            "previous": "&laquo; Previous",
            "next": "Next &raquo;"
        },
        "passwords": {
            "password": "Passwords must be at least six characters and match the confirmation.",
            "reset": "Your password has been reset!",
            "sent": "We have e-mailed your password reset link!",
            "token": "This password reset token is invalid.",
            "user": "We can't find a user with that e-mail address."
        },
        "validate": {
            "message": "Пожалуйста исправьте указанные ошибки:"
        },
        "validation": {
            "accepted": "The {attribute} must be accepted.",
            "active_url": "The {attribute} is not a valid URL.",
            "after": "The {attribute} must be a date after {date}.",
            "after_or_equal": "The {attribute} must be a date after or equal to {date}.",
            "alpha": "The {attribute} may only contain letters.",
            "alpha_dash": "The {attribute} may only contain letters, numbers, and dashes.",
            "alpha_num": "The {attribute} may only contain letters and numbers.",
            "array": "The {attribute} must be an array.",
            "before": "The {attribute} must be a date before {date}.",
            "before_or_equal": "The {attribute} must be a date before or equal to {date}.",
            "between": {
                "numeric": "The {attribute} must be between {min} and {max}.",
                "file": "The {attribute} must be between {min} and {max} kilobytes.",
                "string": "The {attribute} must be between {min} and {max} characters.",
                "array": "The {attribute} must have between {min} and {max} items."
            },
            "boolean": "The {attribute} field must be true or false.",
            "confirmed": "The {attribute} confirmation does not match.",
            "date": "The {attribute} is not a valid date.",
            "date_format": "The {attribute} does not match the format {format}.",
            "different": "The {attribute} and {other} must be different.",
            "digits": "The {attribute} must be {digits} digits.",
            "digits_between": "The {attribute} must be between {min} and {max} digits.",
            "dimensions": "The {attribute} has invalid image dimensions.",
            "distinct": "The {attribute} field has a duplicate value.",
            "email": "The {attribute} must be a valid email address.",
            "exists": "The selected {attribute} is invalid.",
            "file": "The {attribute} must be a file.",
            "filled": "The {attribute} field must have a value.",
            "image": "The {attribute} must be an image.",
            "in": "The selected {attribute} is invalid.",
            "in_array": "The {attribute} field does not exist in {other}.",
            "integer": "The {attribute} must be an integer.",
            "ip": "The {attribute} must be a valid IP address.",
            "ipv4": "The {attribute} must be a valid IPv4 address.",
            "ipv6": "The {attribute} must be a valid IPv6 address.",
            "json": "The {attribute} must be a valid JSON string.",
            "max": {
                "numeric": "The {attribute} may not be greater than {max}.",
                "file": "The {attribute} may not be greater than {max} kilobytes.",
                "string": "The {attribute} may not be greater than {max} characters.",
                "array": "The {attribute} may not have more than {max} items."
            },
            "mimes": "The {attribute} must be a file of type: {values}.",
            "mimetypes": "The {attribute} must be a file of type: {values}.",
            "min": {
                "numeric": "The {attribute} must be at least {min}.",
                "file": "The {attribute} must be at least {min} kilobytes.",
                "string": "The {attribute} must be at least {min} characters.",
                "array": "The {attribute} must have at least {min} items."
            },
            "not_in": "The selected {attribute} is invalid.",
            "numeric": "The {attribute} must be a number.",
            "present": "The {attribute} field must be present.",
            "regex": "The {attribute} format is invalid.",
            "required": "The {attribute} field is required.",
            "required_if": "The {attribute} field is required when {other} is {value}.",
            "required_unless": "The {attribute} field is required unless {other} is in {values}.",
            "required_with": "The {attribute} field is required when {values} is present.",
            "required_with_all": "The {attribute} field is required when {values} is present.",
            "required_without": "The {attribute} field is required when {values} is not present.",
            "required_without_all": "The {attribute} field is required when none of {values} are present.",
            "same": "The {attribute} and {other} must match.",
            "size": {
                "numeric": "The {attribute} must be {size}.",
                "file": "The {attribute} must be {size} kilobytes.",
                "string": "The {attribute} must be {size} characters.",
                "array": "The {attribute} must contain {size} items."
            },
            "string": "The {attribute} must be a string.",
            "timezone": "The {attribute} must be a valid zone.",
            "unique": "The {attribute} has already been taken.",
            "uploaded": "The {attribute} failed to upload.",
            "url": "The {attribute} format is invalid.",
            "custom": {
                "attribute-name": {
                    "rule-name": "custom-message"
                }
            },
            "attributes": []
        }
    }
}

})));
