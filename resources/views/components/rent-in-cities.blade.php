<section class="section-settings" id="rentInCities">
    <div class="container">
        <h1 class="section-title-center">Популярные направления</h1>
        <div class="wrapper">
            <p>Ярким примером глобализации является наша компания. Мы начинали как небольшая фирма, но теперь мы
                представлены в пяти странах мира, где имеем более 30 офисов и пунктов выдачи авто. Менее чем за
                10
                лет
                мы построили отличную команду и обслужили огромное количество клиентов. Приблизительно раз в час
                мы
                выдаем автомобиль очередному клиенту.</p>
            <p>Сегодня офисы нашей компании представлены в Украине, Грузии, Крыму, ОАЭ, Чехии. Если Вы хотите
                арендовать
                автомобиль в любом из этих регионов, Вы можете оформить заявку на сайте, либо позвонить на один
                из
                удобных для Вас номеров.</p>

            <div class="img-line row justify-content-center">
                @foreach($regionsInMain as $id => $city)
                    <div class="col-6 col-lg-4 col-xl">
                        <a class="nav-link" href="{{ route('autopark.city',['country' => str_slug($city['country']['name_country_en']),
                                                                            'city' => $city['name_translit'],
                                                                            'country_id' => $city['country']['id'],
                                                                            'city_id' => $city['id'],
                                                                           ]) }}">
                        <figure>

                            <picture>
                                <source type="image/webp" srcset="/images/img_city/front_{{$city['name_translit']}}.webp">
                                <source type="image/jpeg" srcset="/images/img_city/front_{{$city['name_translit']}}.jpg">
                                <img src="/images/img_city/front_{{$city['name_translit']}}.jpg" alt="ukr">
                            </picture>
                            {{--<img src="/images/img_city/front_{{$city['name_translit']}}.jpg" alt="ukr"/>--}}
                            <figcaption>{{$city['city_localized']}}</figcaption>
                            {{--<div class="rent-price">--}}
                                {{--<p>Аренда авто</p>--}}
                                {{--<p>от 20$/сутки</p>--}}
                            {{--</div>--}}
                        </figure>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
