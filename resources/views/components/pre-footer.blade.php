<section class="section-settings" id="prefooter">
    <div class="container">
        <div class="d-flex justify-content-between border-t">
            <div class="phone-box">
                <p class="phone-box-title">Звоните на один из удобных для Вас номеров:</p>
                <p>Для клиентов из Украины: <span>+38 (067) 780-86-99</span></p>
                <p>Для клиентов из России: <span>+7 (978) 782-52-42</span></p>
                <p>Для клиентов из Грузии: <span>(+995) 514-646-444</span></p>
            </div>

            <div class="mail-box">
                <p class="mail-box-title">Звоните на один из удобных для Вас номеров:</p>
                <p>Для вопросов: <span>promo@lion-avtoprokat.com.ua</span></p>
                <p>Для оформления заказа: <span>zakaz@lion-avtoprokat.com.ua</span></p>
            </div>
        </div>
    </div>
</section>