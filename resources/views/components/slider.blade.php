<section id="slider">


        <order-form
                :countries="{{json_encode($countries)}}"
                :get-cities-when-mounted="{{json_encode(false)}}"
        ></order-form>



    <div class="slider">
        <div id="carouselId" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                <li data-target="#carouselId" data-slide-to="1"></li>
                <li data-target="#carouselId" data-slide-to="2"></li>
                <li data-target="#carouselId" data-slide-to="3"></li>
                <li data-target="#carouselId" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class=" carousel-caption d-none d-md-block txt-prime">
                        <div class="carousel-caption-wrap">
                            <h1>Тут может быть заголовок</h1>
                            <h3>И возможно не много рекламного текста</h3>
                        </div>
                    </div>
                    <picture>
                        <source type="image/webp" srcset="/images/Slider/slide_1.webp">
                        <source type="image/jpeg" srcset="/images/Slider/slide_1.jpg">
                        <img src="/images/Slider/slide_1.jpg" alt="slide_1">
                    </picture>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption d-none d-md-block txt-prime">
                        <h1>Тут может быть заголовок</h1>
                        <h3>И возможно не много рекламного текста</h3>
                    </div>

                    <picture>
                        <source type="image/webp" srcset="/images/Slider/slide_2.webp">
                        <source type="image/jpeg" srcset="/images/Slider/slide_2.jpg">
                        <img src="/images/Slider/slide_2.jpg" alt="slide_2">
                    </picture>
                   {{-- <img rel="preload" src="/images/Slider/slide_2.webp" alt="Second slide" as="image" media="(max-width: 1903px)"/>--}}
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption d-none d-md-block txt-prime">
                        <h1>Тут может быть заголовок</h1>
                        <h3>И возможно не много рекламного текста</h3>
                    </div>

                    <picture>
                        <source type="image/webp" srcset="/images/Slider/slide_3.webp">
                        <source type="image/jpeg" srcset="/images/Slider/slide_3.jpg">
                        <img src="/images/Slider/slide_3.jpg" alt="slide_3">
                    </picture>
                    {{-- <img rel="preload" src="/images/Slider/slide_3.jpg" alt="Third slide" as="image" media="(max-width: 1903px)"/>--}}
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption d-none d-md-block txt-prime">
                        <h1>Тут может быть заголовок</h1>
                        <h3>И возможно не много рекламного текста</h3>
                    </div>

                    <picture>
                        <source type="image/webp" srcset="/images/Slider/slide_4.webp">
                        <source type="image/jpeg" srcset="/images/Slider/slide_4.jpg">
                        <img src="/images/Slider/slide_4.jpg" alt="slide_4">
                    </picture>
                    {{--<img rel="preload" src="/images/Slider/slide_4.jpg" alt="Third slide" as="image" media="(max-width: 1903px)"/>--}}
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption d-none d-md-block txt-prime">
                        <h1>Тут может быть заголовок3</h1>
                        <h3>И возможно не много рекламного текста</h3>
                    </div>

                    <picture>
                        <source type="image/webp" srcset="/images/Slider/slide_5.webp">
                        <source type="image/jpeg" srcset="/images/Slider/slide_5.jpg">
                        <img src="/images/Slider/slide_5.jpg" alt="slide_5">
                    </picture>
                    {{--<img rel="preload" src="/images/Slider/slide_5.jpg" alt="Third slide" as="image" media="(max-width: 1903px)"/>--}}
                </div>
            </div>


            <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
