@extends('layouts.app')

@section('content')

  <div class="container">
    <section class="section-settings" id="ordered">
      <div class="container page_order">

        <div class="container-sidebar p-0">
          {{--<search-form--}}
          {{--:countries="{{json_encode($countries)}}"--}}
          {{--:country="{{json_encode($country)}}"--}}
          {{--:get-cities-when-mounted="{{json_encode(true)}}"--}}
          {{--:preset-form-params="{{json_encode($presetFormParams)}}"--}}
          {{--></search-form>--}}
          <order-form
            :countries="{{json_encode($countries)}}"
            :get-cities-when-mounted="{{json_encode(false)}}"
            :preset-country="{{json_encode($country)}}"
          ></order-form>

        </div>

        <div class="container-page">
          <div class="container">

            <div class="title m-b-md">
              <h1>Country VIEW {{$countryForPages->country->name_country_ru}}</h1>
              {{$countryForPages->title}}
              {{$countryForPages->description}}
              {{$countryForPages->head}}
              {!! $countryForPages->content !!}

            </div>

          </div>
        </div>
      </div>

    </section>

  </div>





@endsection


