@extends('layouts.app')



@section('content')

    @include('components.slider')

    @include('components.rent-in-countries')

    @include('components.rent-in-cities')

    @include('components.advantages')

    @include('components.about-company')

    @include('components.pre-footer')

@endsection

@yield('scripts')
   

