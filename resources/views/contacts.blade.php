@extends('layouts.app')

@section('content')

    <contacts-page class="mt-3 mb-3"
                   :offices="{{json_encode($offices)}}"
                   :country="{{json_encode($country)}}"
    ></contacts-page>

@endsection


