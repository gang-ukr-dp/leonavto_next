@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        You are logged in!
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Статус</th>
                                <th>Дата</th>
                                <th>Номер</th>
                                <th>Автомобиль</th>
                                <th>Начало аренды</th>
                                <th>Завершение аренды</th>
                            </tr>
                            </thead>
                            <tbody>

{{--                            @foreach ($tickets as $ticket)--}}
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {{--@if ($ticket->isOpen())--}}
                                            <span class="badge badge-danger">Open</span>
                                        {{--@elseif ($ticket->isApproved())--}}
                                            <span class="badge badge-primary">Approved</span>
                                        {{--@elseif ($ticket->isClosed())--}}
                                            <span class="badge badge-secondary">Closed</span>
                                        {{--@endif--}}
                                    </td>
                                </tr>
                            {{--@endforeach--}}

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
