@extends('layouts.app')

@section('content')

    {{--slider--}}
    <section id="slider">
        <div class="container container-relative">
            <div class="selectorCar text-center">

                <div class="box-title">Подбор авто1</div>
                <div class="box-body">

                    <form action="" method="post" role="form">


                        <div class="form-group">

                            <input type="text" class="form-control" name="" id="" placeholder="Выберите страну">

                            <input type="text" class="form-control" name="" id=""
                                   placeholder="Введите место подачи">

                            <div class="d-flex row">
                                <div class="col-6">
                                    <label for="">Подача</label>
                                    <input type="text" class="form-control" name="" id=""
                                           placeholder="Выберите страну">
                                </div>
                                <div class="col-6">
                                    <label for="">&nbsp;</label>
                                    <input type="text" class="form-control" name="" id=""
                                           placeholder="Введите место подачи">
                                </div>
                            </div>

                            <div class="form-checkbox">
                                <input type="checkbox" value="" id="r1">
                                <label class="form-checkbox-label" for="r1">
                                    Вернуть в другом месте
                                </label>
                            </div>

                            <div class="d-flex row">
                                <div class="col-6">
                                    <label for="">Возврат</label>
                                    <input type="text" class="form-control" name="" id=""
                                           placeholder="Выберите страну">
                                </div>
                                <div class="col-6">
                                    <label for="">&nbsp;</label>
                                    <input type="text" class="form-control" name="" id=""
                                           placeholder="Введите место подачи">
                                </div>
                            </div>


                        </div>

                    </form>

                </div>
            </div>
        </div>
        <div class="slider">
            <div id="carouselId" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselId" data-slide-to="1"></li>
                    <li data-target="#carouselId" data-slide-to="2"></li>
                    <li data-target="#carouselId" data-slide-to="3"></li>
                    <li data-target="#carouselId" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner" role="listbox">  
                    <div class="carousel-item active">
                        <div class="offset-2 col-8 carousel-caption d-none d-md-block txt-prime">
                            <div class="carousel-caption-wrap">
                                <h1>Тут может быть заголовок</h1>
                                <h3>И возможно не много рекламного текста</h3>
                            </div>
                        </div>
                        <img src="/images/Slider/slide_1.jpg" alt="First slide"/>
                    </div>
                    <div class="carousel-item">
                        <div class="offset-2 col-8 carousel-caption d-none d-md-block txt-prime">
                            <h1>Тут может быть заголовок</h1>
                            <h3>И возможно не много рекламного текста</h3>
                        </div>
                        <img src="/images/Slider/slide_2.jpg" alt="Second slide"/>
                    </div>
                    <div class="carousel-item">
                        <div class="offset-2 col-8 carousel-caption d-none d-md-block txt-prime">
                            <h1>Тут может быть заголовок</h1>
                            <h3>И возможно не много рекламного текста</h3>
                        </div>
                        <img src="/images/Slider/slide_3.jpg" alt="Third slide"/>
                    </div>
                    <div class="carousel-item">
                        <div class="offset-2 col-8 carousel-caption d-none d-md-block txt-prime">
                            <h1>Тут может быть заголовок</h1>
                            <h3>И возможно не много рекламного текста</h3>
                        </div>
                        <img src="/images/Slider/slide_4.jpg" alt="Third slide"/>
                    </div>
                    <div class="carousel-item">
                        <div class="offset-2 col-8 carousel-caption d-none d-md-block txt-prime">
                            <h1>Тут может быть заголовок3</h1>
                            <h3>И возможно не много рекламного текста</h3>
                        </div>
                        <img src="/images/Slider/slide_5.jpg" alt="Third slide"/>
                    </div>
                </div>


                <a class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselId" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div> 
    </section>

    {{--rentInCountries--}}
    <section class="section-settings" id="rentInCountries">
        <div class="container">
            <h1 class="section-title-center">Аренда авто в Украине</h1>
            <div class="wrapper">
                <p>Решили посетить Украину? Задумайтесь про прокат авто в Украине, с данной услугой вы будете
                    чувствовать
                    себя более мобильно и не будете стеснены во времени.</p>
                <p>Услуги проката машин в Украине предлагает множество компаний, но всегда главное – выбрать ту,
                    которая
                    будет вам подходить. Основной критерий, которым руководствуются клиенты при выборе компании по
                    прокату
                    автомобилей, является: цена, новизна автомобилей, приветливый персонал, простота условий и
                    получения
                    автомобиля, большой выбор автомобилей по маркам.
                </p>
                <p>Всем указанным критериям и условиям отвечает наша компания в Украине. Мы предлагаем новые
                    автомобили,
                    по
                    дешевой цене, выбор автомобилей большой и всегда есть в наличии. Лион прокат автомобилей в
                    Украине
                    не
                    локальная, а международная компания, если вас интересует аренда машин не только в Украине, а и в
                    других
                    странах, мы поможем в решении вашего вопроса.</p>

                <div class="img-line row justify-content-center">
                    <div class="col-6 col-lg-4 col-xl">
                        <figure><img src="/images/img_countries/ukr.png" alt="ukr">
                            <figcaption>Прокат авто в Украине</figcaption>
                        </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                        <figure class="active"><img src="/images/img_countries/grg.png" alt="grg">
                            <figcaption>Прокат авто в Грузии</figcaption>
                        </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                        <figure><img src="/images/img_countries/oao.png" alt="oao">
                            <figcaption>Прокат авто в ОАЭ</figcaption>
                        </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                        <figure><img src="/images/img_countries/chz.png" alt="chz">
                            <figcaption>Прокат авто в Чехии</figcaption>
                        </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                        <figure><img src="/images/img_countries/blg.png" alt="blg">
                            <figcaption>Прокат авто в Болгарии</figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    {{--rentInCountries--}}
    <section class="section-settings" id="rentInCountries">
        <div class="container mb-5">
            <h1 class="section-title-center">Аренда авто в Украине</h1>
             <div class="city-describe">
                  <div class="describe-left">
                      <img src="/images/img_city/dnepr.jpg" alt="dneprO">
                  </div>
                  <div class="describe-right">
                      <p>Город Днепропетровск уже давно получил народное название Днепр, но в 2016 году город официально переименовали в Днепр. Есть небольшая коллизия, так как город стоит на одноименной реке, но такая же аналогия есть с Москвой.</p>
                      <p>Существует поговорка, что Днепропетровск не первый город в Украине, но и не второй. Днепр – финансовая и металлургическая столица Украины.</p>
                      <p>Всем указанным критериям и условиям отвечает наша компания в Украине. Мы предлагаем новые
                          автомобили,
                          по
                          дешевой цене, выбор автомобилей большой и всегда есть в наличии. Лион прокат автомобилей в
                          Украине
                          не
                          локальная, а международная компания, если вас интересует аренда машин не только в Украине, а и в
                          других
                          странах, мы поможем в решении вашего вопроса.</p>
                  </div>
             </div>
        </div>

      <div class="container mb-5">
        <h1 class="section-title-center">Популярные автомобили в Днепре</h1>
        <div class="wrapper">
          <p>Цена на услугу зависит от класса автомобиля, а также от срока его аренды. Мы всегда готовы подготовить специальное предложение для предприятий, которые хотят взять авто на долгий срок. Звоните нам, и мы с Вами обсудим все детали нашего сотрудничества.  <a href="#">« Ознакомиться с автопарком »</a></p>
        </div>
      </div>

      <div class="container mb-5">

        <div class="img-line-ravon">
          <a href="#" class="ilr ravon contain-card">

              <div class="head-blue title p-1 pl-2 p3-2">Ravon R2</div>
              <img src="/images/cars/Hyundai_I_30_CDAV_2014_1600_gray.jpg" alt="ukr">
              <div class="head-dark text-center p-2">От 20$/сутки</div>

          </a>
          <a href="#" class="ilr ravon contain-card">

              <div class="head-blue title p-1 pl-2 p3-2">Ravon R2</div>
              <img src="/images/cars/Skoda_Fabia_EDMV_2013_1200_gray.jpg" alt="oao">
              <div class="head-dark text-center p-2">От 20$/сутки</div>

          </a>
          <a href="#" class="ilr ravon contain-card">

              <div class="head-blue title p-1 pl-2 p3-2">Ravon R2</div>
              <img src="/images/cars/Toyota_Corolla_CDAV_2013_1600_gray.jpg" alt="chz">
              <div class="head-dark text-center p-2">От 20$/сутки</div>

          </a>
          <a href="#" class="ilr ravon contain-card">

              <div class="head-blue title p-1 pl-2 p3-2">Ravon R2</div>
              <img src="/images/cars/Volkswagen_Passat_FDAV_2013_1800_black.jpg" alt="blg">
              <div class="head-dark text-center p-2">От 20$/сутки</div>

          </a>
          <a href="#" class="ilr ravon contain-card">

              <div class="head-blue title p-1 pl-2 p3-2">Ravon R2</div>
              <img src="/images/cars/Hyundai_I_30_CDAV_2014_1600_gray.jpg" alt="ukr">
              <div class="head-dark text-center p-2">От 20$/сутки</div>

          </a>
        </div>

      </div>

      <div class="container mb-5">
        <div class="wrapper">
          <h4>Прокат авто без залога</h4>
          <p>Не хотите оставлять залог или сумма залога слишком велика? Запросите расчет услуги «без залога». Наши менеджеры мгновенно отправят вам предложение.</p>

          <h4>Прокат авто с правом выкупа</h4>
          <p>Хотите поощрить ваших сотрудников и взять автомобиль в прокат, а по окончанию срока выкупить – обращайтесь
            в Лион. Мы не предоставляем автомобили для такси!</p>

          <h4>Аренда автомобиля с водителем</h4>
          <p>У вас намечается деловая встреча или просто необходимо отвезти ребенка в школу, а может вы испытываете
            трудности из-за того, что недавно уволили водителя, а быстро не возьмешь на работу надежного человека? В
            такой ситуации обратитесь к нам и закажите аренду автомобиля в Днепропетровске с водителем. Оплата за такую
            услугу может считаться несколькими путями:</p>

          <p>Оплата аренды авто, услуг водителя, а топливо Вы оплатите самостоятельно на заправке.
            Оплата почасовая или за каждый километр, в этом случае топливо, автомобиль и услуги водителя уже включены в
            цену проката.</p>

            <p>Внимание! При аренде автомобиля с водителем, залог за автомобиль оставлять не нужно.</p>
        </div>
      </div>

      <div class="container mb-5">
        <div class="wrapper">
          <h4>Прокат авто без залога</h4>
          <p>Не хотите оставлять залог или сумма залога слишком велика? Запросите расчет услуги «без залога». Наши менеджеры мгновенно отправят вам предложение.</p>

          <h4>Прокат авто с правом выкупа</h4>
          <p>Хотите поощрить ваших сотрудников и взять автомобиль в прокат, а по окончанию срока выкупить – обращайтесь
            в Лион. Мы не предоставляем автомобили для такси!</p>

          <h4>Аренда автомобиля с водителем</h4>
          <p>У вас намечается деловая встреча или просто необходимо отвезти ребенка в школу, а может вы испытываете
            трудности из-за того, что недавно уволили водителя, а быстро не возьмешь на работу надежного человека? В
            такой ситуации обратитесь к нам и закажите аренду автомобиля в Днепропетровске с водителем. Оплата за такую
            услугу может считаться несколькими путями:</p>

          <p>Оплата аренды авто, услуг водителя, а топливо Вы оплатите самостоятельно на заправке.
            Оплата почасовая или за каждый километр, в этом случае топливо, автомобиль и услуги водителя уже включены в
            цену проката.</p>

            <p>Внимание! При аренде автомобиля с водителем, залог за автомобиль оставлять не нужно.</p>
        </div>
      </div>

      <div class="container mb-5">
        <h1 class="section-title-center">Достопримечательности Днепра</h1>
        <div class="city-describe">
          <div class="describe-left">
            <img src="/images/img_city/dnepr_1.jpg" alt="dneprO">
            <h4>Днепропвский цирк</h4>
            <p>Самая романтичная и таинственная достопримечательность Днепра – Монастырский остров. <br>
              Это одно из самых популярных мест среди горожан. Попасть на остров можно по пешеходному мосту, особо
              любимому молодоженами – традиция вешать замки счастья на перилах моста не перестает быть актуальной.<br>
              Еще один вариант добраться на остров – скоростной троллей. Так вы попадете на новую смотровую площадку, с
              которой открывается прекрасная панорама на город. А у подножья шумит искусственный водопад высотой 17
              метров. Украшением острова является белоснежная Николаевская церковь, построенная в 1999 году. Ее
              золоченый купол виден с разных точек города.</p>

            <p>Еще одна достопримечательность – памятник великому Кобзарю Тарасу Шевченко. Этот монумент один из самых
              больших в Украине. Зоопарк, аквариум, различные аттракционы, кафе и рестораны, водная станция, конный
              дворик, пляжная зона – все это способствует полноценному отдыху.</p>

            <p>Остановиться в Днепропетровске можно в отеле или частных апартаментах. Недалеко от Монастырского острова
              есть несколько гостиниц.</p>
          </div>
          <div class="describe-right">
            <h4>Монастырский остров</h4>

            <p>Самая романтичная и таинственная достопримечательность Днепра – Монастырский остров. <br>
            Это одно из самых популярных мест среди горожан. Попасть на остров можно по пешеходному мосту, особо
              любимому молодоженами – традиция вешать замки счастья на перилах моста не перестает быть актуальной.<br>
              Еще один вариант добраться на остров – скоростной троллей. Так вы попадете на новую смотровую площадку, с
              которой открывается прекрасная панорама на город. А у подножья шумит искусственный водопад высотой 17
              метров. Украшением острова является белоснежная Николаевская церковь, построенная в 1999 году. Ее
              золоченый купол виден с разных точек города.</p>

            <p>Еще одна достопримечательность – памятник великому Кобзарю Тарасу Шевченко. Этот монумент один из самых
              больших в Украине. Зоопарк, аквариум, различные аттракционы, кафе и рестораны, водная станция, конный
              дворик, пляжная зона – все это способствует полноценному отдыху.</p>

            <p>Остановиться в Днепропетровске можно в отеле или частных апартаментах. Недалеко от Монастырского острова
              есть несколько гостиниц.</p>
            
            <img src="/images/img_city/dnepr_2.jpg" alt="dneprO">

          </div>
        </div>
      </div>
    </section>

    {{--rentInCities--}}
    <section class="section-settings" id="rentInCities">
        <div class="container">
            <h1 class="section-title-center">Популярные направления</h1>
            <div class="wrapper">
                <p>Ярким примером глобализации является наша компания. Мы начинали как небольшая фирма, но теперь мы
                    представлены в пяти странах мира, где имеем более 30 офисов и пунктов выдачи авто. Менее чем за
                    10
                    лет
                    мы построили отличную команду и обслужили огромное количество клиентов. Приблизительно раз в час
                    мы
                    выдаем автомобиль очередному клиенту.</p>
                <p>Сегодня офисы нашей компании представлены в Украине, Грузии, Крыму, ОАЭ, Чехии. Если Вы хотите
                    арендовать
                    автомобиль в любом из этих регионов, Вы можете оформить заявку на сайте, либо позвонить на один
                    из
                    удобных для Вас номеров.</p>
                <div class="img-line row justify-content-center">
                    <div class="col-6 col-lg-4 col-xl">
                        <figure>
                            <img src="images/img_city/kyv.png" alt="ukr"/>
                            <figcaption>КИЕВ</figcaption>
                            <div class="rent-price">
                                <p>Аренда авто</p>
                                <p>от 20$/сутки</p>
                            </div>
                        </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure class="active">
                        <img src="images/img_city/dub.png" alt="grg"/>
                        <figcaption>ДУБАИ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure>
                        <img src="images/img_city/pra.png" alt="oao"/>
                        <figcaption>ПРАГА</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure>
                        <img src="images/img_city/rome.png" alt="chz"/>
                        <figcaption>РИМ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure>
                        <img src="images/img_city/kyv.png" alt="blg"/>
                        <figcaption>КИЕВ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure>
                        <img src="images/img_city/dub.png" alt="blg"/>
                        <figcaption>ДУБАИ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    </div>
                    <div class="col-6 col-lg-4 col-xl">
                    <figure>
                        <img src="images/img_city/pra.png" alt="blg"/>
                        <figcaption>ПРАГА</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                </div>
                </div>
            </div>
        </div>
    </section>
    {{--
    <section class="section-settings" id="rentInCities">
        <div class="container">
            <h1 class="section-title-center">Популярные направления</h1>
            <div class="wrapper">
                <p>Ярким примером глобализации является наша компания. Мы начинали как небольшая фирма, но теперь мы
                    представлены в пяти странах мира, где имеем более 30 офисов и пунктов выдачи авто. Менее чем за
                    10
                    лет
                    мы построили отличную команду и обслужили огромное количество клиентов. Приблизительно раз в час
                    мы
                    выдаем автомобиль очередному клиенту.</p>
                <p>Сегодня офисы нашей компании представлены в Украине, Грузии, Крыму, ОАЭ, Чехии. Если Вы хотите
                    арендовать
                    автомобиль в любом из этих регионов, Вы можете оформить заявку на сайте, либо позвонить на один
                    из
                    удобных для Вас номеров.</p>
                <div class="img-line">
                    <figure>
                        <img src="images/img_city/front_kiev1.png" alt="ukr"/>
                        <figcaption>КИЕВ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure class="active">
                        <img src="images/img_city/dub.png" alt="grg"/>
                        <figcaption>ДУБАИ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure>
                        <img src="images/img_city/pra.png" alt="oao"/>
                        <figcaption>ПРАГА</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure>
                        <img src="images/img_city/rome.png" alt="chz"/>
                        <figcaption>РИМ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure>
                        <img src="images/img_city/front_kiev1.png" alt="blg"/>
                        <figcaption>КИЕВ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure>
                        <img src="images/img_city/dub.png" alt="blg"/>
                        <figcaption>ДУБАИ</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                    <figure>
                        <img src="images/img_city/pra.png" alt="blg"/>
                        <figcaption>ПРАГА</figcaption>
                        <div class="rent-price">
                            <p>Аренда авто</p>
                            <p>от 20$/сутки</p>
                        </div>
                    </figure>
                </div>
            </div>
        </div>
    </section>
--}}
    {{--advantages--}}
  {{--  <section class="section-settings" id="advantages">
        <h1 class="section-title-center">Наши преимущества</h1>
        <div class="container">
            <div class="d-flex">

                <div class="card active">
                    <div class="card-header">
                        <h3>Индивидуальный подход</h3>
                    </div>
                    <div class="card-container">
                        <p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками.
                        </p>
                    </div>
                </div>

                <div class="card ">
                    <div class="card-header">
                        <h3>Прокат авто в Украине</h3>
                    </div>
                    <div class="card-container">
                        <p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p>
                    </div>
                </div>

                <div class="card ">
                    <div class="card-header">
                        <h3>Прокат авто в Украине</h3>
                    </div>
                    <div class="card-container">
                        <p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p>
                    </div>
                </div>

                <div class="card ">
                    <div class="card-header">
                        <h3>Прокат авто в Украине</h3>
                    </div>
                    <div class="card-container">
                        <p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p>
                    </div>
                </div>

                <div class="card ">
                    <div class="card-header">
                        <h3>Прокат авто в Украине</h3>
                    </div>
                    <div class="card-container">
                        <p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>--}}
    <section id="advantages" class="section-settings"><h1 class="section-title-center">Наши преимущества</h1>
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 col-xl m-1 m-xl-3  card active">
                    <div class="card-header"><h3>Индивидуальный подход</h3></div>
                    <div class="card-container"><p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками.
                        </p></div>
                </div>
                <div class="col-12 col-lg-6 col-xl m-1 m-xl-3  card ">
                    <div class="card-header"><h3>Прокат авто в Украине</h3></div>
                    <div class="card-container"><p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p></div>
                </div>
                <div class="col-12 col-lg-6 col-xl m-1 m-xl-3  card ">
                    <div class="card-header"><h3>Прокат авто в Украине</h3></div>
                    <div class="card-container"><p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p></div>
                </div>
                <div class="col-12 col-lg-6 col-xl m-1 m-xl-3  card ">
                    <div class="card-header"><h3>Прокат авто в Украине</h3></div>
                    <div class="card-container"><p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p></div>
                </div>
                <div class="col-12 col-lg-6 col-xl m-1 m-xl-3  card ">
                    <div class="card-header"><h3>Прокат авто в Украине</h3></div>
                    <div class="card-container"><p>
                            Если Вы когда-то сталкивались с услугой проката авто, несомненно Вы обращали внимание на
                            локальные и международные автопрокатные компании. Весь рынок мирового автопроката
                            представлен крупными торговыми марками и локальными игроками. Если еще 10-20 лет назад
                            мы
                            могли говорить о неоспоримом преимуществе глобальных игроков мирового автопроката, то
                            сегодня их вес пошатан и они понемногу утрачивают позиции.
                        </p></div>
                </div>
            </div>
        </div>
    </section>

    {{--aboutCompany--}}
    <section class="section-settings" id="aboutCompany">
        <div class="container">
            <h1 class="section-title-center">О международной компании Lion</h1>
            <div class="wrapper">
                <p>Ярким примером глобализации является наша компания. Мы начинали как небольшая фирма, но теперь мы
                    представлены в пяти странах мира, где имеем более 30 офисов и пунктов выдачи авто. Менее чем за
                    10
                    лет мы построили отличную команду и обслужили огромное количество клиентов. Приблизительно раз в
                    час
                    мы выдаем автомобиль очередному клиенту.</p>
                <p>Сегодня офисы нашей компании представлены в Украине, Грузии, Крыму, ОАЭ, Чехии. Если Вы хотите
                    арендовать автомобиль в любом из этих регионов, Вы можете оформить заявку на сайте, либо
                    позвонить
                    нам.</p>
            </div>
        </div>
    </section>

    {{--prefooter--}}
    <section class="section-settings" id="prefooter">
        <div class="container">
            <div class="d-flex justify-content-between border-t">
                <div class="phone-box">
                    <p class="phone-box-title">Звоните на один из удобных для Вас номеров:</p>
                    <p>Для клиентов из Украины: <span>+38 (067) 780-86-99</span></p>
                    <p>Для клиентов из России: <span>+7 (978) 782-52-42</span></p>
                    <p>Для клиентов из Грузии: <span>(+995) 514-646-444</span></p>
                </div>

                <div class="mail-box">
                    <p class="mail-box-title">Звоните на один из удобных для Вас номеров:</p>
                    <p>Для вопросов: <span>promo@lion-avtoprokat.com.ua</span></p>
                    <p>Для оформления заказа: <span>zakaz@lion-avtoprokat.com.ua</span></p>
                </div>
            </div>
        </div>
    </section>

    {{--vaucher--}}
    <section class="section-settings" id="vaucher">
        <div class="container">
            <h1 class="text-center"></h1>

            <div class="head-red row text-center">
                <div class="col">
                    <h4>Подача</h4>
                    <div>25/08/2018 - 18 : 00</div>
                    <div>Киев, Офис, ул. Здолбуновская, 7Г</div>
                </div>
                <div class="col">
                    <h2>7 ДНЕЙ</h2>
                </div>
                <div class="col">
                    <h4>Возврат</h4>
                    <div>25/08/2018 - 18 : 00</div>
                    <div>Киев, Офис, ул. Здолбуновская, 7Г</div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12 col-md-8 personal-data">
                    <div class="wrap-vaucher">
                        <div class="per-data-head-dark">
                            <h4>Личные данные</h4>
                        </div>
                        <div class="per-data">
                            <div class="row m-0  form-group">
                                <div class="col-12 col-md-4 col-auto form-line">
                                    <div>ФИО</div>
                                    <input type="text" name="" id="" class="form-control" placeholder="ФИО" aria-describedby="helpId">
                                    <small id="helpId" class="text-muted"></small>
                                </div>
                                <div class="col-12 col-md-4 col-auto form-line">
                                    <div>Телефон</div>
                                    <input type="tel" name="" id="" class="form-control" placeholder="+" aria-describedby="helpId">
                                    <small id="helpId" class="text-muted"></small>
                                </div>
                                <div class="col-12 col-md-4 col-auto form-line">
                                    <div>Email</div>
                                    <input type="email" name="" id="" class="form-control" placeholder="Email"  aria-describedby="helpId">
                                    <small id="helpId" class="text-muted"></small>
                                </div>
                            </div>

                            <div class="col-12 form-group mt-3">
                                <div>Комментарий</div>
                                <textarea name="" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="per-data-head-dark">
                            <h4>Дополнительные опции</h4>
                        </div>
                        <div class="per-data flex-column p-3">
                        <label class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                            <div class="contain ml-3">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </div>
                        </label>

                        <label class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                            <div class="contain ml-3">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </div>
                        </label>

                        <label class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                            <div class="contain ml-3">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </div>
                        </label>

                        <div class="line-b border-bottom total">
                            <p class="mt-3">Внимание! Мы гарантируем выдачу автомобиля в классе с заявленной коробкой передач, мы обязательно учтем ваши пожелания по конкретной марке автомобиля. Выбрав форму оплаты и нажимая заказать вы соглашаетесь с условиями платежа и бронирования изложенными на сайте в разделе условия проката , а так же договора оферты</p>

                        </div>
                    </div>
                        <div class="container">

                            <div class="row">
                                <div class="col-12 col-md-6 mb-3">
                                    {{--<script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                    <div class="g-recaptcha" data-sitekey="your_site_key"></div>--}}
                                </div>
                                <div class="col-12 col-md-6">
                                    <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                        Заказать
                                    </button>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-12 col-md-4 ravon">
                    <div class="row ravon-head ravon-head-red-border-under p-0">
                        <h3 class="text-uppercase ml-3">Ravon r2</h3>
                        <span class="col">или аналоги</span>
                    </div>
                    <div class="ravon-img">
                        <img src="/images/ravon.jpg" alt="picsum">
                    </div>
                    <div class="ravon-param">
                        <div class="container">
                            <div class="row justify-content-between">
                                <div class="col-auto col-sm-auto col-md-auto">
                                    <i class="ravon-icon ravon-icon-user">5 Мест</i>
                                </div>
                                <div class="col-auto col-sm-auto col-md-auto">
                                    <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                </div>
                                <div class="col-auto col-sm-auto col-md-auto">
                                    <i class="ravon-icon ravon-icon-snowflake">Кондиционер</i>
                                </div>
                                <div class="col-auto col-sm-auto col-md-auto">
                                    <i class="ravon-icon ravon-icon-transmission">Коробка передач механическая</i>
                                </div>
                                <div class="col-auto col-sm-auto col-md-auto">
                                    <i class="ravon-icon ravon-icon-engine">3600<span>см<sup>2</sup></span></i>
                                </div>
                            </div>
                        </div>

                        <div class="row ravon-head">
                            <div class="col-12 per-data-head-dark">
                                <h4>Стоимость</h4>
                            </div>
                            <div class="container price-info">
                                <p>Детское кресло</p>
                                <p>Навигатор</p>
                                <p>Взять авто без залога (доп. страховка)</p>
                            </div>
                        </div>
                    </div>

                    <div class="ravon-head text-center">
                        <p class="col-12">Итоговая стоимость за 7 дней</p>
                        <h1 class="col-12 font-weight-bold">355.45$</h1>
                    </div>
                </div>
            </div>

        </div>
    </section>

    {{--order-block--}}
    <section class="section-settings" id="order-block">
        <div class="container">
            <h1 class="text-center text-break ">ЗАКАЗ №123456789101112</h1>

            <div class="head-red row text-center">
                <div class="col">
                    <h4>Подача</h4>
                    <div>25/08/2018 - 18 : 00</div>
                    <div>Киев, Офис, ул. Здолбуновская, 7Г</div>
                </div>
                <div class="col">
                    <h2>7 ДНЕЙ</h2>
                </div>
                <div class="col">
                    <h4>Возврат</h4>
                    <div>25/08/2018 - 18 : 00</div>
                    <div>Киев, Офис, ул. Здолбуновская, 7Г</div>
                </div>
            </div>

            <div class="row">
                <div class="personal-data mr-md-5">
                    <div class="per-data-head-dark">
                        <h4>Личные данные</h4>
                    </div>
                    <div class="per-data ">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="">ФИО</label>
                                <input type="text" name="" id="" class="form-control" placeholder="ФИО"
                                       aria-describedby="helpId">
                                <small id="helpId" class="text-muted"></small>
                            </div>
                            <div class="form-line">
                                <label for="">Телефон</label>
                                <input type="tel" name="" id="" class="form-control" placeholder="+"
                                       aria-describedby="helpId">
                                <small id="helpId" class="text-muted"></small>
                            </div>
                            <div class="form-line">
                                <label for="">Email</label>
                                <input type="email" name="" id="" class="form-control" placeholder="Email"
                                       aria-describedby="helpId">
                                <small id="helpId" class="text-muted"></small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Комментарий</label>
                            <textarea name="" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="per-data-head-dark">
                        <h4>Дополнительные опции</h4>
                    </div>
                    <div class="per-data flex-column">
                        <div class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                           {{-- <label class="contain">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>--}}
                        </div>
                        <div class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                            {{--<label class="contain">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>--}}
                        </div>
                        <div class="line-b border-bottom">
                            <div class="per-data-title">Навигатор</div>
                            <div class="per-data-price">25$</div>
                           {{-- <label class="contain ">
                                <input type="checkbox" checked="checked">
                                <span class="checkmark"></span>
                            </label>--}}
                        </div>

                        <div class="line-b border-bottom total">
                            <div class="per-data-title">Итоговая стоимость за 7 дней</div>
                            <div class="per-data-price">25$</div>

                        </div>
                    </div>
                </div>
                <div class="ravon">
                    <div class="ravon-head ravon-head-red-border-under ">
                        <h4>Ravon r2 </h4>
                        <span>или аналоги</span>
                    </div>
                    <div class="ravon-img">
                        <img src="/images/ravon.jpg" alt="picsum">
                    </div>
                    <div class="ravon-param">
                        <div class="row">
                            <div class="col-4">
                                <img src="/images/svg/icon_asisst/man.svg" alt="user">
                                5 Мест
                            </div>
                            <div class="col-4">
                                <img src="/images/svg/icon_asisst/engine.svg" alt="engine">
                                3600 см3
                            </div>
                            <div class="col-4">
                                <img src="/images/svg/icon_asisst/snowflake.svg" alt="snowflake">
                                Кондиционер
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <img src="/images/svg/icon_asisst/gaz.svg" alt="gaz">
                                Бензин
                            </div>

                            <div class="col">
                                <img src="/images/svg/icon_asisst/transmission.svg" alt="transmission">
                                Коробка передач механическая
                            </div>
                        </div>
                    </div>
                    <p>Внимание! Мы гарантируем выдачу автомобиля в классе с заявленной коробкой передач, мы
                        обязательно учтем ваши пожелания по конкретной марке автомобиля. </p>
                    <p class="text-info">Для получения автомобиля не забудьте взять паспорт, водительское
                        удостоверение и деньги</p>
                </div>
            </div>

            <div class="head-black row text-center">
                <div class="col">
                    <h4>Адрес</h4>
                    <div></div>
                    <div>Киев, Офис, ул. Здолбуновская, 7Г</div>
                </div>
                <div class="col">
                    <h2>Email</h2>
                    <div></div>
                    <div>lion@avtoprokat.com.ua</div>
                </div>
                <div class="col">
                    <h4>Телефон</h4>
                    <div>+38 (067) 780-86-99</div>
                    <div>(круглосуточно)</div>
                </div>
            </div>
        </div>
    </section>

    {{--ordered--}}
    <section class="section-settings" id="ordered">
        <div class="container page_order">

            <div class="container-sidebar">
                <div class="container container-relative">

                    <div class="selectorCar">
                        <div class="box-title">Подбор авто</div>
                        <div class="box-body">

                            <form action="" method="post" role="form">


                                <div class="form-group">
                                    <input type="text" class="form-control" name="" id="" placeholder="Выберите страну">

                                    <input type="text" class="form-control" name="" id=""
                                           placeholder="Введите место подачи">

                                    <div class="d-flex row">
                                        <div class="col-6">
                                            <label for="c1">Подача</label>
                                            <div class="calendar">
                                                <input type="text" id="c1" class="form-control"
                                                       placeholder="Выберите страну">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="">&nbsp;</label>
                                            <div class="clock">
                                                <input type="text" class="form-control" placeholder="Введите место подачи">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-checkbox">
                                        <input type="checkbox" value="" id="r2">
                                        <label class="form-checkbox-label" for="r2">
                                            Вернуть в другом месте
                                        </label>
                                    </div>

                                    <div class="d-flex row">
                                        <div class="col-6">
                                            <label for="c2">Возврат</label>
                                            <div class="calendar">
                                                <input type="text" id="c2" class="form-control" placeholder="Выберите страну">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label for="">&nbsp;</label>
                                            <div class="clock">
                                                <input type="text" class="form-control" placeholder="Введите место подачи">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="filter mt-4 boxshadow">
                        <div class="filter-body">
                            <div class="per-data-head-dark"><h5>Тип автомобиля</h5></div>
                            <div class="filter-box">
                                <ul class="check-list">
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Все типы
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox" checked="checked"/>
                                            <span class="checkmark"></span>
                                            Мини
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Эконом
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Стандарт
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Бизнес
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            SUV
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Премиум
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Внедорожник
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Гибрид
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <part-sidebar sidebar-title="typeCarList"></part-sidebar>

                    <div class="filter mt-4 boxshadow">
                        <div class="filter-body">
                            <div class="per-data-head-dark"><h5>Коробка передач</h5></div>
                            <div class="filter-box">
                                <ul class="check-list">
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Все типы
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Механика
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Автомат
                                        </label>
                                    </li>
                                    <li>
                                        <label class="contain">
                                            <input type="checkbox"/>
                                            <span class="checkmark"></span>
                                            Робот
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="filter mt-4 boxshadow">
                    <div class="filter-body">
                        <div class="per-data-head-dark"><h5>Топливо</h5></div>
                        <div class="filter-box">
                            <ul class="check-list">
                                <li>
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                        Все типы
                                    </label>
                                </li>
                                <li>
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                        Бензин
                                    </label>
                                </li>
                                <li>
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                        Дизель
                                    </label>
                                </li>
                                <li>
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                        Электро
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                </div>
            </div>


            <div class="container-page">
                <div class="d-flex flex-wrap justify-content-between">
                    <h1>Прокат авто в Днепре</h1>
                    <div class="dropdown mb-2">
                        <button type="button" id="triggerId" data-toggle="dropdown" class="btn dropdown-toggle">
                            от дорогих к дешевым
                        </button>
                        <div aria-labelledby="triggerId" class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item">от дешевых к дорогим</a>
                            <a href="#" class="dropdown-item">по популярности</a>
                        </div>
                        <div class="btn-group">
                            <button id="liste" type="button" class="btn btn-default  d-none d-lg-block">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16">
                                    <path d="M15.2 0H5.73a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h9.47a.8.8 0 0 0 .8-.8V.8a.8.8 0 0 0-.8-.8zm0 6.4H5.73a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h9.47a.8.8 0 0 0 .8-.8V7.2a.8.8 0 0 0-.8-.8zm0 6.4H5.73a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h9.47a.8.8 0 0 0 .8-.8v-1.6a.8.8 0 0 0-.8-.8zM2.4 0H.8a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h1.6a.8.8 0 0 0 .8-.8V.8a.8.8 0 0 0-.8-.8zm0 6.4H.8a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h1.6a.8.8 0 0 0 .8-.8V7.2a.8.8 0 0 0-.8-.8zm0 6.4H.8a.8.8 0 0 0-.8.8v1.6a.8.8 0 0 0 .8.8h1.6a.8.8 0 0 0 .8-.8v-1.6a.8.8 0 0 0-.8-.8z" fill-rule="evenodd"></path>
                                </svg>
                            </button>
                            <button id="tile" type="button" class="btn btn-default d-none d-lg-block" style="fill: rgb(213, 0, 28);">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16">
                                    <path d="M4.44.09H.53A.53.53 0 0 0 0 .62v3.91a.53.53 0 0 0 .53.53h3.91a.53.53 0 0 0 .53-.53V.62a.54.54 0 0 0-.53-.53zm0 5.38H.53A.53.53 0 0 0 0 6v3.91a.53.53 0 0 0 .53.53h3.91a.53.53 0 0 0 .53-.53V6a.54.54 0 0 0-.53-.53zm0 5.56H.53a.53.53 0 0 0-.53.52v3.92a.53.53 0 0 0 .53.52h3.91a.53.53 0 0 0 .53-.52v-3.92a.53.53 0 0 0-.53-.52zM9.95 0H6.04a.53.53 0 0 0-.52.53v3.91a.53.53 0 0 0 .52.53h3.91a.53.53 0 0 0 .53-.53V.53A.53.53 0 0 0 9.95 0zm0 5.38H6.04a.52.52 0 0 0-.52.53v3.91a.52.52 0 0 0 .52.52h3.91a.53.53 0 0 0 .53-.52V5.91a.53.53 0 0 0-.53-.53zm0 5.56H6.04a.52.52 0 0 0-.52.52v3.92a.52.52 0 0 0 .52.52h3.91a.53.53 0 0 0 .53-.52v-3.92a.53.53 0 0 0-.53-.52zM15.47 0h-3.91a.53.53 0 0 0-.53.53v3.91a.53.53 0 0 0 .53.53h3.91a.53.53 0 0 0 .52-.53V.53a.53.53 0 0 0-.52-.53zm0 5.38h-3.91a.53.53 0 0 0-.53.53v3.91a.53.53 0 0 0 .53.53h3.91a.53.53 0 0 0 .52-.53V5.91a.53.53 0 0 0-.52-.53zm0 5.56h-3.91a.53.53 0 0 0-.53.52v3.92a.53.53 0 0 0 .53.52h3.91a.52.52 0 0 0 .52-.52v-3.92a.52.52 0 0 0-.52-.52z" fill-rule="evenodd"></path>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>




                {{--part-car-wrapper--}}
                <div class="part-car-wrapper mb-2">
                    <div class="ravon-tail">
                        <div class="ravon-tail-head border_bottom-red m-3">
                            <h4>KIA Rio</h4>
                            <span>или аналоги</span>
                            <i class="ravon-tail-type">Средний</i>
                        </div>

                        <div class="ravon-tail-img pl-3 pt-3 pr-3">
                            <img src="/images/cars/Ford_Mondeo_FDAV_2015_1500_gray.jpg" alt="NO IMAGE">
                        </div>

                        <div class="ravon-tail-services p-3">
                            <i class="ravon-icon ravon-icon-user">4</i>
                            <i class="ravon-icon ravon-icon-gaz">бензин</i>
                            <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                            <i class="ravon-icon ravon-icon-transmission">А</i>
                            <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                        </div>

                        <div class="price">
                            <div class="d-flex text-center head-dark">
                                <div class="col">Дни</div>
                                <div class="col">Цена за сутки</div>
                            </div>
                            <div class="price-body pl-1 pr-1 pb-0">
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">1-6</div>
                                    <div class="col border-bottom">25</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">7-16</div>
                                    <div class="col border-bottom">21</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">17-26</div>
                                    <div class="col border-bottom">18</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">27+</div>
                                    <div class="col border-bottom">13</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">Залог</div>
                                    <div class="col border-bottom">600</div>
                                </div>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                            Заказать
                        </button>

                    </div>
                </div>
                {{-- LINE!!!!!!!  part-car-wrapper-line--}}
                <div class="part-car-wrapper-line mb-2">
                    <div class="ravon-tail">
                        <div class="ravon-tail-head border_bottom-red m-3">
                            <h4>KIA Rio</h4>
                            <span>или аналоги</span>
                            <i class="ravon-tail-type">Средний</i>
                        </div>

                        <div class="ravon-tail-img pl-3 pt-3 pr-3">
                            <img src="/images/cars/Toyota_Corolla_CDAV_2013_1600_gray.jpg" alt="NO IMAGE">
                        </div>

                        <div class="ravon-tail-services p-3">
                            <i class="ravon-icon ravon-icon-user">4</i>
                            <i class="ravon-icon ravon-icon-gaz">бензин</i>
                            <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                            <i class="ravon-icon ravon-icon-transmission">А</i>
                            <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                        </div>

                        <div class="price p-3">
                            <div class="d-flex text-center head-dark">
                                <div class="col">Дни</div>
                                <div class="col">Цена за сутки</div>
                            </div>
                            <div class="price-body pl-1 pr-1 pb-0">
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">1-6</div>
                                    <div class="col border-bottom">25</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">7-16</div>
                                    <div class="col border-bottom">21</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">17-26</div>
                                    <div class="col border-bottom">18</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">27+</div>
                                    <div class="col border-bottom">13</div>
                                </div>
                                <div class="d-flex text-center">
                                    <div class="col border-bottom border-right">Залог</div>
                                    <div class="col border-bottom">600</div>
                                </div>
                            </div>

                            <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                Заказать
                            </button>
                        </div>
                    </div>
                </div>

                {{-- WITH PRICE!!!!!!!!!!!!!!!!!! --}}

                {{--part-car-wrapper--}}
                <div class="part-car-wrapper mb-2">
                    <div class="ravon-tail">
                        <div class="ravon-tail-head border_bottom-red m-3">
                            <h4>KIA Rio</h4>
                            <span>или аналоги</span>
                            <i class="ravon-tail-type">Средний</i>
                        </div>

                        <div class="ravon-tail-img pl-3 pt-3 pr-3">
                            <img src="/images/cars/VolksWagen_Jetta_TSI_CDAF_2015_1800_gray.jpg" alt="NO IMAGE">
                        </div>

                        <div class="ravon-tail-services p-3">
                            <i class="ravon-icon ravon-icon-user">4</i>
                            <i class="ravon-icon ravon-icon-gaz">бензин</i>
                            <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                            <i class="ravon-icon ravon-icon-transmission">А</i>
                            <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                        </div>

                        <div class="price">

                            <div class="ravon-head text-center">
                                <p class="col-12">Итоговая стоимость за 7 дней</p>
                                <h1 class="col-12 font-weight-bold">355.45$</h1>
                            </div>
                        </div>

                        <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                            Заказать
                        </button>

                    </div>
                </div>

                {{-- LINE!!!!!!!  part-car-wrapper-line--}}
                <div class="part-car-wrapper-line mb-2">
                    <div class="ravon-tail">
                        <div class="ravon-tail-head border_bottom-red m-3">
                            <h4>KIA Rio</h4>
                            <span>или аналоги</span>
                            <i class="ravon-type">Средний</i>
                        </div>

                        <div class="ravon-tail-img pl-3 pt-3 pr-3">
                            <img src="/images/cars/Volkswagen_Passat_FDAV_2013_1800_black.jpg" alt="NO IMAGE">
                        </div>

                        <div class="ravon-tail-services p-3">
                            <i class="ravon-icon ravon-icon-user">4</i>
                            <i class="ravon-icon ravon-icon-gaz">бензин</i>
                            <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                            <i class="ravon-icon ravon-icon-transmission">А</i>
                            <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                        </div>

                        <div class="price p-3">

                            <div class="ravon-head text-center">
                                <p class="col-12">Итоговая стоимость за 7 дней</p>
                                <h1 class="col-12 font-weight-bold">355.45$</h1>
                            </div>

                            <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                Заказать
                            </button>
                        </div>


                        <div class="add-serv">
                            <div class="offset-none col-12 offset-md-4 col-md-8">
                                <div class="d-flex justify-content-between mb-0 p-0 mb-md-3 p-md-3">
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark mr-2"></span>
                                        Оплатить сейчас
                                    </label>
                                    <label class="contain">
                                        <input type="checkbox">
                                        <span class="checkmark mr-2"></span>
                                        Оплатить при получении авто
                                    </label>
                                    <a href="#">Условия аренды</a>
                                </div>
                            </div>
                            <div class="price container">
                                <div class="row  head-dark justify-content-between  align-items-center">
                                    <div class="col logo-car text-center">
                                        <img src="/images/brands_cars/bmv.png" width="35" height="35" alt="ravon">
                                    </div>
                                    <div class="col">Безлимитный километраж</div>
                                    <div class="col">Бесплатная отмена брони</div>
                                    <div class="col">Изменения в брони</div>
                                    <div class="col">Без франшизы</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {{--<div class="row">
                    tile
                    <div class="col-12 col-md-6 col-lg-4 col-xl-4 mb-4">
                        <div class="ravon">
                            <div class="listtile">
                                <div class="ravon-head  ravon-head-red-border-under ">
                                    <h4>KIA Rio</h4>
                                    <span>или аналоги</span>
                                    <i class="ravon-type">Средний</i>
                                </div>
                                <div class="ravon-img col p-3">
                                    <img src="/images/cars/notfound.jpg" alt="NO IMAGE">
                                </div>
                                <div class="ravon-services">
                                    <i class="ravon-icon ravon-icon-user">4</i>
                                    <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                    <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                    <i class="ravon-icon ravon-icon-transmission">А</i>
                                    <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                </div>
                                <div class="price">
                                    <div class="d-flex text-center head-dark">
                                        <div class="col">Дни</div>
                                        <div class="col">Цена за сутки</div>
                                    </div>
                                    <div class="price-body pl-1 pr-1 pb-0">
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">1-6</div>
                                            <div class="col border-bottom">25</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">7-16</div>
                                            <div class="col border-bottom">21</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">17-26</div>
                                            <div class="col border-bottom">18</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">27+</div>
                                            <div class="col border-bottom">13</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">Залог</div>
                                            <div class="col border-bottom">600</div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                    Заказать
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 col-xl-4 mb-4">
                        <div class="ravon">
                            <div class="listtile">
                                <div class="ravon-head  ravon-head-red-border-under ">
                                    <h4>KIA Rio</h4>
                                    <span>или аналоги</span>
                                    <i class="ravon-type">Средний</i>
                                </div>
                                <div class="ravon-img col p-3">
                                    <img src='/images/cars/Mazda_6_FDAV_2013_2000_white.jpg' alt="picsum">
                                </div>
                                <div class="ravon-services">
                                    <i class="ravon-icon ravon-icon-user">4</i>
                                    <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                    <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                    <i class="ravon-icon ravon-icon-transmission">А</i>
                                    <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                </div>
                                <div class="price">
                                    <div class="d-flex text-center head-dark">
                                        <div class="col">Дни</div>
                                        <div class="col">Цена за сутки</div>
                                    </div>
                                    <div class="price-body pl-1 pr-1 pb-0">
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">1-6</div>
                                            <div class="col border-bottom">25</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">7-16</div>
                                            <div class="col border-bottom">21</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">17-26</div>
                                            <div class="col border-bottom">18</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">27+</div>
                                            <div class="col border-bottom">13</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">Залог</div>
                                            <div class="col border-bottom">600</div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                    Заказать
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 col-xl-4 mb-4">
                        <div class="ravon">
                            <div class="listtile">
                                <div class="ravon-head  ravon-head-red-border-under ">
                                    <h4>KIA Rio</h4>
                                    <span>или аналоги</span>
                                    <i class="ravon-type">Средний</i>
                                </div>
                                <div class="ravon-img col p-3">
                                    <img src='/images/cars/Toyota_Corolla_CDAV_2013_1600_gray.jpg' alt="picsum">
                                </div>
                                <div class="ravon-services">
                                    <i class="ravon-icon ravon-icon-user">4</i>
                                    <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                    <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                    <i class="ravon-icon ravon-icon-transmission">А</i>
                                    <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                </div>
                                <div class="price">
                                    <div class="d-flex text-center head-dark">
                                        <div class="col">Дни</div>
                                        <div class="col">Цена за сутки</div>
                                    </div>
                                    <div class="price-body pl-1 pr-1 pb-0">
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">1-6</div>
                                            <div class="col border-bottom">25</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">7-16</div>
                                            <div class="col border-bottom">21</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">17-26</div>
                                            <div class="col border-bottom">18</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">27+</div>
                                            <div class="col border-bottom">13</div>
                                        </div>
                                        <div class="d-flex text-center">
                                            <div class="col border-bottom border-right">Залог</div>
                                            <div class="col border-bottom">600</div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                    Заказать
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    line
                    <div class="col-12">
                        <div class="ravon">
                            <div class="container ravon-list">
                                <div class="row">
                                    <div class="ravon-img col-12 col-md-4 p-3">
                                        <img src="/images/cars/notfound.jpg" alt="picsum">
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <div class="ravon-head ravon-head-red-border-under m-0 m-sm-3">
                                            <h4>KIA Rio</h4>
                                            <span>или аналоги</span>
                                            <i class="ravon-type">Средний</i>
                                        </div>
                                        <div class="container border-bottom m-0 pb-0 m-md-3 pb-md-3">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="ravon-services d-block d-md-flex  text-center text-md-left">
                                                        <i class="ravon-icon ravon-icon-user">4</i>
                                                        <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                                        <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                                        <i class="ravon-icon ravon-icon-transmission">А</i>
                                                        <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 p-0">
                                                    <p class="ito-price">Итоговая стоимость за 7 дней</p>
                                                    <div class="price-big">
                                                        355.45$
                                                    </div>
                                                </div>
                                                <div class="offset-none col-12 offset-md-6 col-md-6 ">
                                                    <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                                        Заказать
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-none col-12 offset-md-4 col-md-8">
                                        <div class="d-flex justify-content-between mb-0 p-0 mb-md-3 p-md-3"><label
                                                    class="contain"><input type="checkbox"> <span
                                                        class="checkmark mr-2"></span>
                                                Оплатить сейчас
                                            </label> <label class="contain"><input type="checkbox"> <span
                                                        class="checkmark mr-2"></span>
                                                Оплатить при получении авто
                                            </label> <a href="#">Условия аренды</a></div>
                                    </div>
                                    <div class="price container">
                                        <div class="row  head-dark justify-content-between  align-items-center">
                                            <div class="col logo-car"><img src="/images/brands_cars/bmv.png"
                                                                           alt="ravon"></div>
                                            <div class="col">Безлимитный километраж</div>
                                            <div class="col">Бесплатная отмена брони</div>
                                            <div class="col">Изменения в брони</div>
                                            <div class="col">Без франшизы</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="ravon">
                            <div class="container ravon-list">
                                <div class="row">
                                    <div class="ravon-img col-12 col-md-4 p-3">
                                        <img src="/images/cars/Mazda_6_FDAV_2013_2000_white.jpg" alt="picsum">
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <div class="ravon-head ravon-head-red-border-under m-0 m-sm-3"><h4>KIA Rio</h4>
                                            <span>или аналоги</span> <i class="ravon-type">Средний</i></div>
                                        <div class="container border-bottom m-0 pb-0 m-md-3 pb-md-3">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="ravon-services d-block d-md-flex  text-center text-md-left">
                                                        <i class="ravon-icon ravon-icon-user">4</i>
                                                        <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                                        <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                                        <i class="ravon-icon ravon-icon-transmission">А</i>
                                                        <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 p-0">
                                                    <p class="ito-price">Итоговая стоимость за 7 дней</p>
                                                    <div class="price-big">
                                                        355.45$
                                                    </div>
                                                </div>

                                                <div class="offset-none col-12 offset-md-6 col-md-6 ">
                                                    <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                                        Заказать
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-none col-12 offset-md-4 col-md-8">
                                        <div class="d-flex justify-content-between mb-0 p-0 mb-md-3 p-md-3">
                                            <label class="contain">
                                                <input type="checkbox">
                                                <span class="checkmark mr-2"></span>
                                                Оплатить сейчас
                                            </label>
                                            <label class="contain">
                                                <input type="checkbox">
                                                <span class="checkmark mr-2"></span>
                                                Оплатить при получении авто
                                            </label> <a href="#">Условия аренды</a>
                                        </div>
                                    </div>
                                    <div class="price container">
                                        <div class="row  head-dark justify-content-between  align-items-center">
                                            <div class="col logo-car">
                                                <img src="/images/brands_cars/bmv.png" alt="ravon">
                                            </div>
                                            <div class="col">Безлимитный километраж</div>
                                            <div class="col">Бесплатная отмена брони</div>
                                            <div class="col">Изменения в брони</div>
                                            <div class="col">Без франшизы</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="ravon">
                            <div class="container ravon-list">
                                <div class="row">
                                    <div class="ravon-img col-12 col-md-4 p-3">
                                        <img src="/images/cars/Toyota_Corolla_CDAV_2013_1600_gray.jpg" alt="picsum">
                                    </div>
                                    <div class="col-12 col-md-8">
                                        <div class="ravon-head ravon-head-red-border-under m-0 m-sm-3"><h4>KIA Rio</h4>
                                            <span>или аналоги</span>
                                            <i class="ravon-type">Средний</i>
                                        </div>
                                        <div class="container border-bottom m-0 pb-0 m-md-3 pb-md-3">
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="ravon-services d-block d-md-flex  text-center text-md-left">
                                                        <i class="ravon-icon ravon-icon-user">4</i>
                                                        <i class="ravon-icon ravon-icon-gaz">бензин</i>
                                                        <i class="ravon-icon ravon-icon-snowflake">Есть</i>
                                                        <i class="ravon-icon ravon-icon-transmission">А</i>
                                                        <i class="ravon-icon ravon-icon-engine">1400<span>см<sup>2</sup></span></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 p-0">
                                                    <p class="ito-price">
                                                        Итоговая стоимость за 7 дней</p>
                                                    <div class="price-big">
                                                        355.45$
                                                    </div>
                                                </div>

                                                <div class="offset-none col-12 offset-md-6 col-md-6 ">
                                                    <button type="button" class="btn btn-primary  btn-lg btn-block rounded-0">
                                                        Заказать
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="offset-none col-12 offset-md-4 col-md-8">
                                        <div class="d-flex justify-content-between mb-0 p-0 mb-md-3 p-md-3">
                                            <label class="contain"><input type="checkbox">
                                                <span class="checkmark mr-2"></span>
                                                Оплатить сейчас
                                            </label>
                                            <label class="contain"><input type="checkbox">
                                                <span class="checkmark mr-2"></span>
                                                Оплатить при получении авто
                                            </label>
                                            <a href="#">Условия аренды</a>
                                        </div>
                                    </div>
                                    <div class="price container">
                                        <div class="row  head-dark justify-content-between  align-items-center">
                                            <div class="col logo-car">
                                                <img src="/images/brands_cars/bmv.png" alt="ravon">
                                            </div>
                                            <div class="col">Безлимитный километраж</div>
                                            <div class="col">Бесплатная отмена брони</div>
                                            <div class="col">Изменения в брони</div>
                                            <div class="col">Без франшизы</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}

                <div class="d-flex justify-content-center mt-5">
                    <Paginator></Paginator>
                </div>
            </div>
        </div>

    </section>


    {{--map--}}
    <section class="section-settings" id="contacts-block">
        <contacts-page></contacts-page>
    </section>


    {{--rental-condition--}}
    <section class="section-settings" id="rental-condition">
        <div class="container">
            <div class="per-data-head-dark mb-3">
                <h1>Дополнительные опции</h1>
            </div>

            <div class="per-data-head-dark mb-3">
                <h4>Документы и ограничения по водителю</h4>
            </div>
            <ol>
                <li>Паспорт гражданина или иностранный паспорт, по которому въехал иностранец на территорию Украины </li>
                <li>Водительское удостоверение с категорией «В»</li>
                <li>Стаж вождения – 2 года, если водительское удостоверение менялось, то отметка о стаже вождения (ставится напротив категории «В» или просто в любом месте на водительском удостоверении – во многих странах обязательно проставляется)</li>
                <li>Возраст водителя – не менее 21 года</li>
            </ol>

            <div class="per-data-head-dark mb-3">
                <h4>Документы и ограничения по водителю</h4>
            </div>
                <p>Если вы хотите взять автомобиль в прокат прямо сегодня – можно просто приехать в офис и взять машину, для ускорения процедуры выдачи автомобиля – перезвоните нам и мы сразу же начнем готовить автомобиль для вас.</p>
                <p>Если вы хотите взять автомобиль в прокат прямо сегодня – можно просто приехать в офис и взять машину, для ускорения процедуры выдачи автомобиля – перезвоните нам и мы сразу же начнем готовить автомобиль для вас.</p>

            <p>При предварительном бронировании можно делать предоплату или не делать. В период высокого сезона, всегда лучше делать предоплату, для того чтобы вам не сообщали, что автомобилей осталось ограниченное количество и лучше сделать предоплату именно сегодня.</p>

            <p>Высокий сезон или время, когда автомобили заказывают больше всего это:</p>

            <ol>
                <li>Через форму заказа на <a href="#">сайте</a></li>
                <li>По телефону <a href="tel:(+38) 067-644-6-555">(+38) 067-644-6-555</a></li>
                <li>Через электронную почту <a href="mailto:info@race.net.ua">info@race.net.ua</a></li>
                <li>С помощью он-лайн чата</li>
                <li>Скайп <a href="https://race.ua/">race-ukraine</a></li>
                <li><a href="#">Вайбер</a> или <a href="#">ВотсАпп</a></li>
                <li>Посетив наш <a href="#">офис</a></li>
            </ol>

            <p>Заказать прокат автомобиля можно:</p>

            <ol>
                <li>Наличными в любом нашем офисе</li>
                <li>Банковской картой через <a href="#">сайт</a></li>
                <li>Безналичным перечислением на счета нашей компании</li>
                <li>Пополнением карты ПриватБанка в Украине</li>
                <li>Переводом денег с помощью международных платежных переводов: Western Union, Money Gram, Золотая корона</li>
                <li>Оплатой электронными деньгами WebMoney на любой наш кошелек</li>
            </ol>

            <p >Оплатой электронными деньгами WebMoney на любой наш кошелек</p>



            <div class="per-data-head-dark mb-3">
                <h4>Отказаться от бронирования, аннулировать бронь</h4>
            </div>
            <p>Любое бронирование может быть аннулировано без потери предоплаты минимум за 72 часа до начала проката автомобиля, если иное не предусмотрено специальными условиями бронирования или акции. То есть вы сможете вернуть обратно сумму предоплаты, если отказались от бронировки ранее чем 72 часа до начала Вашего проката.
            </p>
            <p>Если вы отказываетесь от бронирования менее чем за 72 часа до начала Вашего проката авто - мы не вернем Вашу сумму предоплаты.</p>
            <p>Внимание! При возврате предоплаты в установленный срок, если предоплата подлежит 100% возврату, прокатной компанией не возвращаются комиссии банка, стоимость переводов, сборы, налоги и подобные платежи, которые могут взиматься страной перечисления или получения, банковским учреждением, другими предприятиями, учреждениями, организациями, государственными органами, которые в силу договорных отношений или законодательства обязаны удержать такую сумму или удерживают ее и в случаях возврата платежа не возвращают уплату комиссий, налогов, сборов и т.д.</p>

            <div class="per-data-head-dark mb-3">
                <h4>Страховка</h4>
            </div>
            <p>Каждый автомобиль застрахован по программе полное КАСКО, т.е. от всевозможных повреждений. «Полное КАСКО» не означает что хочу то и делаю или как хочу так и еду и ничего не плачу. «Полное КАСКО» - это наличие страховки при наступлении таких случаев: дорожно-транспортное происшествие, угон автомобиля, противоправные действия третьих лиц, падение предметов на автомобиль, пожар, попадание молнии, ущерб нанесенный животными...
            </p>
            <p>Ответственность клиента ограничена суммой залога за автомобиль не зависимо от наличия или отсутствия вины в наступлении дорожно-транспортного происшествия. Ответственность по всем страховым рискам, кроме угона может быть уменьшена путем дополнительной оплаты к сумме проката, т.е. приобретения большего страхового покрытия на автомобиль. О сумме доплаты спрашивайте у наших операторов.</p>
            <p>Страховка не распространяется на любые случаи, произошедшие при таких обстоятельствах:</p>

            <p>
                Автомобилем управлял человек не вписанный в договор проката <br>
                Автомобилем управлял водитель находящийся в состоянии алкогольного или наркотического опьян <br>
                Автомобилем управлял человек без документов на управление или с просроченными докумен <br>
                Водитель автомобиля скрылся с места происшес <br>
                Дорожно-транспортное происшествие не прошедшее регистрацию в органах ГАИ и страховой компа <br>
                Отказ в проведении медицинского освидетельствования после совершения дорожно-транспортного происшествия – прохождение медицинского анализа на алкоголь и нарко <br>
                Участие на прокатном автомобиле в гонках, использование в качестве т <br>
                Перевозка негабаритных грузов, перевозка которых допускается только в специальных условиях или грузовыми автомоби <br>
                Гидроудар – попадание воды в двигатель в результате чего двигатель автомобиля заклини <br>
                Повреждение колес не вызванное дорожно-транспортным происшествием – дырка, прокол колеса, шишка на колесе, порез колеса, погнутость колесного диска на автомо <br>
                Дорожно-транспортное происшествие произошедшее в результате проезда на красный свет светофора, запрещающий сигнал регулировщика, железнодорожного переезда, пресечение сплошной линии разметки с выездом на встречную полосу движ <br>
                Утеря ключей, документов, номерных знаков на автомоб <br>
                Страховка ответственности также включена в стоимость проката авто. Страховка покрывает убытки которые возникли в результате дорожно-транспортного происшествия, а именно повреждения автомобилей других участников дорожного движения или пассажиров. <br>
            </p>

        </div>
    </section>










    {{--    <footer>
            <div class="container">
                <div class="d-flex justify-content-between">

                    <div class="box-footer">
                        <h5 class="box-foo-title">
                            Прокат авто в Украине
                        </h5>
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><p>+38 (067) 780-86-99</p></li>
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <h5 class="box-foo-title">
                            Прокат авто в Грузии
                        </h5>
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><p>(+995) 514-646-444</p></li>
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <h5 class="box-foo-title">
                            Прокат авто в ОАЭ
                        </h5>
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><p>(+995) 514-646-444</p></li>
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <h5 class="box-foo-title">
                            Прокат авто в Чехии
                        </h5>
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><p>(+995) 514-646-444</p></li>
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <h5 class="box-foo-title">
                            Прокат авто в Болгапии
                        </h5>
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                                <li><a href="#">Прокат авто в Украине</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="box-foo-text">
                            <ul>
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Условия проката</a></li>
                                <li><a href="#">Условия проката в Украине</a></li>
                                <li><a href="#">Условия проката в Грузии</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                        </div>
                        <div class="ss">
                            <div>Мы в социальных сетях</div>
                            <div class="ss-box">
                                <ul class="d-flex">
                                    <li>
                                        <a href="#">
                                            <img src="images/svg/play-button-yotube.svg" alt="yotube"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/svg/vk.com.svg" alt="vk" style="max-width: 40px;"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/svg/gmail.svg" alt="gmail"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="images/svg/instagram.svg" alt="instagram" style="max-width: 14px;"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </footer>--}}

@endsection
