
   @include('layouts.head')



<div id="app">
    <main>
        @include('layouts.header')
        @include('layouts.flash')
        @yield('content')
    </main>
</div>
   @include('layouts.footer')



