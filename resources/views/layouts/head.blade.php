<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- SEO -->
{{--    {!! SEO::generate() !!}--}}

{!! SEOMeta::generate() !!}

<!-- LEAFLET -->

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA==" crossorigin="anonymous"/>

    <!-- Styles -->
{{--     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="http://test.lion-avtoprokat.com.ua/public/css/app.css" rel="stylesheet">--}}

{{-- <link href="{{ asset('css/basic.css') }}" rel="stylesheet">  --}}



@if ( App::environment('local') )
    <!-- Styles local -->
       {{-- <link  rel="preload"  href="{{ asset('css/app.css') }}" as="style" onload="this.rel='style'">
        <script rel="preload"  src="{{ asset('js/app.js') }}" as="script" onload="this.rel='script'"></script>--}}
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('css/app.css') }}" rel="preload" onload='this.rel="stylesheet"' as="style">--}}
@endif
@if ( App::environment('production') )
    <!-- Styles production -->
{{--        <link rel="preload" href="{{env('APP_URL_PRODUCTION')}}/public/css/app.css" as="style"  onload="this.rel='style'">--}}
{{--        <script rel="preload" src="{{env('APP_URL_PRODUCTION')}}/public/js/app.js" as="script"  onload="this.rel='script'"></script>--}}
        <link  href="{{env('APP_URL_PRODUCTION')}}/public/css/app.css" rel="stylesheet">
    @endif
    {{--    <link href="http://test.lion-avtoprokat.com.ua/public/css/app.css" rel="stylesheet">--}}
    {{--<link href="{{ asset('css/basic.css') }}" rel="stylesheet">--}}
</head>
<body>
