<footer>
    <div class="container">
        <div class="row ">
            <div class="row col-md-12 align-items-center">
                <div class="pt-2 pt-md-0 col-md-6 col-lg-3 contacts  text-left">
                    <a href="https://msng.link/wa/380677808699" title="WhatsApp">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#4b4b4b" width="17" height="17" style="fill: rgb(13, 193, 67);"><path d="M11.04 3.93a13.61 13.61 0 0 0-5.09 0A2.94 2.94 0 0 0 4.11 5.7a9.51 9.51 0 0 0 0 4.18 2.96 2.96 0 0 0 1.84 1.77h.01a.07.07 0 0 1 .05.07v2.03a.12.12 0 0 0 .2.08l.96-.99.91-.93a.06.06 0 0 1 .05-.03 13.56 13.56 0 0 0 2.91-.23 2.96 2.96 0 0 0 1.85-1.77 9.51 9.51 0 0 0 0-4.18 2.93 2.93 0 0 0-1.85-1.77zm.03 6.05a1.56 1.56 0 0 1-.67.74 2.47 2.47 0 0 1-.29.1l-.33-.1a7.27 7.27 0 0 1-2.75-1.85 6.9 6.9 0 0 1-1.06-1.61c-.14-.28-.25-.56-.37-.85a.73.73 0 0 1 .22-.72 1.75 1.75 0 0 1 .57-.43.36.36 0 0 1 .45.11 5.77 5.77 0 0 1 .71.99.47.47 0 0 1-.13.64.44.44 0 0 0-.15.12.34.34 0 0 0-.12.12.33.33 0 0 0-.02.29 2.63 2.63 0 0 0 1.48 1.63.7.7 0 0 0 .37.09.98.98 0 0 0 .47-.41.45.45 0 0 1 .54-.02l.5.34a5.33 5.33 0 0 1 .47.36.37.37 0 0 1 .11.46zM9.73 7.55h-.02a.16.16 0 0 1-.17-.16l-.03-.22a.6.6 0 0 0-.41-.44 2.34 2.34 0 0 0-.26-.04.16.16 0 0 1 .05-.31.94.94 0 0 1 .96.93.56.56 0 0 1 0 .13.14.14 0 0 1-.12.11zM9.4 6.13a1.95 1.95 0 0 0-.52-.16l-.23-.03a.14.14 0 0 1-.14-.16.15.15 0 0 1 .16-.15 2.3 2.3 0 0 1 .89.23 1.8 1.8 0 0 1 .98 1.41.19.19 0 0 1 .02.08l.01.24a.39.39 0 0 1-.01.09.15.15 0 0 1-.29.02.3.3 0 0 1-.02-.13 1.6 1.6 0 0 0-.21-.83 1.56 1.56 0 0 0-.64-.61zm1.74 1.95a.17.17 0 0 1-.16-.19l-.05-.58a2.43 2.43 0 0 0-1.96-2.03 3.6 3.6 0 0 0-.46-.05.23.23 0 0 1-.25-.14.17.17 0 0 1 .16-.19h.08c1.31.04.08.01.01 0a2.77 2.77 0 0 1 2.71 2.29c.04.23.06.47.08.7a.17.17 0 0 1-.16.19zM0 0v17h17V0H0zm13.88 10.1l-.01.01a3.85 3.85 0 0 1-2.62 2.52l-.01.01a14.95 14.95 0 0 1-2.74.26q-.4 0-.81-.03l-1.24 1.29a.5.5 0 0 1-.85-.34V12.6a3.92 3.92 0 0 1-2.47-2.49l-.01-.01a10.9 10.9 0 0 1 0-4.62l.01-.01a3.88 3.88 0 0 1 2.62-2.53h.01a14.85 14.85 0 0 1 5.48 0h.01a3.88 3.88 0 0 1 2.62 2.53l.01.01a10.9 10.9 0 0 1 0 4.62z" fill-rule="evenodd"></path></svg>
                    </a>
                    <a href="https://msng.link/vi/380677808699" title="Viber">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#4b4b4b" width="17" height="17" style="fill: rgb(123, 81, 157);"><path d="M8.51 3.56a4.93 4.93 0 0 0-4.15 7.59l.12.19-.48 1.7 1.76-.45.18.11a4.93 4.93 0 1 0 2.57-9.14zM11.53 10l-.06.29a1.26 1.26 0 0 1-.54.79 1.68 1.68 0 0 1-1.39.17A6.18 6.18 0 0 1 6.17 8.8a2.66 2.66 0 0 1-.62-2.1 1.83 1.83 0 0 1 .57-.89.44.44 0 0 1 .33-.11l.39.02a.2.2 0 0 1 .17.12l.56 1.32a.18.18 0 0 1-.03.2l-.49.57a.16.16 0 0 0-.02.16A4.08 4.08 0 0 0 9.2 9.95a.14.14 0 0 0 .16-.05l.52-.66a.18.18 0 0 1 .24-.06l1.3.6a.2.2 0 0 1 .11.22zM0 0v17h17V0H0zm8.51 14.42a6.03 6.03 0 0 1-2.91-.75l-3.05.78.84-2.96a5.86 5.86 0 0 1-.82-3 5.93 5.93 0 1 1 5.94 5.93z" fill-rule="evenodd"></path></svg></a> <a href="tel: 380677808699" class="text-white"><i class="el-icon-mobile-phone"></i> <span>UA</span>+380 67 780 86 99
                    </a>
                </div>
                <div class="col-md-1 ml-md-auto ss ">
                    <ul class="d-flex justify-content-end align-items-center">
                        <li><a href="https://www.youtube.com/channel/UCZqJsOv-RQ5KjB23p0iKSJQ"><img src="/images/svg/icon_footer/youtube.svg" alt="yotube"></a></li>
                        <li><a href="mailto:zakaz@lion-avtoprokat.com.ua&subject=🦁"><img src="/images/svg/icon_footer/gmail.svg" alt="gmail"></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row pb-3">
            @if (Route::current()->getName()  == "order.form")

                <div class="footer-card col-md-3 pt-md-5 pt-2 pb-3">
                    <h5 class="border_bottom-red">@lang('form.rent_a_car') @lang('form._ukraine')</h5>
                    <p>
                        <a href="/autopark-country/ukraine?id=1" class="d-inline-block  text-white">
                            @lang('menu.autopark') @lang('form.in_ukraine')
                        </a>
                    </p>
                    <p>
                        <a href="/conditions/ukraine?id=1" class="d-inline-block  text-white">
                            @lang('menu.conditions')
                        </a>
                    </p>
                    <p>
                        <a href="/contacts/ukraine?id=1" class="d-inline-block  text-white">
                            @lang('menu.contacts')
                        </a>
                    </p>
                </div>
                <div class="footer-card col-md-3 pt-5 pb-3"><h5 class="border_bottom-red">@lang('form.rent_a_car') @lang('form._georgia')</h5>
                    <p>
                        <a href="/autopark-country/georgia?id=3" class="d-inline-block  text-white">
                            @lang('menu.autopark') @lang('form._georgia')
                        </a>
                    </p>
                    <p>
                        <a href="/conditions/georgia?id=3" class="d-inline-block  text-white">
                            @lang('menu.conditions')
                        </a>
                    </p>
                    <p>
                        <a href="/contacts/georgia?id=3" class="d-inline-block  text-white">
                            @lang('menu.contacts')
                        </a>
                    </p>
                </div>
                <div class="footer-card col-md-3 pt-5 pb-3"><h5 class="border_bottom-red">@lang('form.rent_a_car') @lang('form._czech')</h5>
                    <p><a href="/autopark-country/czech-republic?id=4"
                          class="d-inline-block  text-white">
                            @lang('menu.autopark') @lang('form._czech')
                        </a>
                    </p>
                    <p>
                        <a href="/conditions/czech-republic?id=4" class="d-inline-block  text-white">
                            @lang('menu.conditions')
                        </a>
                    </p>
                    <p>
                        <a href="/contacts/czech-republic?id=4" class="d-inline-block  text-white">
                            @lang('menu.contacts')
                        </a>
                    </p>
                </div>
                <div class="footer-card col-md-3 pt-5 pb-3"><h5 class="border_bottom-red">@lang('form.rent_a_car') @lang('form._оае')</h5>
                    <p>
                        <a href="/autopark-country/uae?id=11" class="d-inline-block  text-white">
                            @lang('menu.autopark') @lang('form._оае')
                        </a>
                    </p>
                    <p>
                        <a href="/conditions/uae?id=11" class="d-inline-block  text-white">
                            @lang('menu.conditions')
                        </a>
                    </p>
                    <p>
                        <a href="/contacts/uae?id=11" class="d-inline-block  text-white">
                            @lang('menu.contacts')
                        </a>
                    </p>
                </div>

            @else

                @foreach($countries as $country)
                    <div class="footer-card col-md-3 pt-5 pb-3">
                        <h5 class="border_bottom-red">Прокат авто {{$country['country_localized']}}</h5>

                        <p>
                            <a class="d-inline-block  text-white" href="{{ route('autopark.country',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">
                                @lang('menu.autopark') {{$country['country_localized']}}
                            </a>
                        </p>
                        <p>
                            <a class="d-inline-block  text-white" href="{{ route('conditions',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">
                                @lang('menu.conditions')
                            </a>
                        </p>
                        <p>
                            <a class="d-inline-block  text-white" href="{{ route('contacts',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}"> @lang('menu.contacts') </a>
                        </p>
                    </div>
                @endforeach
            @endif
        </div>

{{--
        <div class="justify-content-between mt-3 pt-5">
            <div class="row">
            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <h5 class="box-foo-title">  Прокат авто в Украине  </h5>
                <div class="box-foo-text">
                    <ul>
                        <li><a href="/autopark-country/ukraine?id=1">Прокат авто в Украине</a></li>
                        <li><p>+38 (067) 780-86-99</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <h5 class="box-foo-title">
                    Прокат авто в Грузии
                </h5>
                <div class="box-foo-text">
                    <ul>
                        <li><a href="/autopark-country/georgia?id=3">Прокат авто в Грузии</a></li>

                        <li><p>(+995) 514-646-444</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <h5 class="box-foo-title">
                    Прокат авто в ОАЭ
                </h5>
                <div class="box-foo-text">
                    <ul>
                        <li><a href="/autopark-country/uae?id=11">Прокат авто в ОАЭ</a></li>

                        <li><p>(+995) 514-646-444</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <h5 class="box-foo-title">
                    Прокат авто в Чехии
                </h5>
                <div class="box-foo-text">
                    <ul>
                        <li><a href="/autopark-country/czech-republic?id=4">Прокат авто в Чехии</a></li>

                        <li><p>(+995) 514-646-444</p></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <h5 class="box-foo-title">
                    Прокат авто в Болгапии
                </h5>
                <div class="box-foo-text">
                    <ul>
                        <li><a href="#">Прокат авто в Украине</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-6 text-center text-lg-center col-lg-4 col-xl-2 box-footer">
                <div class="box-foo-text">
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a href="/conditions/ukraine?id=1">Условия проката</a></li>
                        <li><a href="/contacts/ukraine?id=1">Контакты</a></li>
                    </ul>
                </div>
                <div class="ss">
                    <div>Мы в социальных сетях</div>
                    <div class="ss-box">
                        <ul class="d-flex justify-content-around align-items-center">
                            <li>
                                <a href="https://www.youtube.com/channel/UCZqJsOv-RQ5KjB23p0iKSJQ">
                                    <img src="images/svg/icon_footer/youtube.svg" alt="yotube"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/svg/icon_footer/vk.svg" alt="vk" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/svg/icon_footer/gmail.svg" alt="gmail"/>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/svg/icon_footer/instagram.svg" alt="instagram" style="max-width: 14px;"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>

        </div>--}}
    </div>

</footer>


<!-- LEAFLET -->
<script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg==" crossorigin="anonymous"></script>

<!-- Scripts -->
{{--<script src="{{ asset('js/app.js') }}"></script>
<script src="http://test.lion-avtoprokat.com.ua/public/js/app.js"></script>--}}


@if (App::environment('local'))
    <!-- js local -->
    <script src="{{ asset('js/app.js') }}"></script>
@endif
@if (App::environment('production'))
    <!-- js production -->
    <script  src="{{env('APP_URL_PRODUCTION')}}/public/js/app.js"></script>
    @endif


    @yield('scripts')
    </body>
    </html>
