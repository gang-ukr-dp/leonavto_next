  <?php
    $varLang =[];

      foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties){
          $varLang[$localeCode]['url'] =  LaravelLocalization::getLocalizedURL($localeCode, null, [], true) ;
          $varLang[$localeCode]['native'] = $properties['native']  ;
      }

    /*dd($varLang) ;*/
  ?>

<!--  V 01.0001  -->
<header>

    <div class="container border-bottom border-sm-bottom-0">
        <div class="row">
            <nav class="col-2 col-lg-6  col-xl-5 box-1 navbar navbar-expand-lg" data-toggle="collapse" data-target="#navmain">
                <div class="d-md-inline-block d-lg-none m-auto collapsed  menu-icon" ><span></span></div>
                <div class="col htl-menu collapse navbar-collapse">
                    @if(($view_name == 'main')||($view_name == 'contacts')||($view_name == 'conditions'))
                        @include('layouts.navmain')
                    @endif
                    @if(($view_name == 'autopark')||($view_name == 'country'))
                        @include('layouts.navcountry')
                    @endif
                </div>
            </nav>
            <div class="col box-2 text-center d-none d-md-flex  align-items-center hidden-md ">
                <a href="{{ route('main') }}">
                    <img src="{{config('siteparams.logo')}}" alt="lion-avtoprokat" title="lion-avtoprokat.com.ua">
                </a>
            </div>
            <div class="col-3 col-md-3 box-2-2 p-0 d-flex align-items-center d-md-none">
                <a href="{{ route('main') }}" style="font-size: 36px;">LION
                   {{-- <img src="/images/Logo_mob.png" alt="Logo" width="100%" height="auto">--}}
                </a>
            </div>
            <div class="col-7 col-md box-3">
               <div class="box-free languge-valuta">
                    @if(($view_name !== 'order'))
                        <div class="valuta">
                            <currency-switch
                                    :all-currencies="{{json_encode($allCurrencies)}}"
                                    :required-currencies="{{json_encode($requiredCurrencies)}}"
                            ></currency-switch>
                        </div>
                    @endif
                       <div class="languge">
                           <languages-switch
                                   :all-languages="{{ json_encode( $varLang ) }}"
                                   :local-code="{{ json_encode(LaravelLocalization::getCurrentLocale()) }}"
                           ></languages-switch>
                       </div>


                </div>

                <div class="number_phone text-right">
                    <a href="https://msng.link/wa/380677808699" title="WhatsApp">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#4b4b4b" width="17" height="17" style="fill: #0dc143;">
                            <path d="M11.04 3.93a13.61 13.61 0 0 0-5.09 0A2.94 2.94 0 0 0 4.11 5.7a9.51 9.51 0 0 0 0 4.18 2.96 2.96 0 0 0 1.84 1.77h.01a.07.07 0 0 1 .05.07v2.03a.12.12 0 0 0 .2.08l.96-.99.91-.93a.06.06 0 0 1 .05-.03 13.56 13.56 0 0 0 2.91-.23 2.96 2.96 0 0 0 1.85-1.77 9.51 9.51 0 0 0 0-4.18 2.93 2.93 0 0 0-1.85-1.77zm.03 6.05a1.56 1.56 0 0 1-.67.74 2.47 2.47 0 0 1-.29.1l-.33-.1a7.27 7.27 0 0 1-2.75-1.85 6.9 6.9 0 0 1-1.06-1.61c-.14-.28-.25-.56-.37-.85a.73.73 0 0 1 .22-.72 1.75 1.75 0 0 1 .57-.43.36.36 0 0 1 .45.11 5.77 5.77 0 0 1 .71.99.47.47 0 0 1-.13.64.44.44 0 0 0-.15.12.34.34 0 0 0-.12.12.33.33 0 0 0-.02.29 2.63 2.63 0 0 0 1.48 1.63.7.7 0 0 0 .37.09.98.98 0 0 0 .47-.41.45.45 0 0 1 .54-.02l.5.34a5.33 5.33 0 0 1 .47.36.37.37 0 0 1 .11.46zM9.73 7.55h-.02a.16.16 0 0 1-.17-.16l-.03-.22a.6.6 0 0 0-.41-.44 2.34 2.34 0 0 0-.26-.04.16.16 0 0 1 .05-.31.94.94 0 0 1 .96.93.56.56 0 0 1 0 .13.14.14 0 0 1-.12.11zM9.4 6.13a1.95 1.95 0 0 0-.52-.16l-.23-.03a.14.14 0 0 1-.14-.16.15.15 0 0 1 .16-.15 2.3 2.3 0 0 1 .89.23 1.8 1.8 0 0 1 .98 1.41.19.19 0 0 1 .02.08l.01.24a.39.39 0 0 1-.01.09.15.15 0 0 1-.29.02.3.3 0 0 1-.02-.13 1.6 1.6 0 0 0-.21-.83 1.56 1.56 0 0 0-.64-.61zm1.74 1.95a.17.17 0 0 1-.16-.19l-.05-.58a2.43 2.43 0 0 0-1.96-2.03 3.6 3.6 0 0 0-.46-.05.23.23 0 0 1-.25-.14.17.17 0 0 1 .16-.19h.08c1.31.04.08.01.01 0a2.77 2.77 0 0 1 2.71 2.29c.04.23.06.47.08.7a.17.17 0 0 1-.16.19zM0 0v17h17V0H0zm13.88 10.1l-.01.01a3.85 3.85 0 0 1-2.62 2.52l-.01.01a14.95 14.95 0 0 1-2.74.26q-.4 0-.81-.03l-1.24 1.29a.5.5 0 0 1-.85-.34V12.6a3.92 3.92 0 0 1-2.47-2.49l-.01-.01a10.9 10.9 0 0 1 0-4.62l.01-.01a3.88 3.88 0 0 1 2.62-2.53h.01a14.85 14.85 0 0 1 5.48 0h.01a3.88 3.88 0 0 1 2.62 2.53l.01.01a10.9 10.9 0 0 1 0 4.62z"
                                  fill-rule="evenodd"/>
                        </svg>
                    </a>
                    <a href="https://msng.link/vi/380677808699"  title="Viber">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="#4b4b4b" width="17" height="17" style="fill: #7b519d;">
                            <path d="M8.51 3.56a4.93 4.93 0 0 0-4.15 7.59l.12.19-.48 1.7 1.76-.45.18.11a4.93 4.93 0 1 0 2.57-9.14zM11.53 10l-.06.29a1.26 1.26 0 0 1-.54.79 1.68 1.68 0 0 1-1.39.17A6.18 6.18 0 0 1 6.17 8.8a2.66 2.66 0 0 1-.62-2.1 1.83 1.83 0 0 1 .57-.89.44.44 0 0 1 .33-.11l.39.02a.2.2 0 0 1 .17.12l.56 1.32a.18.18 0 0 1-.03.2l-.49.57a.16.16 0 0 0-.02.16A4.08 4.08 0 0 0 9.2 9.95a.14.14 0 0 0 .16-.05l.52-.66a.18.18 0 0 1 .24-.06l1.3.6a.2.2 0 0 1 .11.22zM0 0v17h17V0H0zm8.51 14.42a6.03 6.03 0 0 1-2.91-.75l-3.05.78.84-2.96a5.86 5.86 0 0 1-.82-3 5.93 5.93 0 1 1 5.94 5.93z"
                                  fill-rule="evenodd"/>
                        </svg>
                    </a>
                    <a href="tel: +380 (67) 780 86 99">
                        <i class="el-icon-mobile-phone"></i>
                        <span>UA</span>
                       +380 67 780 86 99
                    </a>
                </div>
            </div>
        </div>




        <div class="collapse  navbar-collapse" id="navmain">
            {{--<div class="d-md-inline-block d-lg-none select-avto collapsed" data-toggle="collapse" data-target="#navmain">Подбор авто</div>--}}

                <div class="box-free languge-valuta-mob">
                    @if(($view_name !== 'order'))
                    <div class="valuta">
                        <currency-switch
                                :all-currencies="{{json_encode($allCurrencies)}}"
                                :required-currencies="{{json_encode($requiredCurrencies)}}"
                        ></currency-switch>
                    </div>
                    @endif
                    <div class="languge">
                        <languages-switch
                                :all-languages="{{ json_encode( $varLang ) }}"
                                :local-code="{{ json_encode(LaravelLocalization::getCurrentLocale()) }}"
                        ></languages-switch>
                    </div>
            </div>

            @if(($view_name == 'main')||($view_name == 'contacts')||($view_name == 'conditions'))
                @include('layouts.navmain')
            @endif
            @if(($view_name == 'autopark')||($view_name == 'country'))
                @include('layouts.navcountry')
            @endif
        </div>
    </div>
</header>


  @section('scripts')
      <script>
          $(".menu-icon").on("click", function(){
              $(this).toggleClass("open");
              $(".container").toggleClass("nav-open");
              $("nav ul li").toggleClass("animate");
          });
      </script>
  @endsection
