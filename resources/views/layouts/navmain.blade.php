
    <ul class="city-select">
        {{--{{ trans('message.hello', ['name' => 'visitor']) }}--}}
       {{-- <li><a class="nav-link active" href="{{ route('main') }}">@lang('menu.main')</a></li>--}}

        @foreach($countries as $country)
        <li class="nav-item htl-menu">
            {{--<a class="nav-link active" href="{{ route('country',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">{{$country['country_localized']}}</a>--}}
            <div class="nav-link text-nowrap"  >{{$country['country_localized']}}</div>
            <ul class="nav-secondary fadeIn animated faster menu-ul-1">
                <li class="nav-item icon-tools">
                    <a class="nav-link text-nowrap" href="{{ route('autopark.country',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.autopark')</a>
                </li>
                <li class="nav-item icon-tools">
                    <a class="nav-link text-nowrap" href="{{ route('conditions',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.conditions')</a>
                </li>
                <li class="nav-item icon-tools">
                    <a class="nav-link text-nowrap" href="{{ route('contacts',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.contacts')</a>
                </li>
            </ul>
        </li>
        @endforeach
    </ul>




