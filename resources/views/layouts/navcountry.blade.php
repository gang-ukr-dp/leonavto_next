<ul class="city-select">
    <li class="nav-item">
        <a class="nav-link active" href="{{ route('main') }}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" fill="#ccc" width="20" style="fill: #999;">
                <path d="M48.5 44.833h-3V24.038l1.968 1.395a1.5 1.5 0 0 0 2.368-1.224v-4a1.5 1.5 0 0 0-.633-1.224L40 12.462V6.083a1.5 1.5 0 1 0-3 0v4.252L25.867 2.443a1.5 1.5 0 0 0-1.734 0L.799 18.985a1.499 1.499 0 0 0-.633 1.224v4a1.5 1.5 0 0 0 2.367 1.224L4.5 24.038v20.795h-3a1.5 1.5 0 1 0 0 3h47a1.5 1.5 0 1 0 0-3zm-16.935 0V27.82a1.5 1.5 0 0 0-1.5-1.5h-10.13a1.5 1.5 0 0 0-1.5 1.5v17.013H7.5V22.249c0-.108-.012-.212-.034-.313L25 9.506l17.534 12.429a1.474 1.474 0 0 0-.034.314v22.584H31.565z"/></svg>
            {{--@lang('menu.main')--}}
        </a>
    </li>
    <li class="nav-item">
        {{--<a class="nav-link active" href="{{ route('autopark.country',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.autopark')</a>--}}
        <div class="nav-link" >@lang('menu.autopark')</div>
        <ul class="nav-secondary fadeIn animated p-2 faster menu-ul-2">
            @if ($cities)
                @foreach($citiesOfficesIdForSlack as $id => $city)
                    <li class="nav-item text-nowrap">
                        <a class="nav-link pr-1 pl-1" href="{{ route('autopark.city',['country' => str_slug($country['name_country_en']),
                                                                            'city' => str_slug($string=($pos=strpos($city,","))?substr($city,0,$pos):$city),
                                                                            'country_id' => $country['id'],
                                                                            'city_id' => $id
                                                                           ]) }}">{{ $string=($pos=strpos($citiesOfficesId[$id],","))?substr($citiesOfficesId[$id],0,$pos):$citiesOfficesId[$id]}}</a>
                    </li>
                @endforeach
            @else
                <li class="nav-item">
                    <a class="nav-link text-nowrap">@lang('form.noFound')</a>
                </li>
            @endif
        </ul>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="{{ route('conditions',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.conditions')</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" href="{{ route('contacts',['country' => str_slug($country['name_country_en']), 'id' => $country['id']]) }}">@lang('menu.contacts')</a>
    </li>
</ul>



