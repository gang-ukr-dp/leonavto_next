@extends('layouts.app')

@section('content')
    <div class="container">
        <section class="section-settings p-0" id="ordered">


         {{--   @if(isset($city) && $city)
                @if (LaravelLocalization::getCurrentLocale() == 'ru')<h1 class="p-3">Прокат авто {{$city->name}}</h1> @endif
                @if (LaravelLocalization::getCurrentLocale() == 'ua')<h1 class="p-3">Прокат авто {{$city->name_uk}}</h1> @endif
                @if (LaravelLocalization::getCurrentLocale() == 'en')<h1 class="p-3">Rent a car in {{$city->name_en}}</h1> @endif
            @else

                @if (LaravelLocalization::getCurrentLocale() == 'ru')<h1 class="p-3">Прокат авто {{$country->name_country_ru}}</h1> @endif
                @if (LaravelLocalization::getCurrentLocale() == 'ua')<h1 class="p-3">Прокат авто {{$country->name_country}}</h1> @endif
                @if (LaravelLocalization::getCurrentLocale() == 'en')<h1 class="p-3">Rent a car in {{$country->name_country_en}}</h1> @endif
            @endif--}}

            <div class="container page_order">

                <div class="container-sidebar mt-3">

                    <search-form
                            :countries="{{json_encode($countries)}}"
                            :country="{{json_encode($country)}}"
                            :get-cities-when-mounted="{{json_encode(true)}}"
                            :preset-form-params="{{json_encode($presetFormParams)}}"
                            :preset-city="{{isset($presetCity) ? json_encode($presetCity) : ''}}"
                    ></search-form>

                    <search-form-modal
                            :countries="{{json_encode($countries)}}"
                            :country="{{json_encode($country)}}"
                            :get-cities-when-mounted="{{json_encode(false)}}"
                            :preset-form-params="{{json_encode($presetFormParams)}}"
                            :preset-city="{{isset($presetCity) ? json_encode($presetCity) : ''}}"
                    ></search-form-modal>

                    <filters
                            :filters-prop="{{json_encode($allFilters)}}"
                            :cars-prop="{{json_encode($allCars)}}"
                    ></filters>

                </div>
                <?/*= var_dump($country)*/?>

                <cars-list
                        :cars-prop="{{json_encode($allCars)}}"
                        :cars-number="{{$carsNumber}}"
                        :intervals="{{json_encode($intervals)}}"
                        :country-city="@if (LaravelLocalization::getCurrentLocale() == 'ru') {{ json_encode($country->name_country_ru) }}@endif
                        @if (LaravelLocalization::getCurrentLocale() == 'ua') {{ json_encode($country->name_country) }}@endif
                        @if (LaravelLocalization::getCurrentLocale() == 'en') {{ json_encode($country->name_country_en) }}@endif"
                        :country-lang="{{ json_encode(LaravelLocalization::getCurrentLocale()) }}"
                ></cars-list>


            </div>

        </section>

    </div>

    @include('components.pre-footer')
@endsection


