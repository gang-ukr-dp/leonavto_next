<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>
    </title>
    <!--[if !mso]>  -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        .ReadMsgBody {
            width: 100%;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass * {
            line-height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width:480px) {
            @-ms-viewport {
                width: 320px;
            }

            @viewport {
                width: 320px;
            }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700&display=swap" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Raleway:400,600,700&display=swap);
    </style>
    <!--<![endif]-->
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }

            .mj-column-per-50 {
                width: 50% !important;
                max-width: 50%;
            }
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {
            table.full-width-mobile {
                width: 100% !important;
            }

            td.full-width-mobile {
                width: auto !important;
            }
        }
    </style>
</head>

<body style="background-color:#fff;">
<div style="background-color:#fff;">

    <!--[if mso | IE]>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">
        <tr>
            <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]-->
    <div style="background:#fff;background-color:#fff;Margin:0 auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff;background-color:#fff; width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr; font-size:0; padding:20px 0; text-align:center; vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align:top;width:600px;">
                    <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix"
                         style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="vertical-align:top;" width="100%">
                            <!-- Image Header -->
                            <tr>
                                <td align="center"
                                    style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="border-collapse:collapse;border-spacing:0;">
                                        <tbody>
                                        <tr>
                                            <td style="width:400px;">
                                                <a href="images/Logo.png" target="_blank">
                                                    <img height="auto" src="{{config('siteparams.logo')}}"
                                                         style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:12px;"
                                                         width="400">
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center"
                                    style="font-size:0;padding:0 8px;word-break:break-word;">
                                    <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:center;color:#7b7b7b;">
                                        <p>@lang('form.in_process')</p>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    style="font-size:0;padding:0 8px;word-break:break-word;">
                                    <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:center;color:#000000;">
                                        <h5 style="font-size: 20px; margin-bottom: 8px; font-weight: 500; line-height: 1.2;">
                                            @lang('certificate.order')<b style="color: #17a2b8 !important"> №
                                                {{$request->get('order_number')}}</b>
                                        </h5>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                            </td>
                        </tr>
                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <!--[if mso | IE]>
            </td>
        </tr>
    </table>

    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;" width="600" >
        <tr>
            <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
    <![endif]-->
                <div style="Margin:0  auto;max-width:600px;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                    <tbody>
                    <tr>
                        <td style="border:1px dotted #eee;direction:ltr;font-size:0;padding:2px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="cell-title-outlook" width="600px"  >
                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                           class="cell-title-outlook" style="width:594px;" width="594" >
                                        <tr>
                                            <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div class="cell-title" style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="margin: 0; white-space: nowrap; background-color: #dee2e6; width: 100%; padding: 1px 0 1px 2px;"
                               width="100%" bgcolor="#dee2e6">
                            <tbody style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                   bgcolor="#dee2e6">
                            <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                bgcolor="#dee2e6">
                                <td style="margin: 0; white-space: nowrap; background-color: #dee2e6; direction: ltr; font-size: 0; text-align: center; vertical-align: top; padding: 1px 0 1px 2px;"
                                    align="center" valign="top" bgcolor="#dee2e6">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr; padding: 1px 0 1px 2px;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:594px;" >
                                        <![endif]-->
                                        <div class="mj-column-per-100 outlook-group-fix"
                                             style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; padding: 1px 0 1px 2px;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="margin: 0; white-space: nowrap; background-color: #dee2e6; vertical-align: top; padding: 1px 0 1px 2px;"
                                                   width="100%" valign="top" bgcolor="#dee2e6">
                                                <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                                    bgcolor="#dee2e6">
                                                    <td align="left" vertical-align="middle"
                                                        style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0px; word-break: break-word; padding: 1px 0 1px 2px;"
                                                        bgcolor="#dee2e6">
                                                        <div style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-family: monospace; font-size: 12px; font-weight: 400; line-height: 1; text-align: left; color: #000000; padding: 1px 0 1px 2px;">
                                                            <h6 style="font-size: 16px; margin-bottom: 8px; font-weight: 500; line-height: 1.2; margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;">
                                                                @lang('form.personal_data')</h6>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        <tr>
                        <td class="" width="600px" >
                            <table align="center" border="0" cellpadding="0" cellspacing="0"  style="width:594px;" width="594" >
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0;padding:20px 0;padding-bottom:10px;padding-top:10px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;" >
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>

                                                <td style="vertical-align:top;width:297px;" >
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000;">
                                                            <b>@lang('form.fio'):</b>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000;">
                                                            <span>{{$request->get('userName')}}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px" >
                            <table  align="center" border="0" cellpadding="0" cellspacing="0" class=""
                                    style="width:594px;" width="594" >
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0;padding:20px 0;padding-bottom:10px;padding-top:10px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>

                                                <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                            <b>@lang('form.phone'):</b>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                            <span>{{$request->get('phone')}}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px" >

                            <table align="center" border="0" cellpadding="0" cellspacing="0" class=""
                                   style="width:594px;" width="594">
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0;padding:20px 0;padding-bottom:10px;padding-top:10px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                            <b>E-mail:</b>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>
                                            <td style="vertical-align:top;width:297px;">
                                                <![endif]-->
                                                <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                                        <tr>
                                                            <td align="left"
                                                                style="font-size:0;padding:0 8px;word-break:break-word;">
                                                                <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                                    <span>{{$request->get('email')}}</span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <!--[if mso | IE]>
                                            </td>
                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                                <tr>
                                    <td style="direction:ltr;padding:20px 8px;padding-bottom:10px;text-align: left;vertical-align:top;color: #7b7b7b;font-size: 12px;">
                                        <div class="comment">{{$request->get('comment')}}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="cell-title-outlook" width="600px">

                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="cell-title-outlook"
                                   style="width:594px;" width="594">
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div class="cell-title" style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="margin: 0; white-space: nowrap; background-color: #dee2e6; width: 100%; padding: 1px 0 1px 2px;"
                               width="100%" bgcolor="#dee2e6">
                            <tbody style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                   bgcolor="#dee2e6">
                            <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                bgcolor="#dee2e6">
                                <td style="margin: 0; white-space: nowrap; background-color: #dee2e6; direction: ltr; font-size: 0px; text-align: center; vertical-align: top; padding: 1px 0 1px 2px;"
                                    align="center" valign="top" bgcolor="#dee2e6">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr; padding: 1px 0 1px 2px;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:594px;" >
                                        <![endif]-->
                                        <div class="mj-column-per-100 outlook-group-fix"
                                             style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; padding: 1px 0 1px 2px;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="margin: 0; white-space: nowrap; background-color: #dee2e6; vertical-align: top; padding: 1px 0 1px 2px;"
                                                   width="100%" valign="top" bgcolor="#dee2e6">
                                                <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                                    bgcolor="#dee2e6">
                                                    <td align="left" vertical-align="middle"
                                                        style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0px; word-break: break-word; padding: 1px 0 1px 2px;"
                                                        bgcolor="#dee2e6">
                                                        <div style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-family: monospace; font-size: 12px; font-weight: 400; line-height: 1; text-align: left; color: #000000; padding: 1px 0 1px 2px;">
                                                            <h6 style="font-size: 16px; margin-bottom: 8px; font-weight: 500; line-height: 1.2; margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;">
                                                                @lang('certificate.order_details')</h6>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px" >
                            <table align="center" border="0" cellpadding="0" cellspacing="0"
                                   style="width:594px;" width="594" >
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:15px;padding-top:15px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td  class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;">
                                                            <b style="color: #2d2971; font-size: 14px; text-transform: uppercase;">@lang('form.get'):</b>
                                                            <p style="white-space: nowrap;">
                                                                <span style="color: #2d2971; font-family: monospace;">  &#128197; {{$request->get('dateStart')}}</span>
                                                                <span style="color: #2d2971; font-family: monospace;">&#128338; {{$request->get('timeStart')}}</span>
                                                            </p>
                                                            <div style="color: #2d2971; font-family: monospace;">{{json_decode($request->get('placeStart'))->city_location}}</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        <td style="vertical-align:top;width:297px;" >
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;">
                                                            <b style="color: #712024; font-size: 14px; text-transform: uppercase;">@lang('form.return'):</b>
                                                            <p style="white-space: nowrap;">
                                                                <span style="color: #712024; font-family: monospace;">&#128197;{{$request->get('dateFinish')}}</span>
                                                                <span style="color: #712024; font-family: monospace;">&#128338;{{$request->get('timeFinish')}}</span>
                                                            </p>
                                                            <div style="color: #712024; font-family: monospace;">{{json_decode($request->get('placeFinish'))->city_location}}</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px">

                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:594px;" width="594">
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0;padding:20px 0;padding-bottom:15px;padding-top:15px;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;" >
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="center" style="font-size:0;padding:10px 25px;word-break:break-word;">
                                                        <table border="0" cellpadding="0" cellspacing="0"
                                                               role="presentation"
                                                               style="border-collapse:collapse;border-spacing:0;">
                                                            <tbody>
                                                            <tr>
                                                                <td style="width:247px;">
                                                                    <img height="auto" src="http://test.lion-avtoprokat.com.ua/{{$car->image}}"
                                                                         style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:12px;"
                                                                         width="247">
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        <td style="vertical-align:top;width:297px;">
                                        <![endif]-->
                                        <div class="mj-column-per-50 outlook-group-fix"
                                             style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                   style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td align="left"  style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                            <div>
                                                                <h4 style="font-size: 24px; margin-bottom: 8px; font-weight: 500; line-height: 1.2;">
                                                                    {{$car->name}}</h4>
                                                                {{$car->vg}} <span class="text-black-50">•</span>
                                                                {{$car->pow}} <span class="text-black-50">•</span>
                                                                М <span class="text-black-50">•</span> Эконом
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px">

                            <tablealign="center" border="0" cellpadding="0" cellspacing="0" style="width:594px;" width="594">
                    <tr>
                        <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div style="Margin:0 auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            <tr>
                                <td style="direction:ltr;font-size:0;padding:20px 0;padding-top:0;text-align:center;vertical-align:top;">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                        <tr>

                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix"
                                         style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:594px;">
                                        <![endif]-->
                                        <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                                <tr>
                                                    <td style="font-size:0;padding:5px;word-break:break-word;">
                                                        <p style="border-top:dashed 1px lightgrey;font-size:1;margin:0 auto;width:100%;">
                                                        </p>
                                                        <!--[if mso | IE]>
                                                        <table align="center" border="0" cellpadding="0"
                                                               cellspacing="0"
                                                               style="border-top:dashed 1px lightgrey;font-size:1;margin:0 auto;width:584px;"
                                                               role="presentation" width="584px" >
                                                            <tr>
                                                                <td style="height:0;line-height:0;">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <![endif]-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" vertical-align="middle"
                                                        style="font-size:0;padding:0 8px;word-break:break-word;">
                                                        <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#7b7b7b;">
                                                            <!--<div style="">Внимание! Мы гарантируем выдачу автомобиля в классе с заявленной коробкой передач, мы обязательно учтем ваши пожелания по конкретной марке автомобиля. Выбрав форму оплаты и нажимая заказать вы соглашаетесь с условиями платежа и бронирования изложенными на сайте в разделе условия проката, а так же договора оферты</div>-->
                                                            <div style="color: #17a2b8 !important">
                                                                @lang('form.notice2')
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="cell-title-outlook" width="600px">

                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="cell-title-outlook" style="width:594px;" width="594">
                                <tr>
                                    <td style="line-height:0;font-size:0;mso-line-height-rule:exactly;">
                    <![endif]-->
                    <div class="cell-title" style="Margin:0px auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="margin: 0; white-space: nowrap; background-color: #dee2e6; width: 100%; padding: 1px 0 1px 2px;"
                               width="100%" bgcolor="#dee2e6">
                            <tbody style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                   bgcolor="#dee2e6">
                            <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                bgcolor="#dee2e6">
                                <td style="margin: 0; white-space: nowrap; background-color: #dee2e6; direction: ltr; font-size: 0px; text-align: center; vertical-align: top; padding: 1px 0 1px 2px;"
                                    align="center" valign="top" bgcolor="#dee2e6">
                                    <!--[if mso | IE]>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="" style="width:594px;">
                                    <![endif]-->
                                    <div class="mj-column-per-100 outlook-group-fix" style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr; padding: 1px 0 1px 2px;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="vertical-align:top;width:594px;">
                                        <![endif]-->
                                        <div class="mj-column-per-100 outlook-group-fix" style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; padding: 1px 0 1px 2px;">
                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="margin: 0; white-space: nowrap; background-color: #dee2e6; vertical-align: top; padding: 1px 0 1px 2px;" width="100%" valign="top" bgcolor="#dee2e6">
                                                <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;" bgcolor="#dee2e6">
                                                    <td align="left" vertical-align="middle" style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0px; word-break: break-word; padding: 1px 0 1px 2px;" bgcolor="#dee2e6">
                                                        <div style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-family: monospace; font-size: 12px; font-weight: 400; line-height: 1; text-align: left; color: #000000; padding: 1px 0 1px 2px;">
                                                            <h6 style="font-size: 16px; margin-bottom: 8px; font-weight: 500; line-height: 1.2; margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;">
                                                                Дополнительные опции
                                                            </h6>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </div>
                                    <!--[if mso | IE]>
                                    </td>

                                    </tr>

                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>
                    </td>
                    </tr>
                    </table>

                    </td>
                    </tr>

                    <tr>
                        <td class="" width="600px">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:594px;" width="594">
                                <tr>
                                    <td style="line-height:0 ;font-size:0 ;mso-line-height-rule:exactly;">
                    <![endif]-->

                    <div style="Margin:0  auto;max-width:594px;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="width:100%;">
                            <tbody>
                            @foreach($options as $option)
                                <tr>
                                    <td style="direction:ltr;font-size:0;padding-bottom:8px;padding-top:8px;text-align:center;vertical-align:top;">
                                        <!--[if mso | IE]>
                                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="" style="width:594px;">
                                        <![endif]-->
                                        <div class="mj-column-per-100 outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                                            <!--[if mso | IE]>
                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="vertical-align:top;width:297px;">
                                            <![endif]-->
                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
                                                    <tr>
                                                        <td align="left"
                                                            style="font-size:0;padding:0 8px;word-break:break-word;">
                                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000;">
                                                                <b>{{$option->name}}</b>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--[if mso | IE]>
                                            </td>
                                            <td style="vertical-align:top;width:297px;" >
                                            <![endif]-->
                                            <div class="mj-column-per-50 outlook-group-fix"
                                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"  style="vertical-align:top;" width="100%">
                                                    <tr>
                                                        <td align="right"   style="font-size:0;padding:0 8px;word-break:break-word;">
                                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:right;color:#000;">
                                                                <div>
                                                                    {{$option->price * intval($currencyRate)}}
                                                                    <span> {{$currencyImage}}</span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <!--[if mso | IE]>
                                            </td>

                                            </tr>
                                            </table>
                                            <![endif]-->
                                        </div>
                                        <!--[if mso | IE]>
                                        </td>

                                        </tr>

                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        </td>
        </tr>

        <!--[if mso | IE]>
        <tr>
            <td class="cell-title-outlook" width="600px" >
                <table  align="center" border="0" cellpadding="0" cellspacing="0" class="cell-title-outlook" style="width:594px;" width="594">
                    <tr>
                        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
        <div class="cell-title" style="Margin:0px auto;max-width:594px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                   style="margin: 0; white-space: nowrap; background-color: #dee2e6; width: 100%; padding: 1px 0 1px 2px;"
                   width="100%" bgcolor="#dee2e6">
                <tbody style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                       bgcolor="#dee2e6">
                <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                    bgcolor="#dee2e6">
                    <td style="margin: 0; white-space: nowrap; background-color: #dee2e6; direction: ltr; font-size: 0px; text-align: center; vertical-align: top; padding: 1px 0 1px 2px;"
                        align="center" valign="top" bgcolor="#dee2e6">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                                <td
                                        class="" style="width:594px;"
                                >
                        <![endif]-->
                        <div class="mj-column-per-100 outlook-group-fix"
                             style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0; line-height: 0; text-align: left; display: inline-block; width: 100%; direction: ltr; padding: 1px 0 1px 2px;">
                            <!--[if mso | IE]>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td style="vertical-align:top;width:594px;" >
                            <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix"
                                 style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; padding: 1px 0 1px 2px;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="margin: 0; white-space: nowrap; background-color: #dee2e6; vertical-align: top; padding: 1px 0 1px 2px;"
                                       width="100%" valign="top" bgcolor="#dee2e6">
                                    <tr style="margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;"
                                        bgcolor="#dee2e6">
                                        <td align="left" vertical-align="middle"
                                            style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-size: 0px; word-break: break-word; padding: 1px 0 1px 2px;"
                                            bgcolor="#dee2e6">
                                            <div style="margin: 0; white-space: nowrap; background-color: #dee2e6; font-family: monospace; font-size: 12px; font-weight: 400; line-height: 1; text-align: left; color: #000000; padding: 1px 0 1px 2px;">
                                                <h6 style="font-size: 16px; margin-bottom: 8px; font-weight: 500; line-height: 1.2; margin: 0; white-space: nowrap; background-color: #dee2e6; padding: 1px 0 1px 2px;">
                                                    @lang('certificate.total')
                                                </h6>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                        <!--[if mso | IE]>
                        </td>

                        </tr>

                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>
        </td>
        </tr>
        </table>

        </td>
        </tr>

        <tr>
            <td
                    class="" width="600px"
            >

                <table
                        align="center" border="0" cellpadding="0" cellspacing="0" class=""
                        style="width:594px;" width="594"
                >
                    <tr>
                        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
        <div style="Margin:0px auto;max-width:594px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                   style="width:100%;">
                <tbody>
                <tr>
                    <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:15px;padding-top:15px;text-align:center;vertical-align:top;">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                                <td
                                        class="" style="width:594px;"
                                >
                        <![endif]-->
                        <div class="mj-column-per-100 outlook-group-fix"
                             style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                            <!--[if mso | IE]>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td style="vertical-align:middle;width:297px;" >
                            <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix"
                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:50%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="vertical-align:middle;" width="100%">
                                    <tr>
                                        <td align="left"
                                            style="font-size:0px;padding:0 8px;word-break:break-word;">
                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                <b>@lang('certificate.deposit')</b>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            <td
                                    style="vertical-align:middle;width:297px;"
                            >
                            <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix"
                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:50%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="vertical-align:middle;" width="100%">
                                    <tr>
                                        <td align="right"
                                            style="font-size:0;padding:0 8px;word-break:break-word;">
                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:right;color:#000000;">
                                                <div class="text-right">
                                                    {{$request->get('priceZalog')}}<span> {{$currencyImage}}</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                        <!--[if mso | IE]>
                        </td>

                        </tr>

                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>
        </td>
        </tr>
        </table>

        </td>
        </tr>

        <tr>
            <td width="600px" >
                <table align="center" border="0" cellpadding="0" cellspacing="0"  style="width:594px;" width="594" >
                    <tr>
                        <td style="line-height:0;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
        <div style="Margin:0px auto;max-width:594px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                   style="width:100%;">
                <tbody>
                <tr>
                    <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:15px;padding-top:15px;text-align:center;vertical-align:top;">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                                <td class="" style="width:594px;" >
                        <![endif]-->
                        <div class="mj-column-per-100 outlook-group-fix"
                             style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                            <!--[if mso | IE]>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td style="vertical-align:middle;width:297px;" >
                            <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix"
                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:50%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="vertical-align:middle;" width="100%">
                                    <tr>
                                        <td align="left"
                                            style="font-size:0px;padding:0 8px;word-break:break-word;">
                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                <b>@lang('certificate.order')/{{$request->get('diffDays')}} {{$request->get('dayVar')}}</b>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            <td style="vertical-align:middle;width:297px;" >
                            <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix"
                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:50%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="vertical-align:middle;" width="100%">
                                    <tr>
                                        <td align="right"
                                            style="font-size:0px;padding:0 8px;word-break:break-word;">
                                            <div style="font-family:monospace;font-size:12px;font-weight:400;line-height:1;text-align:right;color:#000000;">
                                                <div style="font-size: 32px;">
                                                    {{$request->get('priceTotal') + $request->get('addOptionsPrice')}} <span> {{$currencyImage}}</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                        <!--[if mso | IE]>
                        </td>

                        </tr>

                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>
        </td>
        </tr>
        </table>

        </td>
        </tr>

        <tr>
            <td
                    class="" width="600px"
            >

                <table
                        align="center" border="0" cellpadding="0" cellspacing="0" class=""
                        style="width:594px;" width="594"
                >
                    <tr>
                        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
        <![endif]-->
        <div style="Margin:0px auto;max-width:594px;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
                   style="width:100%;">
                <tbody>
                <tr>
                    <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                        <!--[if mso | IE]>
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                            <tr>

                                <td
                                        class="" style="width:594px;"
                                >
                        <![endif]-->
                        <div class="mj-column-per-100 outlook-group-fix"
                             style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                            <!--[if mso | IE]>
                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <tr>

                                    <td
                                            style="vertical-align:top;width:594px;"
                                    >
                            <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix"
                                 style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                       style="vertical-align:top;" width="100%">
                                    <tr>
                                        <td align="left"
                                            style="font-size:0;padding:0 8px;word-break:break-word;">
                                            <div style="font-family:'Raleway', sans-serif;font-size:12px;font-weight:400;line-height:1;text-align:left;color:#000000;">
                                                <div style="font-size: 12px; color: #17a2b8; font-family: monospace;">
                                                    Для консультации Вы можете позвонить по номеру телефона:
                                                    в Украине <span style="white-space: nowrap">+38(067) 780-86-99</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <!--[if mso | IE]>
                            </td>

                            </tr>
                            </table>
                            <![endif]-->
                        </div>
                        <!--[if mso | IE]>
                        </td>

                        </tr>

                        </table>
                        <![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>
        </td>
        </tr>
        </table>

        </td>
        </tr>

        </table>
        <![endif]-->
        </td>
        </tr>
        </tbody>
        </table>
    </div>
    <!--[if mso | IE]>
            </td>
        </tr>
    </table>
    <![endif]-->
</div>
</body>
</html>
