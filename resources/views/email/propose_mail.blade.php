
<!doctype html>
<html lang="ru" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>

    </title>
    <!--[if !mso]> -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a { padding:0; }
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; }
        .ExternalClass * { line-height:100%; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Exo+2:400,500,700&subset=cyrillic" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Exo+2:400,500,700&subset=cyrillic);
    </style>
    <!--<![endif]-->



    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-50 { width:50% !important; max-width: 50%; }
            .mj-column-per-33 { width:33.333333333333336% !important; max-width: 33.333333333333336%; }
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
        }
    </style>


    <style type="text/css">



        @media only screen and (max-width:480px) {
            table.full-width-mobile { width: 100% !important; }
            td.full-width-mobile { width: auto !important; }
        }

    </style>


</head>
<body style="background-color:#ffffff;">


<div style="background-color:#ffffff;">

    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
        <tbody>
        <tr>
            <td>


                <!--[if mso | IE]>
                <table
                        align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                >
                    <tr>
                        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                <![endif]-->


                <div style="Margin:0px auto;max-width:900px;">

                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                        <tr>
                            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                                <!--[if mso | IE]>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                <![endif]-->

                                <!-- Order number logo -->
                                <!--[if mso | IE]>
                                <tr>
                                    <td
                                            class="" width="900px"
                                    >
                                <![endif]-->

                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                    <tbody>
                                    <tr>
                                        <td>


                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                            >
                                                <tr>
                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                            <![endif]-->


                                            <div style="Margin:0px auto;max-width:900px;">

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                            <!--[if mso | IE]>
                                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                                <tr>

                                                                    <td
                                                                            class="" style="vertical-align:middle;width:450px;"
                                                                    >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;" width="100%">

                                                                    <tr>
                                                                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="width:400px;">

                                                                                        <a href="http://lion-avtoprokat.com.ua/autopark.html" target="_blank">

                                                                                            <img height="auto" src="http://lion-avtoprokat.com.ua/images/lion_2.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="400">

                                                                                        </a>

                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            <td
                                                                    class="tar-outlook" style="vertical-align:middle;width:450px;"
                                                            >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix tar" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: middle; text-align: right;" width="100%" valign="middle" align="right">

                                                                    <tr style="text-align: right;" align="right">
                                                                        <td align="right" class="small" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: right;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-weight: 300; line-height: 1; text-align: right; font-size: 11px; color: #a7a7a7;">
                                                                                ПРЕДЛОЖЕНИЕ
                                                                            </div>

                                                                        </td>
                                                                    </tr>
                                                                    <!-- <mj-text mj-class="red-text"> <h3 style="color: #313131"><span style="color: #a7a7a7">предложение № </span>190404142853</h3></mj-text> -->
                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            </tr>

                                                            </table>
                                                            <![endif]-->
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            <!--[if mso | IE]>
                                            </td>
                                            </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                <![endif]-->
                                <!-- Order supply refund -->
                                <!--[if mso | IE]>
                                <tr>
                                    <td
                                            class="" width="900px"
                                    >
                                <![endif]-->

                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#3f51b5;background-color:#3f51b5;width:100%;">
                                    <tbody>
                                    <tr>
                                        <td>


                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                            >
                                                <tr>
                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                            <![endif]-->


                                            <div style="Margin:0px auto;max-width:900px;">

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                            <!--[if mso | IE]>
                                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                                <tr>

                                                                    <td
                                                                            class="tac-outlook" style="vertical-align:middle;width:300.00000000000006px;"
                                                                    >
                                                            <![endif]-->

                                                            <div class="mj-column-per-33 outlook-group-fix tac" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-right: 1px solid #fff; vertical-align: middle; text-align: center;" width="100%" valign="middle" align="center">

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <p style="font-size: 1rem; text-align: center;">ПОДАЧА</p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <p style="font-size: 1rem; text-align: center;"><span style="padding-right: 10px; text-align: center;">{{$request->get('timeStart')}}</span>{{$request->get('dateStart')}}</p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; padding-bottom: 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <div style="color: blanchedalmond; text-align: center;">{{json_decode($request->get('placeStart'))->city_location}}</div>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            <td
                                                                    class="tac-outlook" style="vertical-align:middle;width:300.00000000000006px;"
                                                            >
                                                            <![endif]-->

                                                            <div class="mj-column-per-33 outlook-group-fix tac" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align: middle; text-align: center;" width="100%" valign="middle" align="center">

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <h2 style="font-size: 24px; text-align: center;">{{$request->get('diffDays')}} {{$request->get('dayVar')}}</h2>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            <td
                                                                    class="tac-outlook" style="vertical-align:middle;width:300.00000000000006px;"
                                                            >
                                                            <![endif]-->

                                                            <div class="mj-column-per-33 outlook-group-fix tac" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-left: 1px solid #fff; vertical-align: middle; text-align: center;" width="100%" valign="middle" align="center">

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <p style="font-size: 1rem; text-align: center;">ВОЗВРАТ</p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <p style="font-size: 1rem; text-align: center;"><span style="padding-right: 10px; text-align: center;">{{$request->get('timeFinish')}}</span>{{$request->get('dateFinish')}}</p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr style="text-align: center;" align="center">
                                                                        <td align="center" style="font-size: 0px; padding: 0 15px; padding-bottom: 15px; word-break: break-word; text-align: center;">

                                                                            <div style="font-family: 'Exo 2', sans-serif; font-size: 14px; font-weight: 300; line-height: 1; color: #ffffff; text-align: center;">
                                                                                <div style="color: blanchedalmond; text-align: center;">{{json_decode($request->get('placeFinish'))->city_location}}</div>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            </tr>

                                                            </table>
                                                            <![endif]-->
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            <!--[if mso | IE]>
                                            </td>
                                            </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                <![endif]-->
                                <!-- Order body -->
                                <!--[if mso | IE]>
                                <tr>
                                    <td
                                            class="" width="900px"
                                    >
                                <![endif]-->

                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                    <tbody>
                                    <tr>
                                        <td>


                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                            >
                                                <tr>
                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                            <![endif]-->


                                            <div style="Margin:0px auto;max-width:900px;">

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                            <!--[if mso | IE]>
                                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                                <tr>

                                                                    <td
                                                                            class="" style="vertical-align:top;width:450px;"
                                                                    >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="background-color:#313131;vertical-align:top;" width="100%">

                                                                    <tr>
                                                                        <td align="left" style="font-size:0px;padding:0 15px;word-break:break-word;">

                                                                            <div style="font-family:'Exo 2', sans-serif;font-size:14px;font-weight:300;line-height:1;text-align:left;color:#ffffff;">
                                                                                <p style="font-size: 1rem;">Личные данные</p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            <td
                                                                    class="" style="vertical-align:middle;width:450px;"
                                                            >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;" width="100%">

                                                                    <tr>
                                                                        <td align="left" style="font-size:0px;padding:0 15px;word-break:break-word;">

                                                                            <div style="font-family:'Exo 2', sans-serif;font-size:14px;font-weight:300;line-height:1;text-align:left;color:#000000;">
                                                                                <p style="font-size: 1rem; border-bottom: 1px solid #d5001c;">
                                                                                    <span style="font-size: 18px; font-weight: 500; padding-bottom: 10px; display: inline-block;">{{$car->name}}</span>
                                                                                    <i style="font-size: 14px;font-weight: 400;padding-bottom: 10px;display: inline-block;text-align: right;float: right;color: #a7a7a7;">или аналоги</i>
                                                                                </p>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="right" vertical-align="middle" style="font-size:0px;padding:0 15px;word-break:break-word;">

                                                                            <div style="font-family:'Exo 2', sans-serif;font-size:14px;font-weight:300;line-height:1;text-align:right;color:#000000;">
                                                                                <div style="color: #a7a7a7;  font-size: 12px;">
                                                                                    <span>{{$car->qd}}</span>
                                                                                    <span style="color: #c6e7ff; font-size: 16px; display: inline-block; vertical-align: middle;">&bull;</span>
                                                                                    <span>{{$car->fuel_name}}</span>
                                                                                    <span style="color: #c6e7ff; font-size: 16px; display: inline-block; vertical-align: middle;">&bull;</span>
                                                                                    <span>{{$car->trans_name}}</span>
                                                                                    <span style="color: #c6e7ff; font-size: 16px; display: inline-block; vertical-align: middle;">&bull;</span>
                                                                                    <span>{{$car->class_name}}</span>
                                                                                    <span style="color: #c6e7ff; font-size: 16px; display: inline-block; vertical-align: middle;">&bull;</span>
                                                                                    <span>{{$car->sipp_name}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            </tr>

                                                            </table>
                                                            <![endif]-->
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            <!--[if mso | IE]>
                                            </td>
                                            </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!--[if mso | IE]>
                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div style="Margin:0px auto;max-width:900px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:900px;"
                                                        >
                                                <![endif]-->

                                                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                        <tr>
                                                            <td style="font-size:0px;word-break:break-word;">


                                                                <!--[if mso | IE]>

                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="10" style="vertical-align:top;height:10px;">

                                                                <![endif]-->

                                                                <div style="height:10px;">
                                                                    &nbsp;
                                                                </div>

                                                                <!--[if mso | IE]>

                                                                </td></tr></table>

                                                                <![endif]-->


                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >
                                <![endif]-->

                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                    <tbody>
                                    <tr>
                                        <td>


                                            <!--[if mso | IE]>
                                            <table
                                                    align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                            >
                                                <tr>
                                                    <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                            <![endif]-->


                                            <div style="Margin:0px auto;max-width:900px;">

                                                <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                            <!--[if mso | IE]>
                                                            <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                                <tr>

                                                                    <td
                                                                            class="" style="vertical-align:top;width:450px;"
                                                                    >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                                    <tr>
                                                                        <td align="left" style="font-size:0px;padding:0px;word-break:break-word;">

                                                                            <div style="font-family:'Exo 2', sans-serif;font-size:14px;font-weight:300;line-height:1;text-align:left;color:#000000;">
                                                                                <table width="100%" style="color: #000; font-family:'Exo 2', sans-serif; font-size:14px;  font-weight:300;">
                                                                                    <tbody>
                                                                                    <tr style="background: #eee;">
                                                                                        <td style="text-align: left;padding:5px 0; " valign="middle" colspan="2">
                                                                                            <div style="font-size: 22px;font-weight: 400;">
                                                                                                <span>Ваш заказ включает в себя:</span></div>
                                                                                        </td>
                                                                                    </tr>

                                                                                    <tr>
                                                                                        <td style="text-align: left;padding:5px 0;line-height: 22px" valign="middle">
                                                                                            <div style="border-bottom: dotted  1px #ccc;"><span> Цена за {{$request->get('diffDays')}} {{$request->get('dayVar')}}</span></div>
                                                                                        </td>
                                                                                        <td style="text-align: right;padding:5px 0;" valign="middle">
                                                                                            <div style="border-bottom: dotted  1px #ccc;line-height: 22px; font-size: 22px; font-weight: 500;">{{$request->get('priceTotal')}} <span> $</span></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="text-align: left;padding:5px 0;" valign="middle">
                                                                                            <div style="border-bottom: dotted  1px #ccc;"><span> Залог: </span></div>
                                                                                        </td>
                                                                                        <td style="text-align: right;padding:5px 0;" valign="middle">
                                                                                            <div style="border-bottom: dotted  1px #ccc;">{{$request->get('priceZalog')}} <span> $</span></div>
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="center" vertical-align="middle" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;">
                                                                                <tr>
                                                                                    <td align="center" bgcolor="#f45e43" role="presentation" style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#f45e43;" valign="middle">
                                                                                        <a href="http://vsudy.com/public/images/cars/5/Renault_Logan_EDMV_2013_1200_white.jpg" style="display:inline-block;background:#f45e43;color:white;font-family:Helvetica;font-size:14px;font-weight:300;line-height:120%;Margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;" target="_blank">
                                                                                            Don't click me!
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            <td
                                                                    class="" style="vertical-align:middle;width:450px;"
                                                            >
                                                            <![endif]-->

                                                            <div class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">

                                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;" width="100%">

                                                                    <tr>
                                                                        <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                                                                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td style="width:400px;">

                                                                                        <img height="auto" src="http://vsudy.com/public/images/cars/5/Renault_Logan_EDMV_2013_1200_white.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="400">
{{--                                                                                        <img height="auto" src="{{json_encode($car)->image}}" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="400">--}}

                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>

                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                                                                            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="cellspacing:0;color:#000000;font-family:'Exo 2', sans-serif;font-size:14px;line-height:22px;table-layout:auto;width:100%;">
                                                                                <table style="color: #000; font-family:'Exo 2', sans-serif; font-size:14px;  font-weight:300;">
                                                                                    <tbody>



                                                                                    <tr>
                                                                                        <td width="15%" style="text-align: center;vertical-align:middle;" valign="middle">
                                                                                            <img alt src="http://vsudy.com/public/images/icons/catalog/chars-gray/door.png">
                                                                                            <span style="color: #bcc0c6;">{{$car->qd}}</span>
                                                                                        </td>
                                                                                        <td width="15%" style="text-align: center;vertical-align:middle;" valign="middle">
                                                                                            <img alt src="http://vsudy.com/public/images/icons/catalog/chars-gray/air.png">
                                                                                            <span style="color: #bcc0c6;">есть</span>
                                                                                        </td>
                                                                                        <td width="15%" style="text-align: center;vertical-align:middle;" valign="middle">
                                                                                            <img alt src="http://vsudy.com/public/images/icons/catalog/chars-gray/user.png">
                                                                                            <span style="color: #bcc0c6;">{{$car->qm}}</span>
                                                                                        </td>
                                                                                        <td width="15%" style="text-align: center;vertical-align:middle;" valign="middle">
                                                                                            <img alt src="http://vsudy.com/public/images/icons/catalog/chars-gray/trans.png">
                                                                                            <span style="color: #bcc0c6;">{{$car->trans_name}}</span>
                                                                                        </td>
                                                                                        <td width="30%" style="text-align:right;">
                                                                                            <img alt style="width: 60px;" src="http://lion-avtoprokat.com.ua/images/lion_2.png">
                                                                                        </td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </table>

                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </div>

                                                            <!--[if mso | IE]>
                                                            </td>

                                                            </tr>

                                                            </table>
                                                            <![endif]-->
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>

                                            </div>


                                            <!--[if mso | IE]>
                                            </td>
                                            </tr>
                                            </table>
                                            <![endif]-->


                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                                <!--[if mso | IE]>
                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div style="Margin:0px auto;max-width:900px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td style="direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;">
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:900px;"
                                                        >
                                                <![endif]-->

                                                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                        <tr>
                                                            <td style="font-size:0px;padding:0px;word-break:break-word;">

                                                                <p style="border-top: dashed 2px lightgrey; font-size: 1; margin: 0px auto; width: 100%;">
                                                                </p>

                                                                <!--[if mso | IE]>
                                                                <table
                                                                        align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:dashed 2px lightgrey;font-size:1;margin:0px auto;width:900px;" role="presentation" width="900px"
                                                                >
                                                                    <tr>
                                                                        <td style="height:0;line-height:0;">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <![endif]-->


                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div style="Margin:0px auto;max-width:900px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:900px;"
                                                        >
                                                <![endif]-->

                                                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                        <tr>
                                                            <td align="left" style="font-size:0px;padding:0px;word-break:break-word;">

                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0" style="cellspacing:0;color:#000000;font-family:'Exo 2', sans-serif;font-size:14px;line-height:22px;table-layout:auto;width:100%;">

                                                                </table>

                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div style="Margin:0px auto;max-width:900px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                        <tbody>
                                        <tr>
                                            <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;vertical-align:top;">
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:900px;"
                                                        >
                                                <![endif]-->

                                                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                        <tr>
                                                            <td style="font-size:0px;word-break:break-word;">


                                                                <!--[if mso | IE]>

                                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td height="50" style="vertical-align:top;height:50px;">

                                                                <![endif]-->

                                                                <div style="height:50px;">
                                                                    &nbsp;
                                                                </div>

                                                                <!--[if mso | IE]>

                                                                </td></tr></table>

                                                                <![endif]-->


                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                <tr>
                                    <td
                                            class="" width="900px"
                                    >

                                        <table
                                                align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:900px;" width="900"
                                        >
                                            <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                <![endif]-->


                                <div style="background:#313131;background-color:#313131;Margin:0px auto;max-width:900px;">

                                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#313131;background-color:#313131;width:100%;">
                                        <tbody>
                                        <tr>
                                            <td style="direction:ltr;font-size:0px;padding:15px;text-align:center;vertical-align:top;">
                                                <!--[if mso | IE]>
                                                <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                                                    <tr>

                                                        <td
                                                                class="" style="vertical-align:top;width:870px;"
                                                        >
                                                <![endif]-->

                                                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                                                        <tr>
                                                            <td align="left" style="font-size:0px;padding:10px 25px;word-break:break-word;">

                                                                <table cellpadding="0" cellspacing="0" width="100%" border="0" style="cellspacing:0;color:#000000;font-family:'Exo 2', sans-serif;font-size:14px;line-height:22px;table-layout:auto;width:100%;">
                                                                    <table width="100%" style="color: #fff; font-family:'Exo 2', sans-serif; font-size:14px;  font-weight:300;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="text-align: left;padding: 15px 0;" valign="middle">
                                                                                <div>Украина, Днепр <span>проспект Победы, 67</span></div>
                                                                            </td>

                                                                            <td style="text-align: left;padding: 15px 0;" valign="middle">
                                                                                <div>
                                                                                    <a href="mailto:order@autoprokat.net.ua" style="color: #fff;">
                                                                                        <img src="./images/email.png" alt="phone" width="24" style="display: inline-block; vertical-align: middle;">  order@autoprokat.net.ua</a>
                                                                                </div>
                                                                            </td>

                                                                            <td style="text-align: left;padding: 15px 0;" valign="middle">
                                                                                <div>
                                                                                    <a href="skype:autoprokat.net.ua?chat" style="color: #fff;">
                                                                                        <img src="./images/skype.png" alt="skype" width="24" style="display: inline-block; vertical-align: middle;">  autoprokat.net.ua</a>
                                                                                </div>
                                                                            </td>
                                                                            <td style="text-align: left;padding: 15px 0;" valign="middle">
                                                                                <a href="tel:80676442644" style="color: #fff;">
                                                                                    <img src="./images/phone.png" alt="phone" width="24" style="display: inline-block; vertical-align: middle;">  +38 (067) 644 2 644
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </table>

                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>

                                                <!--[if mso | IE]>
                                                </td>

                                                </tr>

                                                </table>
                                                <![endif]-->
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>


                                <!--[if mso | IE]>
                                </td>
                                </tr>
                                </table>

                                </td>
                                </tr>

                                </table>
                                <![endif]-->
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>


                <!--[if mso | IE]>
                </td>
                </tr>
                </table>
                <![endif]-->


            </td>
        </tr>
        </tbody>
    </table>

</div>

</body>
</html>
  