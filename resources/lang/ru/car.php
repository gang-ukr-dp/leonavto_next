<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'days' => 'Дни',
    'days_price' => 'Цена за сутки',
    'final_price' => 'Итоговая стоимость за',
    'equivalent' => "или аналоги",
    'send_proposal' => "Отправить предложение",
    'enter_email' => "введите свой email",
    'order_btn' => "Заказать",
    'deposit' => "Залог",
];
