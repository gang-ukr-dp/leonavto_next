<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'order_name' => 'ЗАЯВКА НА ПРОКАТ АВТОМОБИЛЯ:',
    'order' => 'заказ',
    'order_includes' => 'Ваш заказ включает в себя',
    'order_additional' => 'Дополнительные опции',
    'order_details' => 'Детали заказа',
    'total' => 'Итого',
    'price' => 'Сумма заказа',
    'deposit' => 'Залог'
];
