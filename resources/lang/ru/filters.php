<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'class' => 'Класс',
    'brand' => 'Марка',
    'transmission' => 'Коробка передач',
    'fuel' => 'Топливо',
    'all_classes' => 'Все классы',
    'all_brands' => 'Все марки',
    'all_transes' => 'Все трансмиссии',
    'all_fuels' => 'Все топлива',
    'sortA' => 'от дешевых к дорогим',
    'sortD' => 'от дорогих к дешевым',

];
