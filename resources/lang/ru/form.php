<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'choose_country' => 'Выберете страну',
    'header' => 'Подбор авто',
    'get' => 'Подача',
    'place_get' => 'Место подачи',
    'return' => 'Возврат',
    'place_return' => 'Место возврата',
    'button' => 'Подобрать автомобиль',
    'button_order' => 'Заказать',
    'personal_data' => 'Личные данные',
    'fio' => 'Полное имя',
    'phone' => 'Телефон',
    'comment' => 'Комментарий',
    'options' => 'Дополнительные опции',
    'notice' => 'Внимание! Мы гарантируем выдачу автомобиля в классе с заявленной коробкой передач, мы обязательно учтем ваши пожелания по конкретной марке автомобиля. Выбрав форму оплаты и нажимая заказать вы соглашаетесь с условиями платежа и бронирования изложенными на сайте в разделе условия проката, а так же договора оферты',
    'notice2' => 'Для получения автомобиля не забудьте взять паспорт, водительское удостоверение и деньги. Зарядить телефон, установить приложение.',
    'back' => 'Назад',
    'final_price' => 'Итоговая стоимость',
    'your_order' => 'Ваш заказ',
    'accepted' => 'принят',
    'in_process' => 'Наши операторы уже обрабатывают Ваш заказ!',
    'for_consult' => 'Для консультации вы можете позвонить по номеру телефона:',
    'in_ukraine' => 'в Украине',
    '_ukraine' => 'Украине',
    '_georgia' => 'Грузии',
    '_czech' => 'Чехии',
    '_оае' => 'ОАЕ',
    'rent_a_car' => 'Прокат авто в',
    'our_offices_in' => 'Наши офисы в',
    'noFound' => 'Без результатов',
];
