<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'choose_country' => 'Select Country',
    'header' => 'Select Car',
    'get' => 'Get',
    'place_get' => 'Place to get',
    'return' => 'Return',
    'place_return' => 'Place to return',
    'button' => 'Select a car',
    'button_order' => 'Make Order',
    'personal_data' => 'Personal params',
    'fio' => 'Full Name',
    'phone' => 'Phone',
    'comment' => 'Comment',
    'options' => 'Additional options',
    'notice' => 'Attention, please! We guarantee that we will give you car in the class with the stated gearbox, we will take into account your wishes for a particular car brand. When you choose the form of payment and click "order the car" you agree with the terms of payment and booking set forth on the website in the section rental conditions, as well as the public offer contract.',
    'notice2' => "To get your car don't forget your passport, driver's licence, money and charge your phone.",
    'back' => 'Back',
    'final_price' => 'Final price',
    'your_order' => 'Your order',
    'accepted' => 'accepted',
    'in_process' => 'Our managers are already processing your order!',
    'for_consult' => 'In order to consult you can make a call on the phone number:',
    'in_ukraine' => 'in Ukraine',
    '_ukraine' => 'Ukraine',
    '_georgia' => 'Georgia',
    '_czech' => 'Czech',
    '_оае' => 'ОАЕ',
    'rent_a_car' => 'Rent a car in',
    'our_offices_in' => 'Our offices in',
    'noFound' => 'Not found',

];
