<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'class' => 'Class',
    'brand' => 'Brand',
    'transmission' => 'Transmission',
    'fuel' => 'Fuel',
    'all_classes' => 'All Classes',
    'all_brands' => 'All Brands',
    'all_transes' => 'All Transmissions',
    'all_fuels' => 'All Fuels',

];
