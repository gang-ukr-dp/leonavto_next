<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'days' => 'Days',
    'days_price' => "Day's price",
    'final_price' => "Final price for",
    'equivalent' => "or equivalent",
    'send_proposal' => "Send proposal",
    'enter_email' => "enter your email",
    'order_btn' => "Order",
    'deposit' => "Deposit",
];
