<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'order_name' => 'Proposal to rent a car:',
    'order' => 'order',
    'order_includes' => 'Your order includes',
    'order_details' => 'Order details',
    'order_additional' => 'Additional options',
    'total' => 'Total',
    'price' => 'Order',
    'deposit' => 'Deposit'
];
