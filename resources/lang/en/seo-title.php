<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'main' => 'Car Rental Company Lion Official Site',
    'main-description' => 'DESCRIPTION',
    'main-keywords' => 'KEYWORDS',

    'autopark-country-ukraine' => 'ukraine',
    'autopark-country-georgia' => 'ukraine',
    'autopark-country-czech-republic' => 'ukraine',
    'autopark-country-czech-uae' => 'ukraine',

    'autopark-city-boryspil' => 'ukraine',
    'autopark-city-dnieper'=> 'ukraine',
    'autopark-city-kherson'=> 'ukraine',
    'autopark-city-kremenchuk'=> 'ukraine',
    'autopark-city-lviv'=> 'ukraine',
    'autopark-city-mykolaiv'=> 'ukraine',
    'autopark-city-rivne'=> 'ukraine',
    'autopark-city-vinnitsa'=> 'ukraine',
    'autopark-city-cherkasy'=> 'ukraine',
    'autopark-city-ivano-frankivsk'=> 'ukraine',
    'autopark-city-khmelnytskyi'=> 'ukraine',
    'autopark-city-kryvyi-rig'=> 'ukraine',
    'autopark-city-mariupil'=> 'ukraine',
    'autopark-city-odessa'=> 'ukraine',
    'autopark-city-ternopil'=> 'ukraine',
    'autopark-city-zaporozhye'=> 'ukraine',
    'autopark-city-chernigiv'=> 'ukraine',
    'autopark-city-kharkiv'=> 'ukraine',
    'autopark-city-kiev'=> 'ukraine',
    'autopark-city-lutsk'=> 'ukraine',
    'autopark-city-melitopil'=> 'ukraine',
    'autopark-city-poltava'=> 'ukraine',
    'autopark-city-uzhhorod'=> 'ukraine',
    'autopark-city-zhitomir'=> 'ukraine',

    'autopark-city-batumi'=> 'batumi',
    'autopark-city-kutaisi'=> 'kutaisi',
    'autopark-city-tbilisi'=> 'tbilisi',



];
