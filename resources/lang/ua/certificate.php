<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'order_name' => 'ЗАЯВКА НА ПРОКАТ АВТОМОБІЛЯ:',
    'order' => 'замовлення',
    'order_includes' => 'Ваше замовлення включає в себе',
    'order_details' => 'Деталі замовлення',
    'order_additional' => 'Додаткові опції',
    'total' => 'Разом',
    'price' => 'Сумма замовлення',
    'deposit' => 'Застава'
];
