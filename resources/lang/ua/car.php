<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'days' => 'Дні',
    'days_price' => 'Ціна за добу',
    'final_price' => 'Підсумкова вартість за',
    'equivalent' => "Або аналоги",
    'send_proposal' => "Відправити пропозицію",
    'enter_email' => "введить свій email",
    'order_btn' => "Заказати",
    'deposit' => "Застава",
];
