<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'class' => 'Класс',
    'brand' => 'Марка',
    'transmission' => 'Коробка передач',
    'fuel' => 'Паливо',
    'all_classes' => 'Усі класи',
    'all_brands' => 'Усі марки',
    'all_transes' => 'Все трансміссії',
    'all_fuels' => 'Всі палива',

    'sortA' => 'від дешевих до дорогих',
    'sortD' => 'від дорогих до дешевих',
];
