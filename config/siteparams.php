<?php

return [
    'countries_for_nav' => [1,3,4,11],
    'currencies' => [1,2,6,7],      //id from table autoproko.currency
    'cities_in_main' => [1,2,12,13],//id from table autoproko.regions

    'site_url' => 'localhost:8000',
    'contacts' => [
        'phone' => '+38-067-644-2-644',
        'address' => 'г. Днепр, ул. Глинки 2',
        'email' => 'order@race.biz.ua',
        'coords' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2645.513626030884!2d35.049562215660515!3d48.465860879250975!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40dbe2c2c8a0e4bb%3A0x1da8e001a5509e78!2z0YPQuy4g0JPQu9C40L3QutC4LCAyLCDQlNC90LjQv9GA0L4sINCU0L3QtdC_0YDQvtC_0LXRgtGA0L7QstGB0LrQsNGPINC-0LHQu9Cw0YHRgtGMLCA0OTAwMA!5e0!3m2!1sru!2sua!4v1545841813910',

     ],
    'viberlink' => '/',
    'whatsapplink' => '/',
    'telegramlink' => '/',
    'viberimage' => '/images/icon_viber.png',
    'whatsappimage' => '/images/icon_whatsapp.png',
    'telegramimage' => '/images/icon_telegram.png',

    'fblink' => '/',
    'twilink' => '/',
    'googlelink' => '/',
    'vklink' => '/',
    'fbimage' => '/images/fb.png',
    'twiimage' => '/images/twi.png',
    'googleimage' => '/images/google.png',
    'vkimage' => '/images/vk.png',
    'logo' => '/images/Logo.png',

];